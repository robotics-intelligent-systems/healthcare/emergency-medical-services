/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.connector;

import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.LoginController;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.RemoteFactory;
import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.request.RequestHandlerIntf;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author Janith
 */
public class ServerConnector {

    private DonationControllerIntf donationController;
    private static ServerConnector serverConnector;
    private RemoteFactory remoteFactory;
    private BloodTypeControllerIntf bloodTypeController;
    private DonorControllerIntf donorController;
    private ReceiverControllerIntf receiverController;
    private SeperationControllerIntf seperationController;
    private RequestHandlerIntf requestHandlerIntf;
    private LoginController loginController;

    private ServerConnector() throws NotBoundException, MalformedURLException, RemoteException {
        remoteFactory = (RemoteFactory) Naming.lookup("rmi://localhost:5050/BloodBankServer");
    }

    public static ServerConnector getServerConnector() throws NotBoundException, MalformedURLException, RemoteException {
        if (serverConnector == null) {
            serverConnector = new ServerConnector();
        }
        return serverConnector;
    }

    public RemoteFactory getRemoteFactory() {
        return remoteFactory;
    }

    public BloodTypeControllerIntf getBloodTypeControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (bloodTypeController == null) {
            bloodTypeController = remoteFactory.getBloodTypeController();
        }
        return bloodTypeController;
    }

    public DonorControllerIntf getDonorControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (donorController == null) {
            donorController = remoteFactory.getDonorController();
        }
        return donorController;
    }

    public ReceiverControllerIntf getReceiverControllerIntf() throws RemoteException {
        if (receiverController == null) {
            receiverController = remoteFactory.getReceiverControllerIntf();
        }
        return receiverController;
    }

    public DonationControllerIntf getDonationControllerIntf() throws RemoteException {
        if (donationController == null) {
            donationController = remoteFactory.getDonationController();
        }
        return donationController;
    }

    public SeperationControllerIntf getSeperationControllerIntf() throws RemoteException {
        if (seperationController == null) {
            seperationController = remoteFactory.getSeperationController();
        }
        return seperationController;
    }

    public LoginController getLoginController() throws NotBoundException, MalformedURLException, RemoteException {
        if (loginController == null) {
            loginController = remoteFactory.getLoginController();
        }
        return loginController;
    }

    public RequestHandlerIntf getRequestHandlerIntf() throws RemoteException {
        if (requestHandlerIntf == null) {
            requestHandlerIntf = remoteFactory.getRequestHandler();
        }
        return requestHandlerIntf;
    }
}