/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.bloodtype.BloodTypes;
import bloodbankcommon.bloodtype.RedCell;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Receiver;
import bloodbankcommon.model.Request;
import bloodbankcommon.model.Seperation;
import bloodbankcommon.observer.RequestObserver;
import bloodbanklaborotary.model.ExpirationTabelModel;
import bloodbanklaborotary.model.RequestsTableModel;
import bloodbanklaborotary.model.SeperationTabelModel;
import bloodbanklaborotary.view.addhospitalsview.AddHospitalsViewController;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Janith
 */
public class LaborotaryViewController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Attributes">
    private Stage stage;
    private LoborotaryViewServices laborotaryViewServices;
    private SeperationTabelModel labBloodSeperation;
    private Donation currentDonation;
    private ObservableList<String> outsBloodIdList = FXCollections.observableArrayList();
    private List<Blood> outsBloodList = new ArrayList<>();
    private ObservableList<ExpirationTabelModel> expTabelList = FXCollections.observableArrayList();
    private ObservableList<RequestsTableModel> reqTabelList = FXCollections.observableArrayList();
    private ObservableList<String> issuingViewList = FXCollections.observableArrayList();
    private BooleanProperty issuingStarted = new SimpleBooleanProperty(false);
    private Receiver currentHospital;
    private BooleanProperty hospLoaded = new SimpleBooleanProperty(false);
    private BooleanProperty hospIdFill = new SimpleBooleanProperty(false);
    private BooleanProperty hospNameFill = new SimpleBooleanProperty(false);
    private BooleanProperty hospAddrFill = new SimpleBooleanProperty(false);
    private BooleanProperty hospTelFill = new SimpleBooleanProperty(false);
    private LaborotaryViewObservable laborotaryViewObservable;
    private int numRequests = 0;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Injections">
    //<editor-fold defaultstate="collapsed" desc="Window">
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private MenuItem addHospitalMenuItem;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private MenuItem settingsMenuItem;
    @FXML
    private Label requestsInfoBar;
    @FXML
    private AnchorPane requestPane;
    @FXML
    private AnchorPane seperationPane;
    @FXML
    private AnchorPane expirationPane;
    @FXML
    private AnchorPane outsourcingPane;
    @FXML
    private AnchorPane returnPane;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Seperations">
    @FXML
    private TextField donationId;
    @FXML
    private Label bloodGroup;
    @FXML
    private Label bloodType;
    @FXML
    private Label seperationErrorLabel;
    @FXML
    private RadioButton seperatedRadio;
    @FXML
    private RadioButton plasmaRadio;
    @FXML
    private RadioButton plateletRadio;
    @FXML
    private RadioButton redCellRadio;
    @FXML
    private ToggleGroup seperationTypesGroup;
    @FXML
    private CheckBox redCellCheck;
    @FXML
    private CheckBox ffPlasmaCheck;
    @FXML
    private CheckBox cspCheck;
    @FXML
    private CheckBox plateletCheck;
    @FXML
    private CheckBox cryoCheck;
    @FXML
    private Button seperationSubmitBtn;
    @FXML
    private CheckBox rejectBloodCheck;
    @FXML
    private CheckBox rejectDonorCheck;
    @FXML
    private TextField rejectReasonText;
    @FXML
    private Button rejectBtn;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Requests">
    @FXML
    private TableView<RequestsTableModel> requestTable;
    @FXML
    private TableColumn<RequestsTableModel, String> wardNumColumn;
    @FXML
    private TableColumn<RequestsTableModel, String> priorityColumn;
    @FXML
    private TableColumn<RequestsTableModel, String> statusColumn;
    @FXML
    private ListView<String> issuingList;
    @FXML
    private Button removeButton;
    @FXML
    private Button issueButton;
    @FXML
    private TextField issueBloodText;
    @FXML
    private TextArea detailViewer;
    @FXML
    private TextField statusText;
    @FXML
    private Button statusButton;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Outsourcing">
    @FXML
    private TextField hospitalIdText;
    @FXML
    private TextField hospitalNameText;
    @FXML
    private TextField hospitalAddrText;
    @FXML
    private TextField hospitalTelText;
    @FXML
    private Label hospitalErrorLabel;
    @FXML
    private Button hospitalButton;
    @FXML
    private ChoiceBox<String> outsTypeChoice;
    @FXML
    private TextField outsIdText;
    @FXML
    private Label outsIdErrorLabel;
    @FXML
    private ListView<String> outsListView;
    @FXML
    private TextArea outsDetailsArea;
    @FXML
    private Button outsRemoveButton;
    @FXML
    private Button outsIssueButton;
    @FXML
    private Label outsErrorLabel;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Expirations">
    @FXML
    private Button expReloadBtn;
    @FXML
    private TableView<ExpirationTabelModel> expirationsTable;
    @FXML
    private TableColumn<ExpirationTabelModel, String> expIdTabelColumn;
    @FXML
    private TableColumn<ExpirationTabelModel, String> expCatagoryTabelColumn;
    @FXML
    private Label expTabelErrorLabel;
    @FXML
    private TextField expIdText;
    @FXML
    private Button expSubmitBtn;
    @FXML
    private Label expErrorLabel;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Returns">
    @FXML
    private TextField returnBloodText;
    @FXML
    private CheckBox removeReturnsCheck;
    @FXML
    private Button submitReturnButton;
    @FXML
    private Label returnErrorLabel;
    @FXML
    private ChoiceBox<String> returnTypeChoice;
    //</editor-fold>
    //</editor-fold>

    public LaborotaryViewController() {
        try {
            this.laborotaryViewServices = new LoborotaryViewServices();
            this.laborotaryViewObservable = new LaborotaryViewObservable(this, stage);
            laborotaryViewObservable.hMap_RequestId_ObservRequests.addListener(new MapChangeListener<Request, RequestObserver>() {
                @Override
                public void onChanged(final MapChangeListener.Change<? extends Request, ? extends RequestObserver> change) {
                    if (change.wasAdded()) {
                        final StringBuilder priority = new StringBuilder("Regular");
                        if (change.getKey().getBlood().getBlootTypeIndex() == BloodType.RED_CELL) {
                            RedCell redCell = (RedCell) change.getKey().getBlood();
                            priority.replace(0, priority.length(), redCell.getPriority().toString().toUpperCase());
                        }
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    RequestsTableModel model = new RequestsTableModel(
                                            change.getValueAdded().getWardId() + "", priority.toString(), change.getKey());                                    
                                    requestTable.getItems().add(model);
                                    Dialogs.showInformationDialog(stage, "A New Request Received");
                                    requestsInfoBar.setText(++numRequests + " Requests Remaining to Process");
                                } catch (RemoteException ex) {
                                    Dialogs.showErrorDialog(stage, ex.getMessage(),
                                            "The Request Received Last Was Unable to Process", "Error");
                                }
                            }
                        });
                    } else if (change.wasRemoved()) {
                        final RequestsTableModel model = new RequestsTableModel(null, null, change.getKey());
                        reqTabelList.remove(model);
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                Dialogs.showInformationDialog(stage, "Request " + model.getRequest().getRequestId() + "was Cancelled");
                                String status = --numRequests > 0 ? numRequests + " Requests Remaining to Process" : "No Requests Remaining to Process";
                                requestsInfoBar.setText(status);
                            }
                        });
                    }
                }
            });

        } catch (NotBoundException | RemoteException | MalformedURLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
            //System.exit(0);
        } catch (IOException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "File Read Error", "File Error");
            //System.exit(0);
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        requestTable.setItems(reqTabelList);
        /**
         * Must add to Server >request handler >List
         */
        expIdText.setDisable(true);
        expSubmitBtn.setDisable(true);
        disableHospControls();
        disableOutsControls();
        outsTypeChoice.getItems().clear();
        outsTypeChoice.getItems().addAll(BloodTypes.RED_CELL.getName(),
                BloodTypes.PLATELET.getName(),
                BloodTypes.PLASMA.getName(),
                BloodTypes.CSP.getName(),
                BloodTypes.CRYO.getName());
        outsTypeChoice.getSelectionModel().selectFirst();

        statusButton.setDisable(true);

        //<editor-fold defaultstate="collapsed" desc="Background">
        String image = LaborotaryViewController.class.getResource("flow8.jpg").toExternalForm();
        String style = "-fx-background-image: url('" + image + "');"
                + "-fx-background-position: center center;"
                + "-fx-background-repeat: stretch;";
        requestPane.setStyle(style);
        seperationPane.setStyle(style);
        outsourcingPane.setStyle(style);
        expirationPane.setStyle(style);
        returnPane.setStyle(style);

        initializeStage();
        //</editor-fold>

        expIdTabelColumn.setCellValueFactory(new PropertyValueFactory<ExpirationTabelModel, String>("id"));
        expCatagoryTabelColumn.setCellValueFactory(new PropertyValueFactory<ExpirationTabelModel, String>("catagory"));

        disableRequestControls();
        wardNumColumn.setCellValueFactory(new PropertyValueFactory<RequestsTableModel, String>("wardNumber"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<RequestsTableModel, String>("priority"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<RequestsTableModel, String>("status"));
        requestTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RequestsTableModel>() {
            @Override
            public void changed(ObservableValue<? extends RequestsTableModel> observable, RequestsTableModel oldValue, RequestsTableModel newValue) {
                if (newValue != null) {
                    detailViewer.setText(newValue.getDetailString());
                    statusText.setText(newValue.getStatus() != null ? newValue.getStatus() : "");
                    enableRequestControls();
                } else {
                    disableRequestControls();
                }
            }
        });
        issueBloodText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.length() > 9) {
                    issueBloodText.setText(oldValue);
                }
            }
        });
        statusText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.length() > 500) {
                    statusText.setText(oldValue);
                }
            }
        });
        issuingViewList.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                issuingStarted.set(issuingViewList.size() > 0);
            }
        });
        issueButton.disableProperty().bind(issuingStarted.not());

        expirationsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ExpirationTabelModel>() {
            @Override
            public void changed(ObservableValue<? extends ExpirationTabelModel> observable, ExpirationTabelModel oldValue, ExpirationTabelModel newValue) {
                if (newValue != null) {
                    expIdText.setText(newValue.getId());
                }
            }
        });
        expirationsTable.itemsProperty().addListener(new ChangeListener<ObservableList<ExpirationTabelModel>>() {
            @Override
            public void changed(ObservableValue<? extends ObservableList<ExpirationTabelModel>> observable, ObservableList<ExpirationTabelModel> oldValue, ObservableList<ExpirationTabelModel> newValue) {
                if (newValue != null || newValue.size() != 0) {
                    expIdText.setDisable(false);
                    expSubmitBtn.setDisable(false);
                } else {
                    expIdText.setDisable(true);
                    expSubmitBtn.setDisable(true);
                }
            }
        });

        returnTypeChoice.getItems().clear();
        returnTypeChoice.getItems().addAll(BloodTypes.RED_CELL.getName(),
                BloodTypes.PLATELET.getName(),
                BloodTypes.PLASMA.getName(),
                BloodTypes.CSP.getName(),
                BloodTypes.CRYO.getName());
        returnTypeChoice.getSelectionModel().selectFirst();
        returnTypeChoice.setDisable(true);
        removeReturnsCheck.setDisable(true);
        submitReturnButton.setDisable(true);
        returnBloodText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if ("^(\\d){2}([a-z|A-Z]){2}(\\d){5}$".matches(newValue)) {
                    removeReturnsCheck.setDisable(true);
                    submitReturnButton.setDisable(true);
                    returnTypeChoice.setDisable(true);
                } else {
                    removeReturnsCheck.setDisable(false);
                    submitReturnButton.setDisable(false);
                    returnTypeChoice.setDisable(false);
                }
            }
        });

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                logout();
            }
        });

    }

    private void initializeStage() {

        //<editor-fold defaultstate="collapsed" desc="Validations">
        donationId.textProperty().addListener(getIdValidater(donationId, 10, null));
        issueBloodText.textProperty().addListener(getIdValidater(issueBloodText, 10, null));
        outsIssueButton.setDisable(true);
        hospitalIdText.textProperty().addListener(getIdValidater(hospitalIdText, 4, hospIdFill));
        hospitalNameText.textProperty().addListener(getIdValidater(hospitalNameText, 50, hospNameFill));
        hospitalAddrText.textProperty().addListener(getIdValidater(hospitalAddrText, 200, hospAddrFill));
        hospitalTelText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!(newValue == null || newValue.isEmpty())) {
                    if (newValue.length() > 10) {
                        hospitalTelText.setText(oldValue);
                    }
                    try {
                        Integer.parseInt(newValue);
                    } catch (NumberFormatException ex) {
                        hospitalTelText.setText(oldValue);
                    }
                    if (newValue.length() == 10) {
                        hospTelFill.set(true);
                    } else {
                        hospTelFill.set(false);
                    }
                } else {
                    hospTelFill.set(false);
                }
            }
        });
        hospitalButton.disableProperty().bind((hospIdFill.and(hospNameFill.and(hospAddrFill.and(hospTelFill)))).not());
        hospLoaded.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    String hId = hospitalIdText.getText();
                    clearHospControls();
                    hospitalIdText.setText(hId);
                    clearOutsControls();
                    enableOutsControls();
                    enableHospControls();

                } else {
                    clearOutsControls();
                    clearHospControls();
                    disableHospControls();
                    disableOutsControls();
                }
            }
        });
        outsListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        outsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null || !newValue.isEmpty()) {
                    outsRemoveButton.setDisable(false);
                    for (Blood b : outsBloodList) {
                        if (newValue.equals(b.getBloodId())) {
                            fillDetailArea(b);
                        }
                    }
                } else {
                    outsRemoveButton.setDisable(true);
                }
            }
        });
        outsBloodIdList.addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                if (outsBloodIdList.size() > 0) {
                    outsIssueButton.setDisable(false);
                } else {
                    outsIssueButton.setDisable(true);
                    outsDetailsArea.setText("");
                }
            }
        });

        returnBloodText.textProperty().addListener(getIdValidater(returnBloodText, 10, null));

        expIdText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String pattern = "^(\\d){2}([a-z|A-Z]){2}(\\d){5}$";
                if (expIdText.getText().matches(pattern)) {
                    expReloadBtn.setDisable(false);
                } else {
                    expReloadBtn.setDisable(true);
                }
            }
        });

        //</editor-fold>
        seperationSubmitBtn.setDisable(true);
        rejectBtn.setDisable(true);
        seperatedRadio.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    enableCheckBoxes();
                } else {
                    disableCheckBoxes();
                }
            }
        });
        seperationSubmitBtn.disableProperty().bind((plasmaRadio.selectedProperty()
                .or(plateletRadio.selectedProperty()
                .or(redCellRadio.selectedProperty()
                .or(redCellCheck.selectedProperty())
                .or(ffPlasmaCheck.selectedProperty())
                .or(cspCheck.selectedProperty())
                .or(cryoCheck.selectedProperty())
                .or(plateletCheck.selectedProperty())))).not());
        rejectDonorCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                rejectBloodCheck.setSelected(newValue);
                rejectBloodCheck.setDisable(newValue);

            }
        });
        rejectReasonText.disableProperty().bind(rejectBloodCheck.selectedProperty().not());
        rejectBloodCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    if (rejectReasonText.getText().isEmpty()) {
                        rejectBtn.setDisable(true);
                    } else {
                        rejectBtn.setDisable(false);
                    }
                } else {
                    rejectBtn.setDisable(true);
                }
            }
        });
        rejectReasonText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!(newValue == null || newValue.isEmpty())) {
                    rejectBtn.setDisable(false);
                    if (newValue.length() > 19) {
                        rejectReasonText.setText(oldValue);
                    }
                } else {
                    rejectBtn.setDisable(true);
                }
            }
        });
    }

    @FXML
    public void searchBloodtoSeperate(ActionEvent event) {
        try {
            labBloodSeperation = laborotaryViewServices.searchBlood(donationId.getText());
            if (labBloodSeperation != null) {
                bloodGroup.setText(labBloodSeperation.getBloodGroup());
                int bT = labBloodSeperation.getBloodType();
                if (bT == 0) {
                    bloodType.setText("WholeBlood");
                } else if (bT == 2) {
                    bloodType.setText("RedCell");
                } else if (bT == 3) {
                    bloodType.setText("Platelets");
                } else if (bT == 4) {
                    bloodType.setText("Plasma");
                }
            } else {
                donationId.setText("");
                bloodGroup.setText("");
                bloodType.setText("");
            }
        } catch (RemoteException | PropertyVetoException | ClassNotFoundException ex) {
            seperationErrorLabel.setText("Server Error");
        } catch (SQLException ex) {
            seperationErrorLabel.setText("Invalid ID");
        }

    }

    @FXML
    public void onExitBtnClick(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    public void onaddHospitalBtnClick(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addhospitalsview/AddHospitalsView.fxml"));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("addhospitalsview/AddHospitalsView.fxml"));
        } catch (IOException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Error Loading DialogBox", "Error");
        }
        AddHospitalsViewController controller = (AddHospitalsViewController) loader.getController();
        controller.setParentStage(stage);
        Stage hospitalStage = new Stage();
        hospitalStage.initModality(Modality.APPLICATION_MODAL);
        hospitalStage.getIcons().add(new Image(LaborotaryViewController.class.getResourceAsStream("BloodDrop.png")));
        Scene scene = new Scene(root);
        hospitalStage.setScene(scene);
        hospitalStage.show();
    }

    @FXML
    public void onsettingsBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image(LaborotaryViewController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("settingsview/SettingsView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onAboutBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image(LaborotaryViewController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("aboutview/AboutView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    private void disableCheckBoxes() {
        redCellCheck.setDisable(true);
        ffPlasmaCheck.setDisable(true);
        cspCheck.setDisable(true);
        plateletCheck.setDisable(true);
        cryoCheck.setDisable(true);
    }

    private void enableCheckBoxes() {
        redCellCheck.setDisable(false);
        ffPlasmaCheck.setDisable(false);
        cspCheck.setDisable(false);
        plateletCheck.setDisable(false);
        cryoCheck.setDisable(false);
    }

    @FXML
    public void onDonationIdFieldActionPerformed(ActionEvent event) {
        seperationErrorLabel.setText("");
        try {
            currentDonation = laborotaryViewServices.getDonationSimple(donationId.getText());
            if (currentDonation == null || currentDonation.getBloodId().equals("")) {
                seperationErrorLabel.setText("Invalid ID");
                seperationSubmitBtn.setDisable(true);
                return;
            }
            String group = laborotaryViewServices.getBloodGroup(currentDonation.getBloodId());
            bloodGroup.setText(group);
            bloodType.setText(BloodType.getStringOf(currentDonation.getType()));
            seperationSubmitBtn.setDisable(false);
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            seperationErrorLabel.setText("Server Error");
            seperationSubmitBtn.setDisable(true);
        } catch (SQLException ex) {
            seperationErrorLabel.setText("Invalid ID");
            seperationSubmitBtn.setDisable(true);
        }
    }

    @FXML
    public void onSeperationSubmitClick(ActionEvent event) {
        String bloodId = currentDonation.getBloodId();
        Seperation seperation = new Seperation(bloodId, "", "", "", "", "");
        List<Blood> bloodList = new ArrayList<>(5);
        if (seperatedRadio.isSelected()) {
            if (redCellCheck.isSelected()) {
                seperation.setRedcellId(bloodId);
                //<editor-fold defaultstate="collapsed" desc="init Blood">
                Blood blood = new Blood(bloodId, bloodGroup.getText());
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, BloodType.RED_CELL_EXPIARY);
                Date exDate = new Date(calendar.getTimeInMillis());
                blood.setExpiaryDate(exDate);
                blood.setBloodType(BloodType.getStringOf(BloodType.RED_CELL));
                bloodList.add(blood);
                //</editor-fold>
            } else if (ffPlasmaCheck.isSelected()) {
                seperation.setFfplasmaId(bloodId);
                //<editor-fold defaultstate="collapsed" desc="init Blood">
                Blood blood = new Blood(bloodId, bloodGroup.getText());
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, BloodType.PLASMA_EXPIARY);
                Date exDate = new Date(calendar.getTimeInMillis());
                blood.setExpiaryDate(exDate);
                blood.setBloodType(BloodType.getStringOf(BloodType.PLASMA));
                bloodList.add(blood);
                //</editor-fold>
            } else if (cspCheck.isSelected()) {
                seperation.setCspplamaId(bloodId);
                //<editor-fold defaultstate="collapsed" desc="init Blood">
                Blood blood = new Blood(bloodId, bloodGroup.getText());
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, BloodType.CSP_EXPIARY);
                Date exDate = new Date(calendar.getTimeInMillis());
                blood.setExpiaryDate(exDate);
                blood.setBloodType(BloodType.getStringOf(BloodType.CSP));
                bloodList.add(blood);
                //</editor-fold>
            } else if (plateletCheck.isSelected()) {
                seperation.setPlateletId(bloodId);
                //<editor-fold defaultstate="collapsed" desc="init Blood">
                Blood blood = new Blood(bloodId, bloodGroup.getText());
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, BloodType.PLATELET_EXPIARY);
                Date exDate = new Date(calendar.getTimeInMillis());
                blood.setExpiaryDate(exDate);
                blood.setBloodType(BloodType.getStringOf(BloodType.PLATELET));
                bloodList.add(blood);
                //</editor-fold>
            } else if (cryoCheck.isSelected()) {
                seperation.setCryoId(bloodId);
                //<editor-fold defaultstate="collapsed" desc="init Blood">
                Blood blood = new Blood(bloodId, bloodGroup.getText());
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_YEAR, BloodType.CRYO_EXPIARY);
                Date exDate = new Date(calendar.getTimeInMillis());
                blood.setExpiaryDate(exDate);
                blood.setBloodType(BloodType.getStringOf(BloodType.CRYO));
                bloodList.add(blood);
                //</editor-fold>
            }
        } else if (plasmaRadio.isSelected()) {
            seperation.setFfplasmaId(bloodId);
            //<editor-fold defaultstate="collapsed" desc="init Blood">
            Blood blood = new Blood(bloodId, bloodGroup.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, BloodType.PLASMA_EXPIARY);
            Date exDate = new Date(calendar.getTimeInMillis());
            blood.setExpiaryDate(exDate);
            blood.setBloodType(BloodType.getStringOf(BloodType.PLASMA));
            bloodList.add(blood);
            //</editor-fold>
        } else if (plateletRadio.isSelected()) {
            seperation.setPlateletId(bloodId);
            //<editor-fold defaultstate="collapsed" desc="init Blood">
            Blood blood = new Blood(bloodId, bloodGroup.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, BloodType.PLATELET_EXPIARY);
            Date exDate = new Date(calendar.getTimeInMillis());
            blood.setExpiaryDate(exDate);
            blood.setBloodType(BloodType.getStringOf(BloodType.PLATELET));
            bloodList.add(blood);
            //</editor-fold>
        } else if (redCellRadio.isSelected()) {
            seperation.setRedcellId(bloodId);
            //<editor-fold defaultstate="collapsed" desc="init Blood">
            Blood blood = new Blood(bloodId, bloodGroup.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, BloodType.RED_CELL_EXPIARY);
            Date exDate = new Date(calendar.getTimeInMillis());
            blood.setExpiaryDate(exDate);
            blood.setBloodType(BloodType.getStringOf(BloodType.RED_CELL));
            bloodList.add(blood);
            //</editor-fold>
        }
        try {
            boolean result = laborotaryViewServices.submitSeperation(seperation, bloodList);
            if (result) {
                donationId.setText("");
                bloodGroup.setText("");
                bloodType.setText("");
                seperatedRadio.setSelected(true);
                seperationSubmitBtn.setDisable(true);
                currentDonation = null;
                rejectReasonText.setText("");
            } else {
                Dialogs.showErrorDialog(stage, "Check server and connection", "Server Error");
            }
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Check blood ID", "Error");
        }
    }

    @FXML
    public void onRejectClick(ActionEvent event) {
        String error = "";
        String reason = rejectReasonText.getText();
        if (rejectDonorCheck.isSelected()) {
            try {
                boolean result = laborotaryViewServices.rejectDonor(currentDonation.getNic(), currentDonation.getBloodId(), reason);
                if (result) {
                    donationId.setText("");
                    bloodGroup.setText("");
                    bloodType.setText("");
                    seperatedRadio.setSelected(true);
                    seperationSubmitBtn.setDisable(true);
                    currentDonation = null;
                    rejectReasonText.setText("");
                } else {
                    Dialogs.showErrorDialog(stage, "Check server and connection", "Cannot submit changes", "Error");
                }
            } catch (RemoteException | ClassNotFoundException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
            } catch (SQLException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Check blood ID", "Error");
            }
        } else if (rejectBloodCheck.isSelected()) {
            try {
                boolean result = laborotaryViewServices.rejectDonation(currentDonation.getBloodId(), reason);
                if (result) {
                    donationId.setText("");
                    bloodGroup.setText("");
                    bloodType.setText("");
                    seperatedRadio.setSelected(true);
                    seperationSubmitBtn.setDisable(true);
                    currentDonation = null;
                    rejectReasonText.setText("");
                } else {
                    Dialogs.showErrorDialog(stage, "Check server and connection", "Cannot submit changes", "Error");
                }
            } catch (RemoteException | ClassNotFoundException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
            } catch (SQLException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Check blood ID", "Error");
            }
        }
    }

    @FXML
    public void onBloodSelection(ActionEvent event) {
        if (!"^(\\d){2}([a-z|A-Z]){2}(\\d){5}$".matches(issueBloodText.getText())) {
            Dialogs.showErrorDialog(stage, "Invalid ID");
            return;
        }
        RequestsTableModel reqTableModel = requestTable.getSelectionModel().getSelectedItem();
        if (reqTableModel == null) {
            return;
        }
        int blootTypeIndex = reqTableModel.getRequest().getBlood().getBlootTypeIndex();
        try {
            Blood blood = laborotaryViewServices.getBlood(issueBloodText.getText(), blootTypeIndex);
            if (blood == null || blood.getBloodId() == null) {
                Dialogs.showErrorDialog(stage, "Invalid ID.\nNo Matching Entry Found.");
                return;
            }
            issuingViewList.add(blood.getBloodId());
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error", "Error");
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Invalid ID", "Error");
        }
    }

    @FXML
    public void onBloodRemoval(ActionEvent event) {
        ObservableList<String> selectedItems = issuingList.getSelectionModel().getSelectedItems();
        issuingViewList.removeAll(selectedItems);
    }

    @FXML
    public void onIssueButtonClick(ActionEvent event) {
        RequestsTableModel request = requestTable.getSelectionModel().getSelectedItem();
        if (request == null || issuingViewList.isEmpty()) {
            return;
        }

        try {
            laborotaryViewObservable.sendStatusForRequest(request.getRequest().getRequestId(), "Issued");
        } catch (RemoteException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "RMI Error", "Error");
        }

        reqTabelList.remove(request);
        issuingViewList.clear();
        issueBloodText.setText("");
    }

    @FXML
    public void onStatusButtonClick(ActionEvent event) {
        RequestsTableModel request = requestTable.getSelectionModel().getSelectedItem();
        if (request == null || statusText.getText() == null || statusText.getText().isEmpty()) {
            return;
        }
        sendStatusForRequest(request.getRequest().getRequestId(), statusText.getText());
        
    }

    @FXML
    public void onHospitalIdActionPerformed(ActionEvent event) {
        String hospId = hospitalIdText.getText();
        if (!hospId.matches("^(h|H)(\\d){3}$")) {
            hospitalErrorLabel.setText("Invalid ID");
            return;
        }
        Receiver hospital = null;
        try {
            hospital = laborotaryViewServices.getHospital(hospitalIdText.getText());
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            hospitalErrorLabel.setText("Server Error");
            return;
        } catch (SQLException ex) {
            hospitalErrorLabel.setText("Invalid ID");
            return;
        }
        if (hospital != null && hospital.getReceiverId() != null) {
            hospitalIdText.setText(hospital.getReceiverId());
            hospitalAddrText.setText(hospital.getAddress());
            hospitalNameText.setText(hospital.getName());
            hospitalTelText.setText(hospital.getTele() + "");
            currentHospital = hospital;
            hospLoaded.set(true);
        } else if (hospital != null && hospital.getReceiverId() == null) {
            hospitalErrorLabel.setText("New Hospital");
            currentHospital = new Receiver(hospId, "", "", null, null, 0);
            enableHospControls();
        }
    }

    @FXML
    public void onHospitalBtnClick(ActionEvent event) {
        if (hospLoaded.get()) {
            if (currentHospital.getName().equals(hospitalNameText.getText())
                    & currentHospital.getAddress().equals(hospitalAddrText.getText())
                    & (currentHospital.getTele() + "").equals(hospitalTelText.getText())) {
                hospitalErrorLabel.setText("No Changes");
            } else {
                currentHospital.setName(hospitalNameText.getText());
                currentHospital.setAddress(hospitalAddrText.getText());
                currentHospital.setTele(Integer.parseInt(hospitalTelText.getText()));
                try {
                    boolean success = laborotaryViewServices.updateHospital(currentHospital);
                } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
                    hospitalErrorLabel.setText("Server Error");
                } catch (SQLException ex) {
                    hospitalErrorLabel.setText("Invalid ID");
                }
            }
        } else {
            currentHospital.setName(hospitalNameText.getText());
            currentHospital.setAddress(hospitalAddrText.getText());
            currentHospital.setTele(Integer.parseInt(hospitalTelText.getText()));
            try {
                boolean success = laborotaryViewServices.addHospital(currentHospital);
                if (success) {
                    hospLoaded.set(true);
                } else {
                    hospitalErrorLabel.setText("Server Error");
                }
            } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
                hospitalErrorLabel.setText("Server Error");
            } catch (SQLException ex) {
                hospitalErrorLabel.setText("Invalid ID");
            }
        }
    }

    @FXML
    public void onOutsBloodIdActionPerformed(ActionEvent event) {
        outsIdErrorLabel.setText("");
        outsErrorLabel.setText("");
        String bloodId = outsIdText.getText();
        int type = outsTypeChoice.getSelectionModel().getSelectedIndex();
        switch (type) {
            case (0):
                type = BloodType.PLATELET;
                break;
            case (1):
                type = BloodType.PLASMA;
                break;
            case (2):
                type = BloodType.CSP;
                break;
            case (3):
                type = BloodType.CRYO;
                break;
        }

        try {
            Blood blood = laborotaryViewServices.getBlood(bloodId, type);
            if (blood.getBloodId() != null || blood.getBloodId().isEmpty()) {
                outsIdErrorLabel.setText("Invalid ID");
                outsIdText.selectAll();
            } else {
                outsBloodIdList.add(blood.getBloodId());
                outsBloodList.add(blood);
            }
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            hospitalErrorLabel.setText("Server Error");
        } catch (SQLException ex) {
            hospitalErrorLabel.setText("Invalid ID");
        }
    }

    @FXML
    public void onOutsRemoveBtnClick(ActionEvent event) {
        if (outsListView.getSelectionModel().getSelectedIndex() != -1) {
            String selectedItem = outsListView.getSelectionModel().getSelectedItem();
            outsBloodIdList.remove(selectedItem);
            Blood removingBlood = null;
            for (Blood b : outsBloodList) {
                if (b.getBloodId().equals(selectedItem)) {
                    removingBlood = b;
                }
            }
            outsBloodList.remove(removingBlood);
        }
    }

    @FXML
    public void onOutsSubmitBtnClick(ActionEvent event) {
        String hospId = currentHospital.getReceiverId();
        try {
            boolean success = laborotaryViewServices.submitOutsourcings(hospId, outsBloodList);
            if (success) {
                hospLoaded.set(false);
                outsBloodIdList.clear();
                outsBloodList.clear();
                clearHospControls();
                clearOutsControls();
                disableHospControls();
                disableOutsControls();
            } else {
                outsErrorLabel.setText("Error! Cannot Submit");
            }
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            hospitalErrorLabel.setText("Server Error");
        } catch (SQLException ex) {
            hospitalErrorLabel.setText("Invalid ID");
        }
    }

    private void disableRequestControls() {
        issueBloodText.setDisable(true);
        issuingList.getItems().clear();
        issuingList.setDisable(true);
        removeButton.setDisable(true);
        statusText.setDisable(false);
        statusButton.setDisable(false);
    }

    private void enableRequestControls() {
        issueBloodText.setDisable(false);
        issueBloodText.setText("");
        issuingList.getItems().clear();
        issuingList.setDisable(false);
        removeButton.setDisable(false);
        statusText.setText("");
        statusText.setDisable(false);
        statusButton.setDisable(false);
    }

    private void fillDetailArea(Blood b) {
        String details = "Blood ID : " + b.getBloodId()
                + "\nBlood Group : " + b.getBloodGroup()
                + "\nBlood Type : " + b.getBloodType()
                + "\nExpiary Date : " + new java.sql.Date(b.getExpiaryDate().getTime()).toString();
    }

    private void clearHospControls() {
        hospitalIdText.setText("");
        hospitalAddrText.setText("");
        hospitalNameText.setText("");
        hospitalTelText.setText("");
        hospitalErrorLabel.setText("");
    }

    private void disableHospControls() {
        hospitalNameText.setDisable(true);
        hospitalAddrText.setDisable(true);
        hospitalTelText.setDisable(true);
        hospitalButton.setDisable(true);
    }

    private void enableHospControls() {
        hospitalNameText.setDisable(false);
        hospitalAddrText.setDisable(false);
        hospitalTelText.setDisable(false);
        hospitalButton.setDisable(false);
    }

    private void clearOutsControls() {
        outsIdText.setText("");
        outsIdErrorLabel.setText("");
        outsBloodIdList.clear();
        outsBloodList.clear();
        outsDetailsArea.setText("");
        outsErrorLabel.setText("");
    }

    private void disableOutsControls() {
        outsTypeChoice.setDisable(true);
        outsIdText.setDisable(true);
        outsListView.setDisable(true);
        outsRemoveButton.setDisable(true);
        outsIssueButton.setDisable(true);
        outsDetailsArea.setDisable(true);
    }

    private void enableOutsControls() {
        outsTypeChoice.setDisable(false);
        outsIdText.setDisable(false);
        outsListView.setDisable(false);
        outsRemoveButton.setDisable(false);
        outsIssueButton.setDisable(false);
        outsDetailsArea.setDisable(false);
    }

    @FXML
    public void onExpReloadClick(ActionEvent event) {
        expTabelErrorLabel.setText("");
        expTabelList.clear();

        try {
            List<Blood> expiredList = laborotaryViewServices.getExpiredList();
            expirationsTable.setItems(expTabelList);
            for (Blood b : expiredList) {
                ExpirationTabelModel model = new ExpirationTabelModel(b.getBloodId(), b.getBloodType());
                expTabelList.add(model);
            }
        } catch (RemoteException | SQLException | PropertyVetoException ex) {
            expTabelErrorLabel.setText("Server Error");
        } catch (ClassNotFoundException ex) {
            expTabelErrorLabel.setText(ex.getMessage());
        }
    }

    @FXML
    public void onExpRemoveClick(ActionEvent event) {
        expErrorLabel.setText("");
        for (ExpirationTabelModel model : expTabelList) {
            if (model.getId().equals(expIdText.getText())) {
                Blood blood = new Blood(model.getId(), model.getCatagory());
                try {
                    boolean removed = laborotaryViewServices.removeBlood(blood);
                    if (removed) {
                        expTabelList.remove(model);
                        expIdText.setText("");
                        expIdText.requestFocus();
                        break;
                    }
                    expErrorLabel.setText("Error! Cannot remove entry");
                    expIdText.selectAll();
                    break;
                } catch (RemoteException | SQLException | PropertyVetoException ex) {
                    expErrorLabel.setText("Server Error");
                } catch (ClassNotFoundException ex) {
                    expErrorLabel.setText(ex.getMessage());
                }
            }
        }
    }

    private ChangeListener<String> getIdValidater(final TextField field, final int limit, final BooleanProperty validater) {
        return new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!(newValue == null || newValue.isEmpty())) {
                    if (newValue.length() > limit) {
                        field.setText(oldValue);
                    }
                    if (validater != null) {
                        validater.set(true);
                    }
                } else {
                    if (validater != null) {
                        validater.set(false);
                    }
                }
            }
        };
    }

    /**
     * *
     * Below method for
     *
     * using Request observer to notify ward about the request;
     *
     */
    public void sendStatusForRequest(int requestId, String status) {
        try {
            laborotaryViewObservable.sendStatusForRequest(requestId, status);
            RequestsTableModel model =
                    reqTabelList.get(reqTabelList.indexOf(new RequestsTableModel(status, status, new Request(requestId))));
            model.setStatus(status);
           reqTabelList.remove(model);
           reqTabelList.add(model);
        } catch (RemoteException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Status Updating Failed", "Error");
        }
    }

    @FXML
    public void onReturnClick(ActionEvent event) {
        returnErrorLabel.setText("");
        int type = returnTypeChoice.getSelectionModel().getSelectedIndex();
        switch (type) {
            case (0):
                type = BloodType.PLATELET;
                break;
            case (1):
                type = BloodType.PLASMA;
                break;
            case (2):
                type = BloodType.CSP;
                break;
            case (3):
                type = BloodType.CRYO;
                break;
        }
        try {
            boolean issued = laborotaryViewServices.isBloodIssued(returnBloodText.getText(), type);
            if (!issued) {
                returnErrorLabel.setText("Blood unit is not issued yet");
                returnBloodText.setText("");
                removeReturnsCheck.setSelected(false);
                returnTypeChoice.setDisable(true);
                removeReturnsCheck.setDisable(true);
                submitReturnButton.setDisable(true);
                return;
            }
            if (removeReturnsCheck.isSelected()) {
                DialogResponse confirmation = Dialogs.showConfirmDialog(stage,
                        "Are you sure you want to remove this unit?", "",
                        "Confirm Removal", DialogOptions.YES_NO);
                if (confirmation == DialogResponse.NO) {
                    return;
                }
                boolean removed = laborotaryViewServices.removeBlood(
                        new Blood(returnBloodText.getText(), null, BloodType.getStringOf(type)));
                if (removed) {
                    returnErrorLabel.setText("Succesfully Removed");
                } else {
                    returnErrorLabel.setText("Error! Not Removed");
                }
            } else {
                boolean success = laborotaryViewServices.markReturned(
                        new Blood(returnBloodText.getText(), null, BloodType.getStringOf(type)));
                if (success) {
                    returnErrorLabel.setText("Succesfully Updated");
                } else {
                    returnErrorLabel.setText("Error! Not Updated");
                }
            }
            returnBloodText.setText("");
            removeReturnsCheck.setSelected(false);
            returnTypeChoice.setDisable(true);
            removeReturnsCheck.setDisable(true);
            submitReturnButton.setDisable(true);
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error", "Error");
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Invalid ID", "Error");
        }
    }

    /**
     *
     * When logout or window close below method should shutdown hooked
     */
    public void logout() {
        try {
            if (!reqTabelList.isEmpty()) {
                Dialogs.DialogResponse response = Dialogs.showConfirmDialog(stage,
                        "Are You Sure You Want to Exit?",
                        "There are " + reqTabelList.size() + " Unhandled Requests.",
                        "Confirm Logout", DialogOptions.YES_NO);
                if (response == DialogResponse.NO) {
                    return;
                }
            }
            laborotaryViewObservable.logout();
        } catch (RemoteException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Logout Process Interrupted", "Error");
        }
    }
}
