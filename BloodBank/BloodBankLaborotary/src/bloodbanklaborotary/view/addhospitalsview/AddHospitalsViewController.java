/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view.addhospitalsview;

import bloodbankcommon.model.Receiver;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Dialogs;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Janith
 */
public class AddHospitalsViewController implements Initializable {

    @FXML
    private TextField hospitalId;
    @FXML
    private TextField hospitalName;
    @FXML
    private TextField hospitalAddress;
    @FXML
    private TextField hospitalTelephone;
    private Stage parentStage;
    private AddHospitalServices addHospitalServices;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            addHospitalServices = new AddHospitalServices();
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Dialogs.showErrorDialog(parentStage, ex.getMessage(), "Check server and connection", "Server Error");
        }
        loadNextHospitalId();

        //<editor-fold defaultstate="collapsed" desc="Validation">
        hospitalName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.matches("^(?:(?:[\\p{Upper}][\\p{Lower}]{3,15})[\\s]?){2,}$")) {
                    hospitalName.setText(newValue);
                } else {
                    hospitalName.setText(oldValue);
                }
            }
        });

        hospitalAddress.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.matches("^(?:(?:[\\p{Upper}][\\p{Lower}]{3,15})[\\s]?){2,}$")) {
                    hospitalAddress.setText(newValue);
                } else {
                    hospitalAddress.setText(oldValue);
                }
            }
        });

        hospitalTelephone.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.matches("^(\\d){9}$")) {
                    hospitalTelephone.setText(newValue);
                } else {
                    hospitalTelephone.setText(oldValue);
                }
            }
        });
        //</editor-fold>
    }

    public void SaveHospital() {
        try {
            String hId = hospitalId.getText();
            String hName = hospitalName.getText();
            String hAddress = hospitalAddress.getText();
            String hTelephone = hospitalTelephone.getText();
            Receiver receiver = new Receiver(hId, hName, hAddress, Integer.parseInt(hTelephone));
            boolean status = addHospitalServices.addHospital(receiver);
            if (status) {
                Dialogs.showInformationDialog(null, "Successfully Added");
                hospitalAddress.setText("");
                hospitalName.setText("");
                hospitalTelephone.setText("");
                loadNextHospitalId();
            } else {
                Dialogs.showErrorDialog(null, "Failed, Try Again");
            }
        } catch (RemoteException | ClassNotFoundException | SQLException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(parentStage, ex.getMessage(), "Check server and connection", "Server Error");
        }
    }

    private void loadNextHospitalId() {
        try {
            String nextHospitalId = addHospitalServices.getNextHospitalId();
            if (nextHospitalId == null) {
                nextHospitalId = "H000";
            } else {
                String no = nextHospitalId.substring(1);
                int newId = Integer.parseInt(no) + 1;
                nextHospitalId = "H" + newId;
            }
            hospitalId.setText(nextHospitalId);
        } catch (RemoteException | ClassNotFoundException | SQLException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(parentStage, ex.getMessage(), "Check server and connection", "Server Error");
        }
    }

    public void setParentStage(Stage stage) {
        this.parentStage = stage;
    }
}