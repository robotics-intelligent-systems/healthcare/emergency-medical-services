/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view.addhospitalsview;

import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.model.Receiver;
import bloodbanklaborotary.connector.ServerConnector;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 *
 * @author oshanz
 */
public class AddHospitalServices {

    private ReceiverControllerIntf receiverControllerIntf;

    public AddHospitalServices() throws NotBoundException, MalformedURLException, RemoteException {
        receiverControllerIntf = ServerConnector.getServerConnector().getReceiverControllerIntf();
    }

    String getNextHospitalId() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverControllerIntf.getNextHospitalId();
    }

    boolean addHospital(Receiver receiver) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverControllerIntf.addHospital(receiver);
    }
}
