/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view.requestdetailformatter;

import bloodbankcommon.bloodtype.CRYO;
import bloodbankcommon.bloodtype.CSP;
import bloodbankcommon.bloodtype.FFPlasma;
import bloodbankcommon.bloodtype.Platelet;
import bloodbankcommon.bloodtype.RedCell;
import bloodbankcommon.model.Request;

/**
 *
 * @author Janith
 */
public class RequestFormatter {
    
    public static String getRedCellString(Request request){
        RedCell blood = (RedCell) request.getBlood();
        String transHisString =
                blood.isTransfusionHistory() ? (blood.isWithinLast_3_Months() ? 
                "Within Last 3 Months" : "Before Last 3 Months") : "None";
        String reactions = blood.getReactionDescription() == null ? "None" : "blood.getReactionDescription()";
        String oHistory = blood.isObstetricHistory() ? "Yes" : "None";
        String parity = blood.isObstetricHistory() ? blood.getParity() != null ? blood.getParity() : "None" : "None";
        String currIndic = blood.getTransfusionIndication() != null ? blood.getTransfusionIndication() : "None";
        String hb = blood.getHbLevel() != null ? blood.getHbLevel() : "None";
        String loss = blood.getBloodLoss() != null ? blood.getBloodLoss() : "None";
        String proc = blood.getProcedure() != null ? blood.getProcedure() : "None";
        String spReqRed = blood.getSpecialReqirement().toString();
        String spReq = request.getSpecialReq() != null ? request.getSpecialReq() : "None";
        String reqAmount = request.getUnits() + "";
                
        String redCellString = "Request ID : " + request.getRequestId() +
                "\nPatient ID : " + request.getPatientId() +
                "\nWard Number : " + request.getWardId() +
                "\nRequested Date : " + request.getDate() +
                "\nPatient's Blood Group : " + request.getGroup() +
                "\n\n-------------- Blood Specifications -------------- " +
                "\n\n\tPriority : " + blood.getPriority().toString() +
                "\n\tTransfusion History : " + transHisString +
                "\n\t\tReactions : " + reactions +
                "\n\tObstetric History : " + oHistory +
                "\n\t\tParity : " + parity +
                "\n\tCurrent Indication for Transfusion : " + currIndic +
                "\n\t\tIf Anemic, HB Level : " + hb +
                "\n\t\tApproximate Blood Loss : " + loss +
                "\n\tSurgery/Procedure : " + proc +
                "\n\tSpecial Requirement : " + spReqRed +
                "\nSpecial Requirements(other) : " + spReq +
                "\n\nRequired Amount : " + reqAmount + "packs";
        
        return redCellString;
    }
    
    public static String getPlateletString(Request request){
        
        Platelet pl = (Platelet) request.getBlood();
        String spReq = request.getSpecialReq() != null ? request.getSpecialReq() : "None";
        String reqAmount = request.getUnits() + "";
        
        String plateletString = "Request ID : " + request.getRequestId() +
                "\nPatient ID : " + request.getPatientId() +
                "\nWard Number : " + request.getWardId() +
                "\nRequested Date : " + request.getDate() +
                "\nPatient's Blood Group : " + request.getGroup() +
                "\n\n-------------- Blood Specifications -------------- " +
                "\n\n\tBM Failure : " + (pl.isBmFailure() ? "Yes" : "No") +
                "\n\tBM Failure With Risk Factors : " + (pl.isBmFailureWithRiscFactors()? "Yes" : "No") +
                "\n\tBM Failure With Bleeding: " + (pl.isBmFailureWithBleeding()? "Yes" : "No") +
                "\n\tAcute DIC With Bleeding : " + (pl.isAcutDicWithBleeding()? "Yes" : "No") +
                "\n\tAfter Massive Transfusion : " + (pl.isAfterMassiveTransfusions()? "Yes" : "No") +
                "\n\tSurgery/Invasive Procedure : " + (pl.isAfterMassiveTransfusions()? "Yes" : "No") +
                "\n\tEye/CNS : " + (pl.isCns()? "Yes" : "No") +
                "\n\tPlatelet Function Defect with Bleeding or Risk of Bleeding : " + (pl.isPlateletFunctionDefect()? "Yes" : "No") +
                "\n\tAny Other : " + (pl.getOther() != null ? pl.getOther() : "None") +
                "\n\tPlatelet Count : " + (pl.getCount() + "") +
                "\nSpecial Requirements : " + spReq +
                "\n\nRequired Amount : " + reqAmount + "packs";
        
        return plateletString;
    }
    
    public static String getFfpString(Request request){
        
        FFPlasma ff = (FFPlasma) request.getBlood();
        String spReq = request.getSpecialReq() != null ? request.getSpecialReq() : "None";
        String reqAmount = request.getUnits() + "";
        
        String plateletString = "Request ID : " + request.getRequestId() +
                "\nPatient ID : " + request.getPatientId() +
                "\nWard Number : " + request.getWardId() +
                "\nRequested Date : " + request.getDate() +
                "\nPatient's Blood Group : " + request.getGroup() +
                "\n\n-------------- Blood Specifications -------------- " +
                "\n\n\tBleeding or Risk of Bleeding due to Coagulopathy " +
                "\n\t\tAcute DIC : " + (ff.isAcutDic() ? "Yes" : "No") +
                "\n\t\tMassive Transfusion : " + (ff.isMassiveTransfusion() ? "Yes" : "No") +
                "\n\t\tLiver Disease : " + (ff.isLiverDisease() ? "Yes" : "No") +
                "\n\t\tSurgery/Invasive Procedure : " + (ff.isSurgeryProcedure() ? "Yes" : "No") +
                "\n\tEmergency Reversal of Warfarin Effect : " + (ff.isWarfarinEffect() ? "Yes" : "No") +
                "\n\tSingle Clotting Factor : " + (ff.isClottingFactor() ? "Yes" : "No") +
                "\n\tAny Other : " + (ff.getOther() != null ? ff.getOther() : "None") +
                "\n\tINR/PT : " + (ff.getInr() != null ? ff.getInr() : "None") +
                "\n\tAPIT : " + (ff.getAptt()!= null ? ff.getAptt(): "None") +
                "\nSpecial Requirements : " + spReq +
                "\n\nRequired Amount : " + reqAmount + "packs";
        
        return plateletString;
    }
    
    public static String getCryoString(Request request){
        
        CRYO cr = (CRYO) request.getBlood();
        String spReq = request.getSpecialReq() != null ? request.getSpecialReq() : "None";
        String reqAmount = request.getUnits() + "";
        
        String plateletString = "Request ID : " + request.getRequestId() +
                "\nPatient ID : " + request.getPatientId() +
                "\nWard Number : " + request.getWardId() +
                "\nRequested Date : " + request.getDate() +
                "\nPatient's Blood Group : " + request.getGroup() +
                "\n\n-------------- Blood Specifications -------------- " +
                "\n\n\tBleeding or Risk of Bleeding with Low Fibrinogen " +
                "\n\t\tAcute DIC : " + (cr.isAcutDic() ? "Yes" : "No") +
                "\n\t\tMassive Transfusion : " + (cr.isMassiveTransfusion() ? "Yes" : "No") +
                "\n\t\tLiver Disease : " + (cr.isLiverDisease() ? "Yes" : "No") +
                "\n\tIn Von Willibrand Desease : " + (cr.isWillibrandDesease() ? "Yes" : "No") +
                "\n\tIn Haemophilia A : " + (cr.isHaemophilia() ? "Yes" : "No") +
                "\n\tAny Other : " + (cr.getOther() != null ? cr.getOther() : "None") +
                "\n\tAPIT : " + (cr.getAptt()!= null ? cr.getAptt(): "None") +
                "\nSpecial Requirements : " + spReq +
                "\n\nRequired Amount : " + reqAmount + "packs";
        
        return plateletString;
    }
    
    public static String getCspString(Request request){
        
        CSP cs = (CSP) request.getBlood();
        String spReq = request.getSpecialReq() != null ? request.getSpecialReq() : "None";
        String reqAmount = request.getUnits() + "";
        
        String plateletString = "Request ID : " + request.getRequestId() +
                "\nPatient ID : " + request.getPatientId() +
                "\nWard Number : " + request.getWardId() +
                "\nRequested Date : " + request.getDate() +
                "\nPatient's Blood Group : " + request.getGroup() +
                "\n\n-------------- Blood Specifications -------------- " +
                "\n\n\tTTP : " + (cs.isTtp() ? "Yes" : "No") +
                "\n\t\tIn Haemophilia B : " + (cs.isInHaemophiliaB() ? "Yes" : "No") +
                "\n\n\tWhen Albumin not Available for Management of " +
                "\n\t\tLiver Disease : " + (cs.isLiverDisease() ? "Yes" : "No") +
                "\n\tBurns : " + (cs.isBurns() ? "Yes" : "No") +
                "\n\tNephrotic. Syndrome : " + (cs.isNephroticSyndrome() ? "Yes" : "No") +
                "\n\tAny Other : " + (cs.getOther() != null ? cs.getOther() : "None") +
                "\n\tSerun Albumin/Protein : " + (cs.getAlbumin()!= null ? cs.getAlbumin(): "None") +
                "\nSpecial Requirements : " + spReq +
                "\n\nRequired Amount : " + reqAmount + "packs";
        
        return plateletString;
    }
}
