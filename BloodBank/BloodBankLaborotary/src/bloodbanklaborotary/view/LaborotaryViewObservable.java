/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view;

import bloodbankcommon.model.Request;
import bloodbankcommon.observer.LabObserver;
import bloodbankcommon.observer.RequestObserver;
import bloodbankcommon.observer.notifier.LabNotifier;
import bloodbankcommon.request.RequestHandlerIntf;
import bloodbanklaborotary.connector.ServerConnector;
import bloodbanklaborotary.observerimpl.LabObserverImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Properties;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.control.Dialogs;
import javafx.stage.Stage;

/**
 *
 * @author Janith
 */
public class LaborotaryViewObservable implements LabNotifier {

    protected ObservableMap<Request, RequestObserver> hMap_RequestId_ObservRequests;
    private LabObserver labObserver;
    private RequestHandlerIntf requestHandler;
    protected int labId;
    private Stage stage;

    LaborotaryViewObservable(LaborotaryViewController viewController, Stage stage) throws RemoteException, NotBoundException, MalformedURLException, IOException {
        this.requestHandler = ServerConnector.getServerConnector().getRequestHandlerIntf();
        this.hMap_RequestId_ObservRequests = FXCollections.observableHashMap();
        this.labId = loadMyId();
        this.labObserver = new LabObserverImpl(this, labId);
        this.stage = stage;
        requestHandler.addLab(labObserver, labId);
    }

    @Override
    public void addRequest(RequestObserver requestObserver, Request request) {
        hMap_RequestId_ObservRequests.put(request, requestObserver);
    }

    @Override
    public void cancelRequest(int requestId) {
        Request request = new Request(requestId);
        hMap_RequestId_ObservRequests.remove(request);
    }

    void logout() throws RemoteException {
    }

    void sendStatusForRequest(final int requestId, final String status) throws RemoteException {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Request fakeRequest = new Request(requestId);
                    RequestObserver observer = hMap_RequestId_ObservRequests.get(fakeRequest);
                    observer.sendStatus(requestId, status);
                } catch (RemoteException ex) {
                    Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error", "Error");
                }
            }
        });
    }

    private int loadMyId() throws IOException {
        Properties properties = new Properties();
        File file = new File("MyConfig.data");
        boolean fileCreated = file.createNewFile();
        int num = 0;
        if (fileCreated) {
            String response = Dialogs.showInputDialog(stage, "Please Enter the Ward Number", "Configuration file missing");
            try {
                num = Integer.parseInt(response);
                properties.setProperty("myId", num + "");
                properties.store(new FileOutputStream(file), "");
            } catch (NumberFormatException ex) {
            }
        }
        properties.load(new FileInputStream(file));
        String property = properties.getProperty("myId") == null ? num + "" : properties.getProperty("myId");
        return Integer.parseInt(property);
    }
}
