/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.view;

import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Receiver;
import bloodbankcommon.model.Seperation;
import bloodbanklaborotary.connector.ServerConnector;
import bloodbanklaborotary.model.SeperationTabelModel;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class LoborotaryViewServices {

    private DonationControllerIntf donationController;
    private DonorControllerIntf donorController;
    private SeperationControllerIntf seperationController;
    private BloodTypeControllerIntf bloodTypeController;
    private ReceiverControllerIntf receiverController;

    public LoborotaryViewServices() throws NotBoundException, RemoteException, MalformedURLException {
        this.donorController = ServerConnector.getServerConnector().getDonorControllerIntf();
        this.donationController = ServerConnector.getServerConnector().getDonationControllerIntf();
        this.seperationController = ServerConnector.getServerConnector().getSeperationControllerIntf();
        this.bloodTypeController = ServerConnector.getServerConnector().getBloodTypeControllerIntf();
        this.receiverController = ServerConnector.getServerConnector().getReceiverControllerIntf();
    }

    SeperationTabelModel searchBlood(String bloodId) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {

        String bloodType = donorController.getBloodGroup(bloodId);
        String bloodGroup = donationController.getBloodType(bloodId);
        SeperationTabelModel labBloodSeperation = null;
        if (bloodType != null && bloodGroup != null) {
            labBloodSeperation = new SeperationTabelModel(bloodId, bloodGroup, Integer.valueOf(bloodType));
        }
        return labBloodSeperation;
    }

    Donation getDonationSimple(String bloodId) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return donationController.getDonnationFromBloodIdSimple(bloodId);
    }

    String getBloodGroup(String nic) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return donorController.getBloodGroup(nic);
    }

    boolean submitSeperation(Seperation seperation, List<Blood> bloodList) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return seperationController.submitSeperation(seperation, bloodList);
    }

    List<Blood> getExpiredList() throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        List<Blood> expiredList = bloodTypeController.getExpiredRedCells();
        expiredList.addAll(bloodTypeController.getExpiredPlatelets());
        expiredList.addAll(bloodTypeController.getExpiredPlasma());
        expiredList.addAll(bloodTypeController.getExpiredCryo());
        expiredList.addAll(bloodTypeController.getExpiredCsp());
        return expiredList;
    }

    boolean removeBlood(Blood blood) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return bloodTypeController.removeBlood(blood);
    }
    
    Receiver getHospital(String hospitalId) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return receiverController.getHospital(hospitalId);
    }
    
    boolean updateHospital(Receiver hospital) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return receiverController.updateHospital(hospital);
    }

    boolean addHospital(Receiver hospital) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return receiverController.addHospital(hospital);
    }

    Blood getBlood(String bloodId, int type) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return bloodTypeController.getBlood(bloodId, type);
    }

    boolean submitOutsourcings(String hospId, List<Blood> bloodList) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException{
        return receiverController.submitOutsourcings(hospId, bloodList);
    }

    boolean rejectDonor(String nic, String bloodId, String reason) throws RemoteException, SQLException, ClassNotFoundException {
        //Paused because AutoCommit Issue
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    boolean rejectDonation(String bloodId, String reason) throws RemoteException, SQLException, ClassNotFoundException {
        //Paused because AutoCommit Issue
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    boolean isBloodIssued(String bloodId, int type) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return bloodTypeController.isBloodIssued(bloodId, type);
    }

    boolean markReturned(Blood blood) throws RemoteException, SQLException, ClassNotFoundException, PropertyVetoException {
        return bloodTypeController.markReturned(blood);
    }
}
