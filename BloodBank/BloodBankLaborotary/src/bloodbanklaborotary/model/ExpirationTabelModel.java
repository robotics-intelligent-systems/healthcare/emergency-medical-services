/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janith
 */
public class ExpirationTabelModel {
    private StringProperty id;
    private StringProperty catagory;

    public ExpirationTabelModel(String id, String catagory) {
        this.id = new SimpleStringProperty(id);
        this.catagory = new SimpleStringProperty(catagory);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getCatagory() {
        return catagory.get();
    }

    public void setCatagory(String catagory) {
        this.catagory.set(catagory);
    }
    
}
