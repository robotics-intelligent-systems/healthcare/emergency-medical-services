/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.model;

/**
 *
 * @author oshanz
 */
public class SeperationTabelModel {
    private String donationId;
    private String bloodGroup;
    private int bloodType;

    public SeperationTabelModel(String donationId, String bloodGroup, int bloodType) {
        this.donationId = donationId;
        this.bloodGroup = bloodGroup;
        this.bloodType = bloodType;
    }

    public String getDonationId() {
        return donationId;
    }

    public void setDonationId(String donationId) {
        this.donationId = donationId;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public int getBloodType() {
        return bloodType;
    }

    public void setBloodType(int bloodType) {
        this.bloodType = bloodType;
    }
}
