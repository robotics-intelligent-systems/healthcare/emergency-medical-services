/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.model;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.model.Request;
import bloodbanklaborotary.view.requestdetailformatter.RequestFormatter;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janith
 */
public class RequestsTableModel {

    private Request request;
    private StringProperty wardNumber = new SimpleStringProperty();
    private StringProperty priority = new SimpleStringProperty();
    private StringProperty status = new SimpleStringProperty();
    private String detailString;

    public RequestsTableModel(String wardNumber, String priority, Request request) {
        this.wardNumber = new SimpleStringProperty(wardNumber);
        this.priority = new SimpleStringProperty(priority);
        this.request = request;
        if (request.getBlood() != null) {
            int blootType = request.getBlood().getBlootTypeIndex();
            if (blootType == BloodType.RED_CELL) {
                detailString = RequestFormatter.getRedCellString(request);
            } else if (blootType == BloodType.PLATELET) {
                detailString = RequestFormatter.getPlateletString(request);
            } else if (blootType == BloodType.PLASMA) {
                detailString = RequestFormatter.getFfpString(request);
            } else if (blootType == BloodType.CRYO) {
                detailString = RequestFormatter.getCryoString(request);
            } else if (blootType == BloodType.CSP) {
                detailString = RequestFormatter.getCspString(request);
            }
        }
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getWardNumber() {
        return wardNumber.get();
    }

    public void setWardNumber(String wardNumber) {
        this.wardNumber.set(wardNumber);
    }

    public String getPriority() {
        return priority.get();
    }

    public void setPriority(String priority) {
        this.priority.set(priority);
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getDetailString() {
        return detailString;
    }

    public void setDetailString(String detailString) {
        this.detailString = detailString;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RequestsTableModel other = (RequestsTableModel) obj;
        if (!Objects.equals(this.request, other.request)) {
            return false;
        }
        return true;
    }
}
