/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbanklaborotary.observerimpl;

import bloodbankcommon.model.Request;
import bloodbankcommon.observer.LabObserver;
import bloodbankcommon.observer.RequestObserver;
import bloodbankcommon.observer.notifier.LabNotifier;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author oshanz
 */
public class LabObserverImpl extends UnicastRemoteObject implements LabObserver {

    private LabNotifier labNotifier;
    private int labId;

    public LabObserverImpl(LabNotifier labNotifier, int labId) throws RemoteException {
        this.labNotifier = labNotifier;
        this.labId = labId;
    }

    @Override
    public void addRequest(final RequestObserver requestObserver, final Request request) throws RemoteException {
        labNotifier.addRequest(requestObserver, request);
    }

    @Override
    public void cancleRequest(final int requestId) throws RemoteException {
        labNotifier.cancelRequest(requestId);
    }

    @Override
    public int getLabId() {
        return labId;
    }
}
