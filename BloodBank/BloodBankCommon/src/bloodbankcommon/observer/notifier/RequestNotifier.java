/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer.notifier;

/**
 *
 * @author oshanz
 */
public interface RequestNotifier {

    public void setRequestStatus(int requestId, String status);

}
