/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer.notifier;

import bloodbankcommon.model.Blood;

/**
 *
 * @author oshanz
 */
public interface BloodNotifier {

    public void setBloodNotification(Blood blood);
}
