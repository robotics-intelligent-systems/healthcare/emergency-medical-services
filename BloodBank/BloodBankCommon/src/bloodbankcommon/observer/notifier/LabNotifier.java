/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer.notifier;

import bloodbankcommon.model.Request;
import bloodbankcommon.observer.RequestObserver;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface LabNotifier {

    public void addRequest(RequestObserver requestObserver, Request request) throws RemoteException;

    public void cancelRequest(int requestId) throws RemoteException;
}
