/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer.notifier;

import bloodbankcommon.model.Donor;

/**
 *
 * @author oshanz
 */
public interface DonorNotifier {

    public void setDonorNotifier(Donor donor);
}
