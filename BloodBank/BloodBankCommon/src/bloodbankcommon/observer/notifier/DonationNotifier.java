/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer.notifier;

import bloodbankcommon.model.Donation;

/**
 *
 * @author oshanz
 */
public interface DonationNotifier {
    public void setDonationNotification(Donation donation);
            
}
