/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import bloodbankcommon.model.Request;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface LabObserver extends Remote {

    public void addRequest(RequestObserver requestObserver, Request request) throws RemoteException;

    public void cancleRequest(int requestId) throws RemoteException;

    public int getLabId() throws RemoteException;
}
