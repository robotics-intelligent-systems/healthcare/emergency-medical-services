/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface RequestObserver extends Remote {

    public void sendStatus(int requestId, String status) throws RemoteException;

    public int getWardId() throws RemoteException;
}
