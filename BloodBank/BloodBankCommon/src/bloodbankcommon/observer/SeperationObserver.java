/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import bloodbankcommon.model.Seperation;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface SeperationObserver extends Remote {

    public void update(Seperation seperation) throws RemoteException;
}
