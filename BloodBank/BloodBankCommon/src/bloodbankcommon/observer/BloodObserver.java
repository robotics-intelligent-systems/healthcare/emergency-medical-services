/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import bloodbankcommon.model.Blood;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface BloodObserver extends Remote {

    public void update(Blood blood) throws RemoteException;
    
}
