/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import bloodbankcommon.model.Donor;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface DonorObserver extends Remote {

    public void update(Donor donor) throws RemoteException;
}
