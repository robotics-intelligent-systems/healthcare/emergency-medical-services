/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.observer;

import bloodbankcommon.model.Donation;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface DonationObserver extends Remote {

    public void update(Donation donation) throws RemoteException;
}
