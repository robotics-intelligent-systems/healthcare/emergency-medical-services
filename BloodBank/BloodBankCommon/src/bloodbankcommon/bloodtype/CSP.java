/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

/**
 *
 * @author oshanz
 */
public class CSP extends BloodType{
    
    private boolean ttp;
    private boolean inHaemophiliaB;
    private boolean burns;
    private boolean liverDisease;
    private boolean nephroticSyndrome;
    private String other;
    private String albumin;

    public CSP(boolean ttp, boolean inHaemophiliaB, boolean burns, boolean liverDisease, boolean nephroticSyndrome, String other, String albumin) {
        this.ttp = ttp;
        this.inHaemophiliaB = inHaemophiliaB;
        this.burns = burns;
        this.liverDisease = liverDisease;
        this.nephroticSyndrome = nephroticSyndrome;
        this.other = other;
        this.albumin = albumin;
    }

    @Override
    public String toString() {
        return "CSP";
    }

    @Override
    public int getBlootTypeIndex() {
        return CSP;
    }

    public boolean isTtp() {
        return ttp;
    }

    public void setTtp(boolean ttp) {
        this.ttp = ttp;
    }

    public boolean isInHaemophiliaB() {
        return inHaemophiliaB;
    }

    public void setInHaemophiliaB(boolean inHaemophiliaB) {
        this.inHaemophiliaB = inHaemophiliaB;
    }

    public boolean isBurns() {
        return burns;
    }

    public void setBurns(boolean burns) {
        this.burns = burns;
    }

    public boolean isLiverDisease() {
        return liverDisease;
    }

    public void setLiverDisease(boolean liverDisease) {
        this.liverDisease = liverDisease;
    }

    public boolean isNephroticSyndrome() {
        return nephroticSyndrome;
    }

    public void setNephroticSyndrome(boolean nephroticSyndrome) {
        this.nephroticSyndrome = nephroticSyndrome;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getAlbumin() {
        return albumin;
    }

    public void setAlbumin(String albumin) {
        this.albumin = albumin;
    }
    
}
