/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

import java.io.Serializable;

/**
 *
 * @author Janith
 */
public enum BloodTypes implements Serializable{

    WHOLE_BLOOD(0, "Whole Blood"), RED_CELL(1, "Red Cell"),
    PLATELET(2, "Platelet"), PLASMA(3, "Plasma"), CRYO(4, "CRYO"), CSP(5, "CSP");
    private int index;
    private String name;

    private BloodTypes(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
