/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

/**
 *
 * @author oshanz
 */
public class FFPlasma extends BloodType{
    
    private boolean acutDic;
    private boolean massiveTransfusion;
    private boolean liverDisease;
    private boolean surgeryProcedure;
    private boolean warfarinEffect;
    private boolean clottingFactor;
    private String other;
    private String inr;
    private String aptt;

    public FFPlasma(boolean acutDic, boolean massiveTransfusion, boolean liverDisease, boolean surgeryProcedure, boolean warfarinEffect, boolean clottingFactor, String other, String inr, String aptt) {
        this.acutDic = acutDic;
        this.massiveTransfusion = massiveTransfusion;
        this.liverDisease = liverDisease;
        this.surgeryProcedure = surgeryProcedure;
        this.warfarinEffect = warfarinEffect;
        this.clottingFactor = clottingFactor;
        this.other = other;
        this.inr = inr;
        this.aptt = aptt;
    }

    public boolean isAcutDic() {
        return acutDic;
    }

    public void setAcutDic(boolean acutDic) {
        this.acutDic = acutDic;
    }

    public boolean isMassiveTransfusion() {
        return massiveTransfusion;
    }

    public void setMassiveTransfusion(boolean massiveTransfusion) {
        this.massiveTransfusion = massiveTransfusion;
    }

    public boolean isLiverDisease() {
        return liverDisease;
    }

    public void setLiverDisease(boolean liverDisease) {
        this.liverDisease = liverDisease;
    }

    public boolean isSurgeryProcedure() {
        return surgeryProcedure;
    }

    public void setSurgeryProcedure(boolean surgeryProcedure) {
        this.surgeryProcedure = surgeryProcedure;
    }

    public boolean isWarfarinEffect() {
        return warfarinEffect;
    }

    public void setWarfarinEffect(boolean warfarinEffect) {
        this.warfarinEffect = warfarinEffect;
    }

    public boolean isClottingFactor() {
        return clottingFactor;
    }

    public void setClottingFactor(boolean clottingFactor) {
        this.clottingFactor = clottingFactor;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getInr() {
        return inr;
    }

    public void setInr(String inr) {
        this.inr = inr;
    }

    public String getAptt() {
        return aptt;
    }

    public void setAptt(String aptt) {
        this.aptt = aptt;
    }
    
    @Override
    public String toString() {
        return "FFPlasma";
    }

    @Override
    public int getBlootTypeIndex() {
        return PLASMA;
    }
    
}
