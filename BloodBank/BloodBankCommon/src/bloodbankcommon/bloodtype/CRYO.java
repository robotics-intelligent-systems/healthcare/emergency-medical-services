/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

import static bloodbankcommon.bloodtype.BloodType.CRYO;

/**
 *
 * @author oshanz
 */
public class CRYO extends BloodType {

    private boolean acutDic;
    private boolean massiveTransfusion;
    private boolean liverDisease;
    private boolean willibrandDesease;
    private boolean haemophilia;
    private String other;
    private String aptt;

    public CRYO(boolean acutDic, boolean massiveTransfusion, boolean liverDisease, boolean willibrandDesease, boolean haemophilia, String other, String aptt) {
        this.acutDic = acutDic;
        this.massiveTransfusion = massiveTransfusion;
        this.liverDisease = liverDisease;
        this.willibrandDesease = willibrandDesease;
        this.haemophilia = haemophilia;
        this.other = other;
        this.aptt = aptt;
    }

    public boolean isAcutDic() {
        return acutDic;
    }

    public void setAcutDic(boolean acutDic) {
        this.acutDic = acutDic;
    }

    public boolean isMassiveTransfusion() {
        return massiveTransfusion;
    }

    public void setMassiveTransfusion(boolean massiveTransfusion) {
        this.massiveTransfusion = massiveTransfusion;
    }

    public boolean isLiverDisease() {
        return liverDisease;
    }

    public void setLiverDisease(boolean liverDisease) {
        this.liverDisease = liverDisease;
    }

    public boolean isWillibrandDesease() {
        return willibrandDesease;
    }

    public void setWillibrandDesease(boolean willibrandDesease) {
        this.willibrandDesease = willibrandDesease;
    }

    public boolean isHaemophilia() {
        return haemophilia;
    }

    public void setHaemophilia(boolean haemophilia) {
        this.haemophilia = haemophilia;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getAptt() {
        return aptt;
    }

    public void setAptt(String aptt) {
        this.aptt = aptt;
    }

    @Override
    public String toString() {
        return "CRYO";
    }

    @Override
    public int getBlootTypeIndex() {
        return CRYO;
    }
}
