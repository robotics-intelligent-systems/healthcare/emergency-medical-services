/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

import bbcommon.Priority;
import bbcommon.RedCellSpecialReq;

/**
 *
 * @author oshanz
 */
public class RedCell extends BloodType{

    private Priority priority;
    private boolean transfusionHistory;
    private boolean withinLast_3_Months;
    private boolean transfusionReactions;
    private String reactionDescription;
    private boolean obstetricHistory;
    private String parity;
    private String transfusionIndication;
    private String hbLevel;
    private String bloodLoss;    
    private String procedure;    
    private RedCellSpecialReq specialReqirement;    
    
    public RedCell(Priority priority, boolean transfusionHistory, boolean withinLast_3_Months, boolean transfusionReactions, String reactionDescription, boolean obstetricHistory, String parity, String transfusionIndication, String hbLevel, String bloodLoss, String procedure, RedCellSpecialReq specialReqirement) {
        this.priority = priority;
        this.transfusionHistory = transfusionHistory;
        this.withinLast_3_Months = withinLast_3_Months;
        this.transfusionReactions = transfusionReactions;
        this.reactionDescription = reactionDescription;
        this.obstetricHistory = obstetricHistory;
        this.parity = parity;
        this.transfusionIndication = transfusionIndication;
        this.hbLevel = hbLevel;
        this.bloodLoss = bloodLoss;
        this.procedure = procedure;
        this.specialReqirement = specialReqirement;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public boolean isTransfusionHistory() {
        return transfusionHistory;
    }

    public void setTransfusionHistory(boolean transfusionHistory) {
        this.transfusionHistory = transfusionHistory;
    }

    public boolean isWithinLast_3_Months() {
        return withinLast_3_Months;
    }

    public void setWithinLast_3_Months(boolean withinLast_3_Months) {
        this.withinLast_3_Months = withinLast_3_Months;
    }

    public boolean isTransfusionReactions() {
        return transfusionReactions;
    }

    public void setTransfusionReactions(boolean transfusionReactions) {
        this.transfusionReactions = transfusionReactions;
    }

    public String getReactionDescription() {
        return reactionDescription;
    }

    public void setReactionDescription(String reactionDescription) {
        this.reactionDescription = reactionDescription;
    }

    public boolean isObstetricHistory() {
        return obstetricHistory;
    }

    public void setObstetricHistory(boolean obstetricHistory) {
        this.obstetricHistory = obstetricHistory;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }

    public String getTransfusionIndication() {
        return transfusionIndication;
    }

    public void setTransfusionIndication(String transfusionIndication) {
        this.transfusionIndication = transfusionIndication;
    }

    public String getHbLevel() {
        return hbLevel;
    }

    public void setHbLevel(String hbLevel) {
        this.hbLevel = hbLevel;
    }

    public String getBloodLoss() {
        return bloodLoss;
    }

    public void setBloodLoss(String bloodLoss) {
        this.bloodLoss = bloodLoss;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public RedCellSpecialReq getSpecialReqirement() {
        return specialReqirement;
    }

    public void setSpecialReqirement(RedCellSpecialReq specialReqirement) {
        this.specialReqirement = specialReqirement;
    }
    
    @Override
    public String toString() {
        return "RedCell";
    }

    @Override
    public int getBlootTypeIndex() {
        return RED_CELL;
    }
    
}
