/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

import java.io.Serializable;

/**
 *
 * @author Janith
 */
public class BloodType implements Serializable {

    public static final int WHOLE_BLOOD = 0;
    public static final int RED_CELL = 1;
    public static final int PLATELET = 2;
    public static final int PLASMA = 3;
    public static final int CRYO = 4;
    public static final int CSP = 5;
    //ExpiaryDays (No. of Days)
    public static final int RED_CELL_EXPIARY = 35;
    public static final int PLATELET_EXPIARY = 5;
    public static final int PLASMA_EXPIARY = 365;
    public static final int CRYO_EXPIARY = 365;
    public static final int CSP_EXPIARY = 365;
    // whole blood must seperate within 8 hours. if not expire. 

    public static String getStringOf(int index) {
        switch (index) {
            case 0:
                return "Whole Blood";
            case 1:
                return "Red Cell";
            case 2:
                return "Platelet";
            case 3:
                return "Plasma";
            case 4:
                return "CRYO";
            case 5:
                return "CSP";
            default:
                return "invalid index";
        }
    }

    public static int getIndexOf(String type) {
        switch (type) {
            case "Whole Blood":
                return 0;
            case "Red Cell":
                return 1;
            case "Platelet":
                return 2;
            case "Plasma":
                return 3;
            case "CRYO":
                return 4;
            case "CSP":
                return 5;
            default:
                return -1;
        }
    }

    public int getBlootTypeIndex() {
        return -1;
    }
}