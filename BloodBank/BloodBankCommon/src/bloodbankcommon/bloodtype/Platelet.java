/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.bloodtype;

/**
 *
 * @author oshanz
 */
public class Platelet extends BloodType{

    private boolean bmFailure;
    private boolean bmFailureWithRiscFactors;
    private boolean bmFailureWithBleeding;
    private boolean acutDicWithBleeding;
    private boolean afterMassiveTransfusions;
    private boolean surgeryProcedure;
    private boolean cns;
    private boolean plateletFunctionDefect;
    private String other;
    private int count;

    public Platelet(boolean bmFailure, boolean bmFailureWithRiscFactors, boolean bmFailureWithBleeding, boolean acutDicWithBleeding, boolean afterMassiveTransfusions, boolean surgeryProcedure, boolean cns, boolean plateletFunctionDefect, String other, int count) {
        this.bmFailure = bmFailure;
        this.bmFailureWithRiscFactors = bmFailureWithRiscFactors;
        this.bmFailureWithBleeding = bmFailureWithBleeding;
        this.acutDicWithBleeding = acutDicWithBleeding;
        this.afterMassiveTransfusions = afterMassiveTransfusions;
        this.surgeryProcedure = surgeryProcedure;
        this.cns = cns;
        this.plateletFunctionDefect = plateletFunctionDefect;
        this.other = other;
        this.count = count;
    }

    public boolean isBmFailure() {
        return bmFailure;
    }

    public void setBmFailure(boolean bmFailure) {
        this.bmFailure = bmFailure;
    }

    public boolean isBmFailureWithRiscFactors() {
        return bmFailureWithRiscFactors;
    }

    public void setBmFailureWithRiscFactors(boolean bmFailureWithRiscFactors) {
        this.bmFailureWithRiscFactors = bmFailureWithRiscFactors;
    }

    public boolean isBmFailureWithBleeding() {
        return bmFailureWithBleeding;
    }

    public void setBmFailureWithBleeding(boolean bmFailureWithBleeding) {
        this.bmFailureWithBleeding = bmFailureWithBleeding;
    }

    public boolean isAcutDicWithBleeding() {
        return acutDicWithBleeding;
    }

    public void setAcutDicWithBleeding(boolean acutDicWithBleeding) {
        this.acutDicWithBleeding = acutDicWithBleeding;
    }

    public boolean isAfterMassiveTransfusions() {
        return afterMassiveTransfusions;
    }

    public void setAfterMassiveTransfusions(boolean afterMassiveTransfusions) {
        this.afterMassiveTransfusions = afterMassiveTransfusions;
    }

    public boolean isSurgeryProcedure() {
        return surgeryProcedure;
    }

    public void setSurgeryProcedure(boolean surgeryProcedure) {
        this.surgeryProcedure = surgeryProcedure;
    }

    public boolean isCns() {
        return cns;
    }

    public void setCns(boolean cns) {
        this.cns = cns;
    }

    public boolean isPlateletFunctionDefect() {
        return plateletFunctionDefect;
    }

    public void setPlateletFunctionDefect(boolean plateletFunctionDefect) {
        this.plateletFunctionDefect = plateletFunctionDefect;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    @Override
    public String toString() {
        return "Platelet";
    }

    @Override
    public int getBlootTypeIndex() {
        return PLATELET;
    }
    
}
