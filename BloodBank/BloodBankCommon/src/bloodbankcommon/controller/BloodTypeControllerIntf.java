/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.model.Blood;
import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Janith
 */
public interface BloodTypeControllerIntf extends Remote {

    public Blood getCryo(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Blood getCsp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Blood getFfp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Blood getRedCell(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Blood getPlatelet(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public List<Blood> getExpiredRedCells() throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Collection<Blood> getExpiredPlatelets() throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Collection<Blood> getExpiredPlasma() throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Collection<Blood> getExpiredCryo() throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Collection<Blood> getExpiredCsp() throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean removeBlood(Blood blood) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Blood getBlood(String bloodId, int type) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean isBloodIssued(String id, int type) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean markReturned(Blood blood) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

}
