/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Receiver;
import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public interface ReceiverControllerIntf extends Remote{

    public Receiver getConfirmReceiver(String searchId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Receiver getReceiverSimple(String id) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public ObservableList<Donation> getTransfusionsOf(String patient) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean addPatient(Receiver receiver)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Receiver getHospital(String hospitalId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean updateHospital(Receiver hospital)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean addHospital(Receiver hospital)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean submitOutsourcings(String hospId, List<Blood> bloodList)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;;

    public String getNextHospitalId()throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException ;

}
