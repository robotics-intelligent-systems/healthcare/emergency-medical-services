/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 *
 * @author Janith
 */
public interface LoginController extends Remote {
    
    public boolean checkConnection() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;

    public boolean login(String user, String hash, String type) throws RemoteException, ClassNotFoundException, PropertyVetoException, SQLException ;
    
}
