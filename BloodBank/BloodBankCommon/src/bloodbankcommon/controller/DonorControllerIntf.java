/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.model.Donor;
import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 *
 * @author Janith
 */
public interface DonorControllerIntf extends Remote {

    public Donor getDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Donor getDonorSimple(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public String getBloodGroup(String bloodId)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean getDonorExists(String id)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean addDonor(Donor donor)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public Donor getDonorLess(String donorId)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean editDonor(Donor donor)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;
  
}
