/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.request.RequestHandlerIntf;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Janith
 */
public interface RemoteFactory extends Remote {

    public BloodTypeControllerIntf getBloodTypeController() throws RemoteException;

    public DonationControllerIntf getDonationController() throws RemoteException;

    public DonorControllerIntf getdDonorController() throws RemoteException;

    public RequesterControllerIntf getRequesterController() throws RemoteException;

    public CampaignControllerIntf getCampaignControllerIntf() throws RemoteException;

    public ReceiverControllerIntf getReceiverControllerIntf() throws RemoteException;

    public SeperationControllerIntf getSeperationControllerIntf() throws RemoteException;

    public UsersControllerIntf getUsersControllerIntf() throws RemoteException;

    public DonorControllerIntf getDonorController() throws RemoteException;

    public SeperationControllerIntf getSeperationController() throws RemoteException;

    public ReceiverControllerIntf getReceiverController() throws RemoteException;
    
    public LoginController getLoginController() throws RemoteException;

    public RequestHandlerIntf getRequestHandler() throws RemoteException;
    
    }
