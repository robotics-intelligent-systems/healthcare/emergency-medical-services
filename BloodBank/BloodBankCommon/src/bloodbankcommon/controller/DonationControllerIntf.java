/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.model.Donation;
import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.SQLException;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public interface DonationControllerIntf extends Remote {

    public String getBloodType(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;

    public ObservableList<Donation> getDonationListOfDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;

    public boolean addDonation(Donation donation) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;

    public Donation getDonnationFromBloodIdSimple(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;

    public Date getLastDonation(String nic, int typeIndex) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException;
}
