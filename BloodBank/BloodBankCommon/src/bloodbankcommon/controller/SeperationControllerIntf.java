/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.controller;

import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Seperation;
import java.beans.PropertyVetoException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author oshanz
 */
public interface SeperationControllerIntf extends Remote{

    public Seperation getSeperationOf(String bloodId)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

    public boolean submitSeperation(Seperation seperation, List<Blood> bloodList)throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException;

}
