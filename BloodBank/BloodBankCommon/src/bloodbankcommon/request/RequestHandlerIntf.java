/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.request;

import bloodbankcommon.model.Request;
import bloodbankcommon.model.Request_ObservLab;
import bloodbankcommon.model.Request_ObservRequest;
import bloodbankcommon.observer.LabObserver;
import bloodbankcommon.observer.RequestObserver;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author oshanz
 */
public interface RequestHandlerIntf extends Remote {

    public Request_ObservLab sendRequest(Request request) throws RemoteException;

    public boolean addLab(LabObserver labObserver, int labId) throws RemoteException;

    public boolean addWard(RequestObserver requestObserver, int wardId) throws RemoteException;

    public boolean removeLab(int labId) throws RemoteException;

    public boolean removeWard(int wardId) throws RemoteException;

    public void labLogout(Request_ObservRequest request_ObservRequest) throws RemoteException;
}
