/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Janith
 */
public class Donor implements Serializable {

    private String nic;
    private String name;
    private String address;
    private String bloodGroup;
    private String rejected;
    private String sex;
    private int tele;
    private int count;

    public Donor(String nic, String name, String bloodGroup) {
        this.nic = nic;
        this.name = name;
        this.bloodGroup = bloodGroup;
    }

    public Donor(String nic, String name, String address, String bloodGroup, String rejected, String sex, int tele, int count) {
        this.nic = nic;
        this.name = name;
        this.address = address;
        this.bloodGroup = bloodGroup;
        this.rejected = rejected;
        this.sex = sex;
        this.tele = tele;
        this.count = count;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getTele() {
        return tele;
    }

    public void setTele(int tele) {
        this.tele = tele;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nic);
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.address);
        hash = 59 * hash + Objects.hashCode(this.bloodGroup);
        hash = 59 * hash + Objects.hashCode(this.rejected);
        hash = 59 * hash + Objects.hashCode(this.sex);
        hash = 59 * hash + this.tele;
        hash = 59 * hash + this.count;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Donor other = (Donor) obj;
        if (!Objects.equals(this.nic, other.nic)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.bloodGroup, other.bloodGroup)) {
            return false;
        }
        if (!Objects.equals(this.rejected, other.rejected)) {
            return false;
        }
        if (!Objects.equals(this.sex, other.sex)) {
            return false;
        }
        if (this.tele != other.tele) {
            return false;
        }
        if (this.count != other.count) {
            return false;
        }
        return true;
    }

}
