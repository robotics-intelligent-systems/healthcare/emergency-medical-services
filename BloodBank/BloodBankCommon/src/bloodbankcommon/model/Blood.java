/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author oshanz
 */
public class Blood implements Serializable {

    private Date expiaryDate;
    private Date sentdate;
    private String bloodId;
    private String bloodGroup;
    private String receiverId;
    private String specialRequest;
    private String bloodType;

    public Blood(String bloodId) {
        this.bloodId = bloodId;
    }

    public Blood(Date expiaryDate, Date sentdate, String bloodId, String bloodGroup, String receiverId, String specialRequest, String bloodType) {
        this.expiaryDate = expiaryDate;
        this.sentdate = sentdate;
        this.bloodId = bloodId;
        this.bloodGroup = bloodGroup;
        this.receiverId = receiverId;
        this.specialRequest = specialRequest;
        this.bloodType = bloodType;
    }

    public Blood(String bloodId, String receiverId, String bloodType) {
        this.bloodId = bloodId;
        this.receiverId = receiverId;
        this.bloodType = bloodType;
    }

    public Blood(String bloodId, String bloodGroup) {
        this.bloodId = bloodId;
        this.bloodGroup = bloodGroup;
    }

    public Date getExpiaryDate() {
        return expiaryDate;
    }

    public void setExpiaryDate(Date expiaryDate) {
        this.expiaryDate = expiaryDate;
    }

    public Date getSentdate() {
        return sentdate;
    }

    public void setSentdate(Date sentdate) {
        this.sentdate = sentdate;
    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.bloodId);
        hash = 53 * hash + Objects.hashCode(this.bloodType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Blood other = (Blood) obj;
        if (!Objects.equals(this.bloodId, other.bloodId)) {
            return false;
        }
        if (!Objects.equals(this.bloodType, other.bloodType)) {
            return false;
        }
        return true;
    }
    
}
