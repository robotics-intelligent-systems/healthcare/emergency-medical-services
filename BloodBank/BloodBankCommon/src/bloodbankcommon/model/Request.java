/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import bloodbankcommon.bloodtype.BloodType;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author oshanz
 */
public class Request implements Serializable {

    private String patientId;
    private String group;
    private BloodType blood;
    private int units;
    private int requestId;
    private String date;
    private int wardId;
    private int labId;
    private String specialReq;

    public Request(int requestId) {
        this.requestId = requestId;
    }

    public Request(String patientId, String group, BloodType blood, int units, String specReq, int ward) {
        this.patientId = patientId;
        this.group = group;
        this.blood = blood;
        this.units = units;
        this.specialReq = specReq;
        this.wardId = ward;

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
        String dateString = dateFormat.format(new Date());
        dateString += timeFormat.format(new Date());
        this.date = dateString;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public BloodType getBlood() {
        return blood;
    }

    public void setBlood(BloodType blood) {
        this.blood = blood;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int ward) {
        this.wardId = ward;
    }

    public int getLabId() {
        return labId;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }

    public String getSpecialReq() {
        return specialReq;
    }

    public void setSpecialReq(String specialReq) {
        this.specialReq = specialReq;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.requestId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Request other = (Request) obj;
        if (this.requestId != other.requestId) {
            return false;
        }
        return true;
    }
}
