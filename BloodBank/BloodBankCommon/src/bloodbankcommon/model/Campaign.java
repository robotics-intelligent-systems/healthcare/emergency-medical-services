/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author oshanz
 */
public class Campaign implements Serializable {

    private Date date;
    private String corName;
    private String location;
    private String head;
    private String status;
    private int corTele;

    public Campaign(Date date, String corName, String location, String head, String status, int corTele) {
        this.date = date;
        this.corName = corName;
        this.location = location;
        this.head = head;
        this.status = status;
        this.corTele = corTele;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCorName() {
        return corName;
    }

    public void setCorName(String corName) {
        this.corName = corName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCorTele() {
        return corTele;
    }

    public void setCorTele(int corTele) {
        this.corTele = corTele;
    }

}
