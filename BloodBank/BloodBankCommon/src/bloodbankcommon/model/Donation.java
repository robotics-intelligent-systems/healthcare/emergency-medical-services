/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public class Donation implements Serializable {

    private String bloodId;
    private String donationDate;
    private String nic;
    private String status;
    private int type;

    public Donation(String bloodId, String donationDate, String nic, String status, int type) {
        this.bloodId = bloodId;
        this.donationDate = donationDate;
        this.nic = nic;
        this.status = status;
        this.type = type;
    }

    public String getDonationDate() {
        return donationDate;
    }

    public void setDonationDate(String donationDate) {
        this.donationDate = donationDate;
    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
