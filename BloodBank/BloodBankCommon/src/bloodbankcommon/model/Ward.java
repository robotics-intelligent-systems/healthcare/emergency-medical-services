/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import bloodbankcommon.observer.RequestObserver;
import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public class Ward implements Serializable {

    private RequestObserver requestObserver;
    private int wardId;

    public Ward(RequestObserver requestObserver, int wardId) {
        this.requestObserver = requestObserver;
        this.wardId = wardId;
    }

    public RequestObserver getRequestObserver() {
        return requestObserver;
    }

    public void setRequestObserver(RequestObserver requestObserver) {
        this.requestObserver = requestObserver;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }
}
