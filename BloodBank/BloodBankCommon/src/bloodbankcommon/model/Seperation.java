/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public class Seperation implements Serializable{
    
    private String bloodId;
    private String cryoId;
    private String plateletId;
    private String ffplasmaId;
    private String redcellId;
    private String cspplamaId;
    
    public Seperation(String bloodId, String cryoId, String plateletId, String ffplasmaId, String redcellId, String cspplamaId) {
        this.cryoId = cryoId;
        this.plateletId = plateletId;
        this.ffplasmaId = ffplasmaId;
        this.redcellId = redcellId;
        this.cspplamaId = cspplamaId;
    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public String getCryoId() {
        return cryoId;
    }

    public void setCryoId(String cryoId) {
        this.cryoId = cryoId;
    }

    public String getPlateletId() {
        return plateletId;
    }

    public void setPlateletId(String plateletId) {
        this.plateletId = plateletId;
    }

    public String getFfplasmaId() {
        return ffplasmaId;
    }

    public void setFfplasmaId(String ffplasmaId) {
        this.ffplasmaId = ffplasmaId;
    }

    public String getRedcellId() {
        return redcellId;
    }

    public void setRedcellId(String redcellId) {
        this.redcellId = redcellId;
    }

    public String getCspplamaId() {
        return cspplamaId;
    }

    public void setCspplamaId(String cspplamaId) {
        this.cspplamaId = cspplamaId;
    }
    
}
