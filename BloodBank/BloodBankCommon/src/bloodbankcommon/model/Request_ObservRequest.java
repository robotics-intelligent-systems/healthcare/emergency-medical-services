/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import bloodbankcommon.observer.RequestObserver;

/**
 *
 * @author oshanz
 */
public class Request_ObservRequest {

    private Request request;
    private RequestObserver requestObserver;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request requestId) {
        this.request = requestId;
    }

    public RequestObserver getRequestObserver() {
        return requestObserver;
    }

    public void setRequestObserver(RequestObserver requestObserver) {
        this.requestObserver = requestObserver;
    }

    public Request_ObservRequest(Request requestId, RequestObserver requestObserver) {
        this.request = requestId;
        this.requestObserver = requestObserver;
    }
}
