/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import java.io.Serializable;

/**
 *
 * @author Janith
 */
public class Receiver implements Serializable {

    private String receiverId;
    private String name;
    private String address;
    private String bloodGroup;
    private String sex;
    private int tele;
    private int returnAmt;

    public Receiver(String receiverId, String name, String address, int tele) {
        this.receiverId = receiverId;
        this.name = name;
        this.address = address;
        this.tele = tele;
    }

    public Receiver(String receiverId, String name, String bloodGroup) {
        this.receiverId = receiverId;
        this.name = name;
        this.bloodGroup = bloodGroup;
    }

    public Receiver(String receiverId, String name, int returnAmt) {
        this.receiverId = receiverId;
        this.name = name;
        this.returnAmt = returnAmt;
    }

    public Receiver(String receiverId, String name, String address, String bloodGroup, String sex, int tele) {
        this.receiverId = receiverId;
        this.name = name;
        this.address = address;
        this.bloodGroup = bloodGroup;
        this.sex = sex;
        this.tele = tele;
    }

    public Receiver(String receiverId, String name, String address, String bloodGroup, String sex, int tele, int returnAmt) {
        this.receiverId = receiverId;
        this.name = name;
        this.address = address;
        this.bloodGroup = bloodGroup;
        this.sex = sex;
        this.tele = tele;
        this.returnAmt = returnAmt;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getTele() {
        return tele;
    }

    public void setTele(int tele) {
        this.tele = tele;
    }

    public int getReturnAmt() {
        return returnAmt;
    }

    public void setReturnAmt(int returnAmt) {
        this.returnAmt = returnAmt;
    }

}
