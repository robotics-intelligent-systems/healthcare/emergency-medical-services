/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import bloodbankcommon.observer.LabObserver;
import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public class Lab implements Serializable {

    private LabObserver labObserver;
    private int requests;
    private int labId;

    public Lab(LabObserver lab, int requests, int labId) {
        this.labObserver = lab;
        this.requests = requests;
        this.labId = labId;
    }

    public int getLabId() {
        return labId;
    }

    public void setLabId(int labId) {
        this.labId = labId;
    }

    public int getRequests() {
        return requests;
    }

    public void setRequests(int requests) {
        this.requests = requests;
    }

    public LabObserver getLabObserver() {
        return labObserver;
    }

    public void setLabObserver(LabObserver labObserver) {
        this.labObserver = labObserver;
    }
}
