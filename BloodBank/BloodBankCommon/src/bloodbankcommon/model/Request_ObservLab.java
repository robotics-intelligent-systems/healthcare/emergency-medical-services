/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankcommon.model;

import bloodbankcommon.observer.LabObserver;
import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public class Request_ObservLab implements Serializable {
    
    private Request request;
    private LabObserver labObserver;

    public Request_ObservLab(Request requestId, LabObserver labObserver) {
        this.request = requestId;
        this.labObserver = labObserver;
    }

    public LabObserver getLabObserver() {
        return labObserver;
    }

    public void setLabObserver(LabObserver labObserver) {
        this.labObserver = labObserver;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request requestId) {
        this.request = requestId;
    }
    
}
