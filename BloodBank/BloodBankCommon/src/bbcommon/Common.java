/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbcommon;

import javafx.application.Application;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 *
 * @author oshanz
 */
public class Common extends  Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        boolean supported = Platform.isSupported(ConditionalFeature.SCENE3D);
        System.out.println(supported);
    }
    
    public static void main(String[] args) {
        
    }
}
