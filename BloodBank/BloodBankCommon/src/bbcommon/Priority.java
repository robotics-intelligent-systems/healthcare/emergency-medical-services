/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbcommon;

import java.io.Serializable;

/**
 *
 * @author oshanz
 */
public enum Priority implements Serializable {

    URGENT(2), EMERGENCY(1), REGULAR(0);
    private int priority;

    private Priority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
    
    public String getName(int index){
        String name = null;
        switch(index){
            case 0 :
                name = "Regular";
                break;
            case 1 :
                name = "Emergency";
                break;
            case 2 :
                name = "Urgent";
                break;
        }
        return name;
    }
    
    @Override
    public String toString(){
        String name = null;
        switch(priority){
            case 0 :
                name = "Regular";
                break;
            case 1 :
                name = "Emergency";
                break;
            case 2 :
                name = "Urgent";
                break;
        }
        return name;
    }
}
