/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bbcommon;

/**
 *
 * @author Janith
 */
public enum RedCellSpecialReq {
    LUCOREDUCED(0), WASHED(1), IRRADIATED(2);
    private int index;

    private RedCellSpecialReq(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
    
    public String getName(int index){
        String name = null;
        switch(index){
            case 0 :
                name = "Lucoreduced";
                break;
            case 1 :
                name = "Washed";
                break;
            case 2 :
                name = "Irradiated";
                break;
        }
        return name;
    }
    
    @Override
    public String toString(){
        String name = null;
        switch(index){
            case 0 :
                name = "Lucoreduced";
                break;
            case 1 :
                name = "Washed";
                break;
            case 2 :
                name = "Irradiated";
                break;
        }
        return name;
    }
}
