/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice.connector;

import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.LoginController;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.RemoteFactory;
import bloodbankcommon.controller.SeperationControllerIntf;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author Janith
 */
public class ServerConnector {

    private static ServerConnector serverConnector;
    private RemoteFactory remoteFactory;
    private BloodTypeControllerIntf bloodTypeControllerIntf;
    private DonorControllerIntf donorControllerIntf;
    private DonationControllerIntf donationControllerIntf;
    private SeperationControllerIntf seperationController;
    private ReceiverControllerIntf receiverController;
    private LoginController loginController;

    private ServerConnector() throws NotBoundException, MalformedURLException, RemoteException {
        remoteFactory = (RemoteFactory) Naming.lookup("rmi://localhost:5050/BloodBankServer");
    }

    public static ServerConnector getServerConnector() throws NotBoundException, MalformedURLException, RemoteException {
        if (serverConnector == null) {
            serverConnector = new ServerConnector();
        }
        return serverConnector;
    }

    public RemoteFactory getRemoteFactory() {
        return remoteFactory;
    }

    public BloodTypeControllerIntf getBloodTypeControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (bloodTypeControllerIntf == null) {
            bloodTypeControllerIntf = remoteFactory.getBloodTypeController();
        }
        return bloodTypeControllerIntf;
    }

    public DonorControllerIntf getDonorControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (donorControllerIntf == null) {
            donorControllerIntf = remoteFactory.getDonorController();
        }
        return donorControllerIntf;
    }

    public DonationControllerIntf getDonationControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (donationControllerIntf == null) {
            donationControllerIntf = remoteFactory.getDonationController();
        }
        return donationControllerIntf;
    }

    public SeperationControllerIntf getSeperationControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (seperationController == null) {
            seperationController = remoteFactory.getSeperationController();
        }
        return seperationController;
    }

    public LoginController getLoginController() throws NotBoundException, MalformedURLException, RemoteException {
        if (loginController == null) {
            loginController = remoteFactory.getLoginController();
        }
        return loginController;
    }

    public ReceiverControllerIntf getRecetverControllerIntf() throws NotBoundException, MalformedURLException, RemoteException {
        if (receiverController == null) {
            receiverController = remoteFactory.getReceiverController();
        }
        return receiverController;
    }
}