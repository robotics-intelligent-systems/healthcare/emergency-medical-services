/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice.view;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Donor;
import bloodbankcommon.model.Receiver;
import bloodbankcommon.model.Seperation;
import bloodbankheadoffice.connector.ServerConnector;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import javafx.collections.ObservableList;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author oshanz
 */
public class HeadOfficeViewServices {

    private DonorControllerIntf donorController = null;
    private DonationControllerIntf donationsController = null;
    private SeperationControllerIntf seperationController = null;
    private BloodTypeControllerIntf bloodController = null;
    private ReceiverControllerIntf receiverController = null;

    public HeadOfficeViewServices() throws NotBoundException, MalformedURLException, RemoteException {
        donorController = ServerConnector.getServerConnector().getDonorControllerIntf();
        donationsController = ServerConnector.getServerConnector().getDonationControllerIntf();
        seperationController = ServerConnector.getServerConnector().getSeperationControllerIntf();
        bloodController = ServerConnector.getServerConnector().getBloodTypeControllerIntf();
        receiverController = ServerConnector.getServerConnector().getRecetverControllerIntf();
    }

    Donor getDonorSimple(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donorController.getDonorSimple(donorId);
    }

    boolean addDonation(Donation donation) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationsController.addDonation(donation);
    }

    ObservableList<Donation> getDonationListOfDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationsController.getDonationListOfDonor(donorId);
    }

    Seperation getSeperationOf(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return seperationController.getSeperationOf(bloodId);
    }

    Blood getRedCell(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return bloodController.getRedCell(bloodId);
    }

    Blood getPlatelet(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return bloodController.getPlatelet(bloodId);
    }

    Blood getFfp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return bloodController.getFfp(bloodId);
    }

    Blood getCsp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return bloodController.getCsp(bloodId);
    }

    Blood getCryo(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return bloodController.getCryo(bloodId);
    }

    Receiver getReceiverSimple(String receiverId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return receiverController.getReceiverSimple(receiverId);
    }

    ObservableList<Donation> getTransfusionsOf(String patient) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return receiverController.getTransfusionsOf(patient);
    }

    String getLastDonationWarning(String nic, int typeIndex) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        java.sql.Date lastDate = donationsController.getLastDonation(nic, typeIndex);
        String message = "";
        if (lastDate != null) {
            DateTime lDate = new DateTime(lastDate.getTime());
            DateTime curDate = new DateTime();
            int days = Days.daysBetween(lDate, lDate).getDays();

            if (typeIndex == BloodType.WHOLE_BLOOD || typeIndex == BloodType.RED_CELL) {
                if (days <= 120) {
                    message = "The donor has donated blood within last 4 months.\n"
                            + "Last donation date is : " + curDate.toString("YYYY-MM-dd");
                }
            } else if (typeIndex == BloodType.PLASMA || typeIndex == BloodType.PLATELET) {
                if (days <= 7) {
                    message = "The donor has donated blood within last 1 week.\n"
                            + "Last donation date is : " + curDate.toString("YYYY-MM-dd");
                }
            }
        }
        return message;
    }

    boolean getDonorExists(String id) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donorController.getDonorExists(id);
    }

    boolean addDonor(Donor donor) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donorController.addDonor(donor);
    }

    Donor getDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donorController.getDonor(donorId);
    }

    Donor getDonorLess(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donorController.getDonorLess(donorId);
    }

    boolean editDonor(Donor donor) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException{
        return donorController.editDonor(donor);
    }
}
