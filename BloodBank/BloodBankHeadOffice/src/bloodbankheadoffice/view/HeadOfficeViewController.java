/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice.view;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.bloodtype.BloodTypes;
import bloodbankheadoffice.model.SeperationTabelModel;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Donor;
import bloodbankcommon.model.Receiver;
import bloodbankcommon.model.Seperation;
import bloodbankheadoffice.model.DonationTabelModel;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Janith
 */
public class HeadOfficeViewController implements Initializable {

//    //<editor-fold defaultstate="collapsed" desc="Injections">
    @FXML
    private TextField donorInfIdField;
    @FXML
    private Label donorInfIdErrorLabel;
    @FXML
    private ChoiceBox<String> donorInfTypeCombo;
    @FXML
    private TextField donorInfNameField;
    @FXML
    private TextField donorInfAgeField;
    @FXML
    private TextField donorInfGroupField;
    @FXML
    private Button donorInfSubmitButton;
    @FXML
    private Label donorInfErrorLabel;
    @FXML
    private TextField donorInfBloodIdField;
    @FXML
    private RadioButton byDonorRadioBtn;
    @FXML
    private RadioButton byPatientRadioBtn;
    @FXML
    private TextField bDetailsIdField;
    @FXML
    private TextField bDetailsNameField;
    @FXML
    private TextField bDetailsGroupField;
    @FXML
    private Label bDetailsErrorLabel;
    @FXML
    private TableView<DonationTabelModel> donationsTable;
    @FXML
    private TableView<SeperationTabelModel> seperationsTable;
    @FXML
    private TableColumn<DonationTabelModel, String> donationsDateCol;
    @FXML
    private TableColumn<DonationTabelModel, String> donationsBTypeCol;
    @FXML
    private TableColumn<DonationTabelModel, String> donationsBIdCol;
    @FXML
    private TableColumn<DonationTabelModel, String> donationsStatusCol;
    @FXML
    private TableColumn<SeperationTabelModel, String> seperationsBTypeCol;
    @FXML
    private TableColumn<SeperationTabelModel, String> seperationsBIdCol;
    @FXML
    private TableColumn<SeperationTabelModel, String> seperationsStatusCol;
    @FXML
    private MenuItem exitMenuItem;
    @FXML
    private MenuItem aboutMenuItem;
    @FXML
    private MenuItem settingsMenuItem;
    @FXML
    private TextField addDonorIdField;
    @FXML
    private Label addDonorIdErrorLabel;
    @FXML
    private TextField addDonorNameField;
    @FXML
    private TextArea addDonorAddressField;
    @FXML
    private TextField addDonorTelField;
    @FXML
    private RadioButton addDonorMaleRadio;
    @FXML
    private RadioButton addDonorFemaleRadio;
    @FXML
    private ChoiceBox<String> addDonorGroupChoice;
    @FXML
    private ChoiceBox<String> addDonorRhChoice;
    @FXML
    private Button addDonorButton;
    @FXML
    private Button addDonorCancelButton;
    @FXML
    private Label addDonorErrorLabel;
    @FXML
    private TextField editDonorIdField;
    @FXML
    private Label editDonorIdErrorLabel;
    @FXML
    private TextField editDonorNameField;
    @FXML
    private TextArea editDonorAddressField;
    @FXML
    private TextField editDonorTelField;
    @FXML
    private RadioButton editDonorMaleRadio;
    @FXML
    private RadioButton editDonorFemaleRadio;
    @FXML
    private ChoiceBox<String> editDonorGroupChoice;
    @FXML
    private ChoiceBox<String> editDonorRhChoice;
    @FXML
    private Button editDonorButton;
    @FXML
    private Button editDonorCancelButton;
    @FXML
    private Label editDonorErrorLabel;
    //</editor-fold>
    private Donor editingDonationDonor;
    private Donor addingDonor;
    private Donor editingDonor;
    private ObservableList<DonationTabelModel> donationTableList;
    private ObservableList<SeperationTabelModel> seperationTableList;
    private HeadOfficeViewServices headOfficeViewServices;
    private Stage stage;
    private BooleanProperty addDonorNameFill = new SimpleBooleanProperty(false);
    private BooleanProperty addDonorAddressFill = new SimpleBooleanProperty(false);
    private BooleanProperty addDonorTelFill = new SimpleBooleanProperty(false);
    private BooleanProperty editDonorNameFill = new SimpleBooleanProperty(false);
    private BooleanProperty editDonorAddressFill = new SimpleBooleanProperty(false);
    private BooleanProperty editDonorTelFill = new SimpleBooleanProperty(false);

    public HeadOfficeViewController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            this.headOfficeViewServices = new HeadOfficeViewServices();
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
        }

        //<editor-fold defaultstate="collapsed" desc="Controls Processing">
        addDonorNameField.setDisable(true);
        addDonorAddressField.setDisable(true);
        addDonorTelField.setDisable(true);
        addDonorMaleRadio.setDisable(true);
        addDonorFemaleRadio.setDisable(true);
        addDonorGroupChoice.setDisable(true);
        addDonorRhChoice.setDisable(true);
        addDonorButton.setDisable(true);

        editDonorNameField.setDisable(true);
        editDonorAddressField.setDisable(true);
        editDonorTelField.setDisable(true);
        editDonorMaleRadio.setDisable(true);
        editDonorFemaleRadio.setDisable(true);
        editDonorGroupChoice.setDisable(true);
        editDonorRhChoice.setDisable(true);
        editDonorButton.setDisable(true);
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Combo Inits">
        donorInfTypeCombo.getItems().remove(0, donorInfTypeCombo.getItems().size());
        donorInfTypeCombo.getItems().addAll(
                BloodTypes.WHOLE_BLOOD.getName(),
                BloodTypes.RED_CELL.getName(),
                BloodTypes.PLATELET.getName(),
                BloodTypes.PLASMA.getName());
        donorInfTypeCombo.getSelectionModel().selectFirst();

        addDonorGroupChoice.getItems().remove(0, addDonorGroupChoice.getItems().size());
        addDonorGroupChoice.getItems().addAll("A", "B", "AB", "O");
        addDonorGroupChoice.getSelectionModel().select(0);

        addDonorRhChoice.getItems().remove(0, addDonorRhChoice.getItems().size());
        addDonorRhChoice.getItems().addAll("Positive", "Negative");
        addDonorRhChoice.getSelectionModel().select(0);

        editDonorGroupChoice.getItems().remove(0, editDonorGroupChoice.getItems().size());
        editDonorGroupChoice.getItems().addAll("A", "B", "AB", "O");
        editDonorGroupChoice.getSelectionModel().select(0);

        editDonorRhChoice.getItems().remove(0, editDonorRhChoice.getItems().size());
        editDonorRhChoice.getItems().addAll("Positive", "Negative");
        editDonorRhChoice.getSelectionModel().select(0);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="TabelColumn Inits">
        donationsDateCol.setCellValueFactory(new PropertyValueFactory<DonationTabelModel, String>("donationDate"));
        donationsBTypeCol.setCellValueFactory(new PropertyValueFactory<DonationTabelModel, String>("type"));
        donationsBIdCol.setCellValueFactory(new PropertyValueFactory<DonationTabelModel, String>("bloodId"));
        donationsStatusCol.setCellValueFactory(new PropertyValueFactory<DonationTabelModel, String>("status"));
        seperationsBTypeCol.setCellValueFactory(new PropertyValueFactory<SeperationTabelModel, String>("bloodType"));
        seperationsBIdCol.setCellValueFactory(new PropertyValueFactory<SeperationTabelModel, String>("bloodId"));
        seperationsStatusCol.setCellValueFactory(new PropertyValueFactory<SeperationTabelModel, String>("status"));
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Validations">
        donorInfBloodIdField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    donorInfSubmitButton.setDisable(true);
                } else {
                    if (newValue.length() > 10) {
                        donorInfBloodIdField.setText(oldValue);
                    }
                    donorInfSubmitButton.setDisable(false);
                }
            }
        });

        //<editor-fold defaultstate="collapsed" desc="Add Donor Validations">
        addDonorIdField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                } else {
                    if (newValue.length() > 10) {
                        addDonorIdField.setText(oldValue);
                    }
                }
            }
        });
        addDonorNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    addDonorNameFill.set(false);
                } else {
                    if (newValue.length() > 50) {
                        addDonorNameField.setText(oldValue);
                    }
                    addDonorNameFill.set(true);
                }
            }
        });
        addDonorAddressField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    addDonorAddressFill.set(false);
                } else {
                    if (newValue.length() > 200) {
                        addDonorAddressField.setText(oldValue);
                    }
                    addDonorAddressFill.set(true);
                }
            }
        });
        addDonorTelField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    addDonorTelFill.set(false);
                } else {
                    if (addDonorTelField.getText().matches("[\\d]*")) {
                        addDonorTelField.setText(oldValue);
                    }
                    if (newValue.length() > 10) {
                        addDonorTelField.setText(oldValue);
                    }
                    addDonorTelFill.set(true);
                }
            }
        });
        addDonorButton.disableProperty().bind((addDonorNameFill.and(addDonorAddressFill).and(addDonorTelFill)).not());
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Edit Donor Validations">
        editDonorIdField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                } else {
                    if (newValue.length() > 10) {
                        editDonorIdField.setText(oldValue);
                    }
                }
            }
        });
        editDonorNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    editDonorNameFill.set(false);
                } else {
                    if (newValue.length() > 50) {
                        editDonorNameField.setText(oldValue);
                    }
                    editDonorNameFill.set(true);
                }
            }
        });
        editDonorAddressField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    editDonorAddressFill.set(false);
                } else {
                    if (newValue.length() > 200) {
                        editDonorAddressField.setText(oldValue);
                    }
                    editDonorAddressFill.set(true);
                }
            }
        });
        editDonorTelField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    editDonorTelFill.set(false);
                } else {
                    if (editDonorTelField.getText().matches("[\\d]*")) {
                        editDonorTelField.setText(oldValue);
                    }
                    if (newValue.length() > 10) {
                        editDonorTelField.setText(oldValue);
                    }
                    editDonorTelFill.set(true);
                }
            }
        });
        editDonorButton.disableProperty().bind((editDonorNameFill.and(editDonorAddressFill).and(editDonorTelFill)).not());
        //</editor-fold>
        //</editor-fold>
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void onExitBtnClick(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    public void onsettingsBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image(HeadOfficeViewController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("settingsview/SettingsView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onAboutBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image(HeadOfficeViewController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("aboutview/AboutView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void ondonorInfIdFieldActionPerformed(ActionEvent event) {
        donorInfIdErrorLabel.setText("");
        donorInfErrorLabel.setText("");
        try {
            editingDonationDonor = headOfficeViewServices.getDonorSimple(donorInfIdField.getText());
            if (editingDonationDonor != null) {
                donorInfIdErrorLabel.setText("");
                donorInfNameField.setText(editingDonationDonor.getName());

                int age = Integer.parseInt(editingDonationDonor.getNic().substring(0, 2));
                int currentYear = Calendar.getInstance().get(Calendar.YEAR);
                currentYear -= 2000;
                if (age < currentYear) {
                    age += 2000;
                } else {
                    age += 1900;
                }
                age = Calendar.getInstance().get(Calendar.YEAR) - age;

                donorInfAgeField.setText(age + "");
                donorInfGroupField.setText(editingDonationDonor.getBloodGroup());
                donorInfTypeCombo.setDisable(false);
                donorInfBloodIdField.setDisable(false);
            } else {
                donorInfIdErrorLabel.setText("Invalid Donor ID");
                clearDonorInfView();
            }
        } catch (RemoteException | ClassNotFoundException ex) {
            donorInfIdErrorLabel.setText("Server Error");
            clearDonorInfView();
        } catch (SQLException ex) {
            donorInfIdErrorLabel.setText("Invalid Donor ID");
            clearDonorInfView();
            Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void onDonationSubmitBtnClick(ActionEvent event) {
        String nic = editingDonationDonor.getNic();
        int typeIndex = donorInfTypeCombo.getSelectionModel().getSelectedIndex();
        try {
            String message = headOfficeViewServices.getLastDonationWarning(nic, typeIndex);
            Dialogs.DialogResponse response = Dialogs.showConfirmDialog(
                    stage, "Do you want to continue?", message, "Warning");
            if (response == DialogResponse.YES) {
                boolean success = addDonation(nic, typeIndex);
                if (success) {
                    donorInfErrorLabel.setText("Donation Submitted");
                    clearDonorInfView();
                } else {
                    donorInfErrorLabel.setText("Error! Donation not Submitted");
                }
            }
        } catch (RemoteException | ClassNotFoundException | SQLException ex) {
        } catch (PropertyVetoException ex) {
            Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private boolean addDonation(String nic, int type) {
        Donation donation = new Donation(donorInfBloodIdField.getText(),
                new java.sql.Date(System.currentTimeMillis()).toString(),
                nic, "", type);
        try {
            boolean successful = headOfficeViewServices.addDonation(donation);
            if (successful) {
                return successful;
            } else {
                return false;
            }
        } catch (RemoteException | ClassNotFoundException ex) {
            donorInfErrorLabel.setText("Server Error");
            return false;
        } catch (SQLException ex) {
            donorInfErrorLabel.setText("Invalid ID");
            return false;
        } catch (PropertyVetoException ex) {
            donorInfErrorLabel.setText("Server Error");
            return false;
        }
    }

    @FXML
    public void onBDetailsIdActionPerformed(ActionEvent event) {
        if (bDetailsIdField.getText() != null || !bDetailsIdField.getText().isEmpty()) {
            if (byDonorRadioBtn.isSelected()) {
                try {
                    bDetailsErrorLabel.setText("");
                    Donor donor = headOfficeViewServices.getDonorSimple(bDetailsIdField.getText());
                    if (donor.getName().isEmpty()) {
                        bDetailsErrorLabel.setText("Invalid ID");
                        return;
                    }
                    bDetailsNameField.setText(donor.getName());
                    bDetailsGroupField.setText(donor.getBloodGroup());
                    ObservableList<Donation> donationList = headOfficeViewServices.getDonationListOfDonor(bDetailsIdField.getText());
                    for (Donation d : donationList) {
                        DonationTabelModel donationTabelModel;
                        donationTabelModel = new DonationTabelModel(d.getDonationDate(),
                                BloodType.getStringOf(d.getType()), d.getBloodId(),
                                d.getStatus());
                        donationTableList.add(donationTabelModel);
                    }
                    donationsTable.setItems(donationTableList);

                    List<Seperation> seperationList = new ArrayList<>();
                    for (Donation donation : donationList) {
                        Seperation seperation = headOfficeViewServices.getSeperationOf(donation.getBloodId());
                        seperationList.add(seperation);
                    }

                    List<Blood> bloodList = new ArrayList<>();
                    for (Seperation s : seperationList) {
                        String bloodId = s.getBloodId();
                        if (s.getCryoId().isEmpty()) {
                            Blood cryo = headOfficeViewServices.getCryo(bloodId);
                            bloodList.add(cryo);
                        } else if (s.getCspplamaId().isEmpty()) {
                            Blood csp = headOfficeViewServices.getCsp(bloodId);
                            bloodList.add(csp);
                        } else if (s.getFfplasmaId().isEmpty()) {
                            Blood ffp = headOfficeViewServices.getFfp(bloodId);
                            bloodList.add(ffp);
                        } else if (s.getPlateletId().isEmpty()) {
                            Blood platelet = headOfficeViewServices.getPlatelet(bloodId);
                            bloodList.add(platelet);
                        } else if (s.getRedcellId().isEmpty()) {
                            Blood redCell = headOfficeViewServices.getRedCell(bloodId);
                            bloodList.add(redCell);
                        }
                    }

                    seperationTableList = FXCollections.observableArrayList();
                    for (Blood b : bloodList) {
                        String receiver = b.getReceiverId();
                        if (receiver.isEmpty()) {
                            if (!b.getExpiaryDate().toString().isEmpty() || b.getExpiaryDate().after(new Date())) {
                                receiver = "Date of expiary : "
                                        + new java.sql.Date(b.getExpiaryDate().getTime()).toString();
                            } else {
                                receiver = "Expired";
                            }
                        } else if (receiver.startsWith("h")) {
                            receiver = "Sent to hospital : " + receiver;
                        } else {
                            receiver = "Transfused to patient : " + receiver;
                        }
                        SeperationTabelModel model = new SeperationTabelModel(b.getBloodType(),
                                b.getBloodId(), receiver);
                        seperationTableList.add(model);
                    }

                    seperationsTable.setItems(seperationTableList);

                } catch (RemoteException | ClassNotFoundException ex) {
                    bDetailsErrorLabel.setText("Server Error");
                } catch (SQLException ex) {
                    bDetailsErrorLabel.setText("Invalid ID");
                } catch (PropertyVetoException ex) {
                    bDetailsErrorLabel.setText("Server Error");
                }
            } else if (byPatientRadioBtn.isSelected()) {
                try {
                    bDetailsErrorLabel.setText("");
                    Receiver receiver = headOfficeViewServices.getReceiverSimple(bDetailsIdField.getText());
                    if (receiver.getName().isEmpty()) {
                        bDetailsErrorLabel.setText("Invalid ID");
                        return;
                    }
                    bDetailsNameField.setText(receiver.getName());
                    bDetailsGroupField.setText(receiver.getBloodGroup());

                    ObservableList<Donation> transfusionList;
                    transfusionList = headOfficeViewServices.getTransfusionsOf(bDetailsIdField.getText());
                    for (Donation d : transfusionList) {
                        String type = BloodType.getStringOf(d.getType());
                        DonationTabelModel model;
                        model = new DonationTabelModel(d.getDonationDate(), type, d.getBloodId(), "");
                        donationTableList.add(model);
                    }

                    donationsTable.setItems(donationTableList);

                } catch (RemoteException | ClassNotFoundException ex) {
                    bDetailsErrorLabel.setText("Server Error");
                } catch (SQLException ex) {
                    bDetailsErrorLabel.setText("Invalid ID");
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void clearDonorInfView() {
        donorInfIdField.setText("");
        donorInfTypeCombo.setDisable(true);
        donorInfBloodIdField.setText("");
        donorInfBloodIdField.setDisable(true);
        donorInfNameField.setText("");
        donorInfAgeField.setText("");
        donorInfGroupField.setText("");
        editingDonationDonor = null;
    }

    @FXML
    public void onAddDonorIdFieldActionPerformed(ActionEvent event) {
        addDonorIdErrorLabel.setText("");
        addDonorErrorLabel.setText("");

        if (!addDonorIdField.getText().matches("^[\\d][0-5][0-6][0-6][\\d]{5}\\s?[vx|VX]$")) {
            addDonorIdErrorLabel.setText("Invalid ID");
            return;
        }

        try {
            boolean donorExists = headOfficeViewServices.getDonorExists(addDonorIdField.getText());
            if (donorExists) {
                addDonorIdErrorLabel.setText("Donor Already in the Database");
                addDonorIdField.selectAll();
            } else {
                addingDonor = new Donor(addDonorIdField.getText(), "", "");
                enableAddFields();
            }
        } catch (RemoteException | ClassNotFoundException ex) {
            addDonorIdErrorLabel.setText("Server Error");
        } catch (SQLException ex) {
            addDonorIdErrorLabel.setText("Invalid ID");
        } catch (PropertyVetoException ex) {
            Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void onAddDonationBtnClick(ActionEvent event) {
        addDonorIdErrorLabel.setText("");
        addDonorErrorLabel.setText("");
        if (addingDonor != null) {
            String bloodGroup = addDonorGroupChoice.getSelectionModel().getSelectedItem();
            if (addDonorRhChoice.getSelectionModel().getSelectedIndex() == 0) {
                bloodGroup += "+";
            } else {
                bloodGroup += "-";
            }
            addingDonor.setName(addDonorNameField.getText());
            addingDonor.setAddress(addDonorAddressField.getText());
            addingDonor.setTele(Integer.parseInt(addDonorTelField.getText()));
            addingDonor.setBloodGroup(bloodGroup);
            try {
                boolean success = headOfficeViewServices.addDonor(addingDonor);
                if (success) {
                    addDonorErrorLabel.setText("Donor Added Successfully");
                    disableAddFields();
                } else {
                    addDonorErrorLabel.setText("Error! Donor Not Added");
                }
            } catch (RemoteException | ClassNotFoundException ex) {
                addDonorErrorLabel.setText("Server Error");
            } catch (SQLException ex) {
                addDonorErrorLabel.setText("Invalid Entries");
            } catch (PropertyVetoException ex) {
                Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    public void onAddDonationCancelBtnClick(ActionEvent event) {
        addDonorIdField.setText("");
        disableAddFields();
        addingDonor = null;
    }

    private void enableAddFields() {
        addDonorNameField.setDisable(false);
        addDonorAddressField.setDisable(false);
        addDonorTelField.setDisable(false);
        addDonorMaleRadio.setDisable(false);
        addDonorFemaleRadio.setDisable(false);
        addDonorGroupChoice.setDisable(false);
        addDonorRhChoice.setDisable(false);
    }

    private void disableAddFields() {
        addDonorNameField.setText("");
        addDonorAddressField.setText("");
        addDonorTelField.setText("");
        addDonorMaleRadio.setSelected(true);

        addDonorNameField.setDisable(true);
        addDonorAddressField.setDisable(true);
        addDonorTelField.setDisable(true);
        addDonorMaleRadio.setDisable(true);
        addDonorFemaleRadio.setDisable(true);
        addDonorGroupChoice.setDisable(true);
        addDonorRhChoice.setDisable(true);
    }

    @FXML
    public void onEditDonorIdFieldActionPerformed(ActionEvent event) {
        editDonorIdErrorLabel.setText("");
        editDonorErrorLabel.setText("");

        if (!editDonorIdField.getText().matches("^[\\d][0-5][0-6][0-6][\\d]{5}\\s?[vx|VX]$")) {
            editDonorIdErrorLabel.setText("Invalid ID");
            return;
        }

        try {
            editingDonor = headOfficeViewServices.getDonorLess(editDonorIdField.getText());
            if (editingDonor.getNic() == null) {
                addDonorIdErrorLabel.setText("Donor is not in the Database");
                addDonorIdField.selectAll();
            } else {
                enableEditFields();
                fillEditFields(editingDonor);
            }
        } catch (RemoteException | ClassNotFoundException ex) {
            editDonorIdErrorLabel.setText("Server Error");
        } catch (SQLException ex) {
            editDonorIdErrorLabel.setText("Invalid ID");
        } catch (PropertyVetoException ex) {
            Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void onEditDonationBtnClick(ActionEvent event) {
        addDonorIdErrorLabel.setText("");
        addDonorErrorLabel.setText("");
        if (editingDonor != null) {
            Donor editingDonorNew = new Donor(editingDonor.getNic(), "", "");
            String bloodGroup = addDonorGroupChoice.getSelectionModel().getSelectedItem();
            if (addDonorRhChoice.getSelectionModel().getSelectedIndex() == 0) {
                bloodGroup += "+";
            } else {
                bloodGroup += "-";
            }

            editingDonorNew.setName(addDonorNameField.getText());
            editingDonorNew.setAddress(addDonorAddressField.getText());
            editingDonorNew.setTele(Integer.parseInt(addDonorTelField.getText()));
            editingDonorNew.setBloodGroup(bloodGroup);
            if (editDonorMaleRadio.isSelected()) {
                editingDonorNew.setSex("m");
            } else {
                editingDonorNew.setSex("f");
            }

            if (editingDonorNew.equals(editingDonor)) {
                addDonorErrorLabel.setText("No changes detected");
                return;
            }

            try {
                boolean success = headOfficeViewServices.editDonor(editingDonorNew);
                if (success) {
                    editDonorErrorLabel.setText("Donor Updated Successfully");
                    disableEditFields();
                } else {
                    addDonorErrorLabel.setText("Error! Donor Not Updated");
                }
            } catch (RemoteException | ClassNotFoundException ex) {
                editDonorErrorLabel.setText("Server Error");
            } catch (SQLException ex) {
                editDonorErrorLabel.setText("Invalid Entries");
            } catch (PropertyVetoException ex) {
                Logger.getLogger(HeadOfficeViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    public void onEditDonationCancelBtnClick(ActionEvent event) {
        editDonorIdField.setText("");
        disableEditFields();
        editingDonor = null;
    }

    private void enableEditFields() {
        editDonorNameField.setDisable(false);
        editDonorAddressField.setDisable(false);
        editDonorTelField.setDisable(false);
        editDonorMaleRadio.setDisable(false);
        editDonorFemaleRadio.setDisable(false);
        editDonorGroupChoice.setDisable(false);
        editDonorRhChoice.setDisable(false);
    }

    private void disableEditFields() {
        editDonorNameField.setText("");
        editDonorAddressField.setText("");
        editDonorTelField.setText("");
        editDonorMaleRadio.setSelected(true);

        editDonorNameField.setDisable(true);
        editDonorAddressField.setDisable(true);
        editDonorTelField.setDisable(true);
        editDonorMaleRadio.setDisable(true);
        editDonorFemaleRadio.setDisable(true);
        editDonorGroupChoice.setDisable(true);
        editDonorRhChoice.setDisable(true);
    }

    private void fillEditFields(Donor d) {
        editDonorNameField.setText(d.getName());
        editDonorAddressField.setText(d.getAddress());
        editDonorTelField.setText(d.getTele() + "");
        if (d.getSex().toLowerCase().equals("m")) {
            editDonorMaleRadio.setSelected(true);
        } else {
            editDonorFemaleRadio.setSelected(true);
        }
        String group = d.getBloodGroup().toLowerCase();
        int groupIndex = 0;
        int rhIndex = 0;
        if (group.contains("o")) {
            groupIndex = 3;
        } else if (group.contains("ab")) {
            groupIndex = 2;
        } else if (group.contains("a")) {
            groupIndex = 1;
        }

        if (group.contains("-")) {
            rhIndex = 1;
        }
        editDonorGroupChoice.getSelectionModel().select(groupIndex);
        editDonorRhChoice.getSelectionModel().select(rhIndex);
    }
}
