/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice.model;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Janith
 */
public class DonationTabelModel implements Serializable {

    private SimpleStringProperty donationDate;
    private SimpleStringProperty bloodType;
    private SimpleStringProperty bloodId;
    private SimpleStringProperty status;

    public DonationTabelModel(String donationDate, String bloodType, String bloodId, String status) {
        this.donationDate = new SimpleStringProperty(donationDate);
        this.bloodType = new SimpleStringProperty(bloodType);
        this.bloodId = new SimpleStringProperty(bloodId);
        this.status = new SimpleStringProperty(status);
    }

    public String getDonationDate() {
        return donationDate.get();
    }

    public void setDoantionDate(String donationDate) {
        this.donationDate.set(donationDate);
    }

    public String getBloodType() {
        return bloodType.get();
    }

    public void setBloodType(String bloodType) {
        this.bloodType.set(bloodType);
    }

    public String getBloodId() {
        return bloodId.get();
    }

    public void setBloodId(String bloodId) {
        this.bloodId.set(bloodId);
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }
}
