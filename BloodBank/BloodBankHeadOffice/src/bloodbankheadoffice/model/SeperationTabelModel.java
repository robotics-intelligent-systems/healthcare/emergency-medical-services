/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice.model;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Janith
 */
public class SeperationTabelModel implements Serializable{
    private SimpleStringProperty bloodType;
    private SimpleStringProperty bloodId;
    private SimpleStringProperty status;

    public SeperationTabelModel(String bloodType, String bloodId, String status) {
        this.bloodType = new SimpleStringProperty(bloodType);
        this.bloodId = new SimpleStringProperty(bloodId);
        this.status = new SimpleStringProperty(status);
    }

    public String getBloodType() {
        return bloodType.get();
    }

    public void setBloodType(String bloodType) {
        this.bloodType.set(bloodType);
    }

    public String getBloodId() {
        return bloodId.get();
    }

    public void setBloodId(String bloodId) {
        this.bloodId.set(bloodId);
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }
    
}
