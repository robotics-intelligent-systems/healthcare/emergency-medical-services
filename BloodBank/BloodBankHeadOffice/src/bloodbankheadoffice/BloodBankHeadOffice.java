/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankheadoffice;

import java.io.IOException;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 *
 * @author Janith
 */
public class BloodBankHeadOffice extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        Stage loginStage = new Stage();

        LoginViewController controller = null;

        loginStage.centerOnScreen();
        loginStage.initStyle(StageStyle.TRANSPARENT);
        loginStage.getIcons().add(new Image(LoginViewController.class.getResourceAsStream( "BloodDrop.png" )));
        
        Parent root = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginView.fxml"));
            root = (Parent) loader.load();
            controller = (LoginViewController)loader.getController();
        } catch (IOException ex) {
            System.exit(0);
        }
        Scene scene = new Scene(root);
        loginStage.setTitle("Blood Bank Login");
        controller.setStage(primaryStage, loginStage);
        
        String image = BloodBankHeadOffice.class.getResource("loginBackground.jpg").toExternalForm();
        String style = "-fx-background-image: url('" + image + "');"
                + "-fx-background-position: center center;"
                + "-fx-background-repeat: stretch;";
        root.setStyle(style);
        
        loginStage.setScene(scene);
        loginStage.centerOnScreen();
        loginStage.show();
        controller.loadPhase1();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}