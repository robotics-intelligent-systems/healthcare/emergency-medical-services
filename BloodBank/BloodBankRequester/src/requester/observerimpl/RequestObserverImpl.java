/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.observerimpl;

import bloodbankcommon.observer.RequestObserver;
import bloodbankcommon.observer.notifier.RequestNotifier;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author oshanz
 */
public class RequestObserverImpl extends UnicastRemoteObject implements RequestObserver {

    private RequestNotifier requestNotifier;
    private int wardId;

    public RequestObserverImpl(RequestNotifier requestNotifier, int wardId) throws RemoteException {
        this.requestNotifier = requestNotifier;
        this.wardId = wardId;
    }

    @Override
    public void sendStatus(int requestId, String status) throws RemoteException {
        requestNotifier.setRequestStatus(requestId, status);
    }

    @Override
    public int getWardId() throws RemoteException {
        return wardId;
    }
}
