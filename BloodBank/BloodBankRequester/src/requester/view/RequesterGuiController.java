/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.view;

import bbcommon.Priority;
import bbcommon.RedCellSpecialReq;
import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.bloodtype.CRYO;
import bloodbankcommon.bloodtype.CSP;
import bloodbankcommon.bloodtype.FFPlasma;
import bloodbankcommon.bloodtype.Platelet;
import bloodbankcommon.bloodtype.RedCell;
import bloodbankcommon.model.Receiver;
import bloodbankcommon.model.Request;
import bloodbankcommon.observer.LabObserver;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import requester.tablemodels.RequestDetails;

/**
 * FXML Controller class
 *
 * @author Janith
 */
public class RequesterGuiController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Attributes">
    private RequesterGuiServices requesterGuiServices;
    private Receiver currentPatient = null;
    private Stage stage;
    private int wardId;
    private BooleanProperty patientLoaded = new SimpleBooleanProperty(false);
    private BooleanProperty amountEntered = new SimpleBooleanProperty(false);
    private RequesterGuiObservable requesterGuiObservable;
    private ObservableList<RequestDetails> tableList = FXCollections.observableArrayList();
    private ObservableList<RequestDetails> requestList = FXCollections.observableArrayList();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="View Requests Tab">
    @FXML
    private TableView<RequestDetails> requestsTable;
    @FXML
    private TableColumn<RequestDetails, String> reqIdColumn;
    @FXML
    private TableColumn<RequestDetails, String> patientIdColumn;
    @FXML
    private TableColumn<RequestDetails, String> accepterColumn;
    @FXML
    private TableColumn<RequestDetails, String> bloodTypeColumn;
    @FXML
    private TableColumn<RequestDetails, Integer> unitsColumn;
    @FXML
    private TableColumn<RequestDetails, String> priorityColumn;
    @FXML
    private TableColumn<RequestDetails, String> statusColumn;
    @FXML
    private Button cancelButton;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Patient">
    @FXML
    private TextField patientIdText;
    @FXML
    private Label patientErrorLabel;
    @FXML
    private Label patientNameLabel;
    @FXML
    private Label unitsToReturnLabel;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Blood Types Radio Buttons">
    @FXML
    private ToggleGroup bloodTypes;
    @FXML
    private RadioButton redCellRadioBtn;
    @FXML
    private RadioButton plateletRadioBtn;
    @FXML
    private RadioButton ffpRadioBtn;
    @FXML
    private RadioButton cryoRadioBtn;
    @FXML
    private RadioButton cspRadioBtn;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="StackPane & Main Panes">
    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane redCellPane;
    @FXML
    private AnchorPane plateletPane;
    @FXML
    private AnchorPane ffpPane;
    @FXML
    private AnchorPane cryoPane;
    @FXML
    private AnchorPane cspPane;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Red Cells Pane">
    @FXML
    private RadioButton routineRadioBtn;
    @FXML
    private RadioButton urgentRadioBtn;
    @FXML
    private RadioButton emergencyRadioBtn;
    @FXML
    private CheckBox transHistoryCheck;
    @FXML
    private RadioButton beforeMonthsCheck;
    @FXML
    private RadioButton withinMonthsCheck;
    @FXML
    private CheckBox transReactionsCheck;
    @FXML
    private TextField descReactionsText;
    @FXML
    private CheckBox obstetricCheck;
    @FXML
    private TextField parityText;
    @FXML
    private TextField currIndicText;
    @FXML
    private TextField hbLevelText;
    @FXML
    private TextField bloodLossText;
    @FXML
    private TextField procedureText;
    @FXML
    private RadioButton LucoreducedRadio;
    @FXML
    private RadioButton WashedRadio;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ffpPane">
    @FXML
    private CheckBox ffpAcuteCheck;
    @FXML
    private CheckBox ffpMassiveCheck;
    @FXML
    private CheckBox ffpLiverCheck;
    @FXML
    private CheckBox ffpSurgeryCheck;
    @FXML
    private CheckBox ffpWarfarinCheck;
    @FXML
    private CheckBox ffpClottingCheck;
    @FXML
    private TextField ffpOtherText;
    @FXML
    private TextField ffpInrText;
    @FXML
    private TextField ffpApttText;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Platelet Pane">
    @FXML
    private CheckBox plateBmCheck;
    @FXML
    private CheckBox plateRiscCheck;
    @FXML
    private CheckBox plateBmBleedingCheck;
    @FXML
    private CheckBox plateAcuteCheck;
    @FXML
    private CheckBox plateMassiveCheck;
    @FXML
    private CheckBox plateSurgeryCheck;
    @FXML
    private CheckBox plateCnsCheck;
    @FXML
    private CheckBox plateFuncDefectCheck;
    @FXML
    private TextField plateOtherText;
    @FXML
    private TextField plateletCountText;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CRYO Pane">
    @FXML
    private CheckBox cryoAcuteCheck;
    @FXML
    private CheckBox cryoMassiveCheck;
    @FXML
    private CheckBox cryoLiverCheck;
    @FXML
    private CheckBox cryoWillibrandCheck;
    @FXML
    private CheckBox cryoHemopCheck;
    @FXML
    private TextField cryoOtherText;
    @FXML
    private TextField cryoApitText;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CSP Pane">
    @FXML
    private CheckBox cspTtpCheck;
    @FXML
    private CheckBox cspHemopCheck;
    @FXML
    private CheckBox cspBurnsCheck;
    @FXML
    private CheckBox cspLiverCheck;
    @FXML
    private CheckBox cspNephroticCheck;
    @FXML
    private TextField cspOtherText;
    @FXML
    private TextField cspAlbuminText;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Request's Common Fields">
    @FXML
    private TextField specialReqText;
    @FXML
    private TextField requiredAmtText;
    @FXML
    private Button submitRequestButton;
    //</editor-fold>

    public RequesterGuiController() {
        try {
            requesterGuiServices = new RequesterGuiServices();
            requesterGuiObservable = new RequesterGuiObservable(this, stage);
            requesterGuiObservable.hMap_requestId_ObservLabs.addListener(new MapChangeListener<Request, LabObserver>() {
                @Override
                public void onChanged(final MapChangeListener.Change<? extends Request, ? extends LabObserver> change) {
                    if (change.wasAdded()) {
                        final Request request = change.getKey();
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    String requestId = request.getRequestId() + "";
                                    String patientId = request.getPatientId();
                                    String labId = change.getValueAdded().getLabId() + "";
                                    String bloodType = request.getBlood().toString();
                                    int numUnits = request.getUnits();
                                    String priority = "";
                                    if (request.getBlood().getBlootTypeIndex() == 1) {  //Red Cells
                                        RedCell redCells = (RedCell) request.getBlood();
                                        priority = redCells.getPriority().toString();
                                    }
                                    RequestDetails requestDetails = new RequestDetails(requestId,
                                            patientId, labId, bloodType, numUnits, priority);
                                    requestsTable.getItems().add(requestDetails);
                                } catch (RemoteException ex) {
                                    Dialogs.showErrorDialog(stage, ex.getMessage(), "Check server and connection", "Server Error");
                                }
                            }
                        });
                    } else if (change.wasRemoved()) {
                        RequestDetails rd = new RequestDetails(change.getKey().getRequestId() + "",
                                null, null, null, 0, null);
                        requestList.remove(rd);
                    }
                }
            });

        } catch (RemoteException | NotBoundException | MalformedURLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(),
                    "Check server and connection.\nClick Ok to Exit", "Server Error");
            //System.exit(0);
        } catch (IOException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(),
                    "File Read Error.\nClick Ok to Exit", "File Error");
            //System.exit(0);
        }
    }

    public void setStage(Stage primaryStage) {
        stage = primaryStage;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        requestsTable.setItems(requestList);
        stackPane.getChildren().clear();
        stackPane.getChildren().add(redCellPane);
        bloodTypes.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                stackPane.getChildren().clear();
                if (newValue == redCellRadioBtn) {
                    stackPane.getChildren().add(redCellPane);
                } else if (newValue == plateletRadioBtn) {
                    stackPane.getChildren().add(plateletPane);
                } else if (newValue == ffpRadioBtn) {
                    stackPane.getChildren().add(ffpPane);
                } else if (newValue == cryoRadioBtn) {
                    stackPane.getChildren().add(cryoPane);
                } else if (newValue == cspRadioBtn) {
                    stackPane.getChildren().add(cspPane);
                }
            }
        });

        cancelButton.setDisable(true);
        reqIdColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, String>("requestId"));
        patientIdColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, String>("patientId"));
        accepterColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, String>("accepterId"));
        bloodTypeColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, String>("bloodType"));
        unitsColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, Integer>("numUnits"));
        priorityColumn.setCellValueFactory(new PropertyValueFactory<RequestDetails, String>("priority"));

        requestsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RequestDetails>() {
            @Override
            public void changed(ObservableValue<? extends RequestDetails> observable, RequestDetails oldValue, RequestDetails newValue) {
                if (newValue != null) {
                    cancelButton.setDisable(false);
                } else {
                    cancelButton.setDisable(true);
                }
            }
        });
        requestsTable.setItems(tableList);

        disableFields();
        disableTransfuHistory();
        parityText.setDisable(true);
        submitRequestButton.setDisable(true);
        patientIdText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.length() > 10) {
                    patientIdText.setText(oldValue);
                }
            }
        });
        requiredAmtText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                amountEntered.set(false);
                if (newValue != null && !newValue.isEmpty()) {
                    if (newValue.length() > 4) {
                        requiredAmtText.setText(oldValue);
                    }
                    try {
                        Integer.parseInt(newValue);
                        amountEntered.set(true);
                    } catch (NumberFormatException ex) {
                        requiredAmtText.setText(oldValue);
                    }
                }
            }
        });
        transHistoryCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    enableTransfuHistory();
                } else {
                    disableTransfuHistory();
                }
            }
        });
        obstetricCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    parityText.setDisable(false);
                } else {
                    parityText.setDisable(true);
                }
            }
        });
        plateletCountText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    Integer.parseInt(newValue);
                    if (newValue.length() > 6) {
                        plateletCountText.setText(oldValue);
                    }
                } catch (NumberFormatException ex) {
                    plateletCountText.setText(oldValue);
                }
            }
        });
        descReactionsText.disableProperty().bind(transReactionsCheck.selectedProperty().not());

        patientLoaded.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    clearStackPaneElements();
                    specialReqText.setText("");
                    requiredAmtText.setText("");
                    enableFields();
                } else {
                    clearStackPaneElements();
                    specialReqText.setText("");
                    requiredAmtText.setText("");
                    disableFields();
                }
            }
        });
        submitRequestButton.disableProperty().bind((patientLoaded.and(amountEntered).not()));
    }

    @FXML
    public void onCancelClicked(ActionEvent event) {
        Dialogs.DialogResponse response = Dialogs.showConfirmDialog(stage,
                "Are you sure you want to cancel the request?", "Confirm Cancellation");
        if (response == Dialogs.DialogResponse.YES) {
            try {
                RequestDetails selectedItem = requestsTable.getSelectionModel().getSelectedItem();
                requesterGuiObservable.cancelRequest(Integer.parseInt(selectedItem.getRequestId()));
            } catch (RemoteException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Error Canceling Request", "Error");
            }
        }
    }

    @FXML
    public void onPatientIdActionPerformed(ActionEvent event) {
        patientNameLabel.setText("");
        unitsToReturnLabel.setText("");
        patientErrorLabel.setText("");
        String pattern = "^(\\d){9}(v|V|x|X)$";
        if (!patientIdText.getText().matches(pattern)) {
            patientErrorLabel.setText("Invalid ID");
            return;
        }

        try {
            String searchId = patientIdText.getText();
            currentPatient = requesterGuiServices.getConfirmReceiver(searchId);

            if (currentPatient != null) {
                patientNameLabel.setText(currentPatient.getName());
                int donations = currentPatient.getReturnAmt();
                String donationStatus = "";
                if (donations < 0) {
                    donationStatus = (donations * -1) + " Blood units to be collected";
                } else {
                    donationStatus = donations + " Donations";
                }
                unitsToReturnLabel.setText(donationStatus);

                patientLoaded.set(true);
            } else {
                currentPatient = null;
                patientLoaded.set(false);
                patientErrorLabel.setText("No Entry Found!");
                patientIdText.selectAll();
            }
        } catch (RemoteException | ClassNotFoundException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error!", "Error");
            patientLoaded.set(false);
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Invalid ID", "Error");
            patientLoaded.set(false);
        }

    }

    @FXML
    public void onSubmitClicked(ActionEvent event) {
        patientErrorLabel.setText("");
        if (currentPatient == null) {
            patientErrorLabel.setText("No patient selected");
            clearStackPaneElements();
            disableFields();
            return;
        }

        BloodType blood;
        if (redCellRadioBtn.isSelected()) {
            blood = loadRedCell();
        } else if (plateletRadioBtn.isSelected()) {
            blood = loadPlatelet();
        } else if (ffpRadioBtn.isSelected()) {
            blood = loadFfp();
        } else if (cryoRadioBtn.isSelected()) {
            blood = loadCryo();
        } else {
            blood = loadCsp();
        }

        String specialReq = specialReqText.getText();
        int reqAmount = Integer.parseInt(requiredAmtText.getText());

        Request request = new Request(currentPatient.getReceiverId(),
                currentPatient.getBloodGroup(),
                blood, reqAmount,
                specialReq, getWardIndex());

        requesterGuiObservable.sendRequest(request);
    }

    @FXML
    public void onExitBtnClick(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    public void onaddPatientBtnClick(ActionEvent event) {
        Stage patientStage = new Stage();
        patientStage.initModality(Modality.APPLICATION_MODAL);
        patientStage.getIcons().add(new Image(RequesterGuiController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;

        try {
            root = FXMLLoader.load(getClass().getResource("addpatientsview/AddPatientView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);

        patientStage.setScene(scene);
        patientStage.setTitle("Add Patient");
        patientStage.show();
    }

    @FXML
    public void onsettingsBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage
                .getIcons().add(new Image(RequesterGuiController.class
                .getResourceAsStream("BloodDrop.png")));
        Parent root = null;


        try {
            root = FXMLLoader.load(getClass().getResource("settingsview/SettingsView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();
    }

    @FXML
    public void onAboutBtnClick(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage
                .getIcons().add(new Image(RequesterGuiController.class
                .getResourceAsStream("BloodDrop.png")));
        Parent root = null;


        try {
            root = FXMLLoader.load(getClass().getResource("aboutview/AboutView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);

        stage.setScene(scene);

        stage.show();
    }

    private void enableTransfuHistory() {
        beforeMonthsCheck.setDisable(false);
        withinMonthsCheck.setDisable(false);
        transReactionsCheck.setDisable(false);
    }

    private void disableTransfuHistory() {
        beforeMonthsCheck.setDisable(true);
        withinMonthsCheck.setDisable(true);
        transReactionsCheck.setDisable(true);
    }

    private void clearStackPaneElements() {
        patientNameLabel.setText("");
        unitsToReturnLabel.setText("");

        routineRadioBtn.setSelected(true);
        transHistoryCheck.setSelected(false);
        beforeMonthsCheck.setSelected(true);
        transReactionsCheck.setSelected(false);
        descReactionsText.setText("");
        obstetricCheck.setSelected(false);
        parityText.setText("");
        currIndicText.setText("");
        hbLevelText.setText("");
        bloodLossText.setText("");
        procedureText.setText("");
        LucoreducedRadio.setSelected(true);

        ffpAcuteCheck.setSelected(false);
        ffpMassiveCheck.setSelected(false);
        ffpLiverCheck.setSelected(false);
        ffpSurgeryCheck.setSelected(false);
        ffpWarfarinCheck.setSelected(false);
        ffpClottingCheck.setSelected(false);
        ffpOtherText.setText("");
        ffpInrText.setText("");
        ffpApttText.setText("");

        plateBmCheck.setSelected(false);
        plateRiscCheck.setSelected(false);
        plateBmBleedingCheck.setSelected(false);
        plateAcuteCheck.setSelected(false);
        plateMassiveCheck.setSelected(false);
        plateSurgeryCheck.setSelected(false);
        plateCnsCheck.setSelected(false);
        plateFuncDefectCheck.setSelected(false);
        plateOtherText.setText("");
        plateletCountText.setText("");

        cryoAcuteCheck.setSelected(false);
        cryoMassiveCheck.setSelected(false);
        cryoLiverCheck.setSelected(false);
        cryoWillibrandCheck.setSelected(false);
        cryoHemopCheck.setSelected(false);
        cryoOtherText.setText("");
        cryoApitText.setText("");

        cspTtpCheck.setSelected(false);
        cspHemopCheck.setSelected(false);
        cspBurnsCheck.setSelected(false);
        cspLiverCheck.setSelected(false);
        cspNephroticCheck.setSelected(false);
        cspOtherText.setText("");
        cspAlbuminText.setText("");

        specialReqText.setText("");
        requiredAmtText.setText("");
    }

    private void disableFields() {
        redCellRadioBtn.setDisable(true);
        plateletRadioBtn.setDisable(true);
        ffpRadioBtn.setDisable(true);
        cryoRadioBtn.setDisable(true);
        cspRadioBtn.setDisable(true);
        specialReqText.setDisable(true);
        requiredAmtText.setDisable(true);
        stackPane.setDisable(true);
    }

    private void enableFields() {
        redCellRadioBtn.setDisable(false);
        plateletRadioBtn.setDisable(false);
        ffpRadioBtn.setDisable(false);
        cryoRadioBtn.setDisable(false);
        cspRadioBtn.setDisable(false);
        specialReqText.setDisable(false);
        requiredAmtText.setDisable(false);
        stackPane.setDisable(false);
    }

    private RedCell loadRedCell() {
        Priority priority;
        RedCellSpecialReq specialReq;

        if (urgentRadioBtn.isSelected()) {
            priority = Priority.URGENT;
        } else if (emergencyRadioBtn.isSelected()) {
            priority = Priority.EMERGENCY;
        } else {
            priority = Priority.REGULAR;
        }

        if (LucoreducedRadio.isSelected()) {
            specialReq = RedCellSpecialReq.LUCOREDUCED;
        } else if (WashedRadio.isSelected()) {
            specialReq = RedCellSpecialReq.WASHED;
        } else {
            specialReq = RedCellSpecialReq.IRRADIATED;
        }

        RedCell redCell = new RedCell(
                priority,
                transHistoryCheck.isSelected(),
                withinMonthsCheck.isSelected(),
                transReactionsCheck.isSelected(),
                descReactionsText.getText(),
                obstetricCheck.isSelected(),
                parityText.getText(),
                currIndicText.getText(),
                hbLevelText.getText(),
                bloodLossText.getText(),
                procedureText.getText(),
                specialReq);

        return redCell;
    }

    private CRYO loadCryo() {
        return new CRYO(
                cryoAcuteCheck.isSelected(),
                cryoMassiveCheck.isSelected(),
                cryoLiverCheck.isSelected(),
                cryoWillibrandCheck.isSelected(),
                cryoHemopCheck.isSelected(),
                cryoOtherText.getText(),
                cryoApitText.getText());
    }

    private CSP loadCsp() {
        return new CSP(
                cspTtpCheck.isSelected(),
                cspHemopCheck.isSelected(),
                cspBurnsCheck.isSelected(),
                cspLiverCheck.isSelected(),
                cspNephroticCheck.isSelected(),
                cspOtherText.getText(),
                cspAlbuminText.getText());
    }

    private FFPlasma loadFfp() {
        return new FFPlasma(
                ffpAcuteCheck.isSelected(),
                ffpMassiveCheck.isSelected(),
                ffpLiverCheck.isSelected(),
                ffpSurgeryCheck.isSelected(),
                ffpWarfarinCheck.isSelected(),
                ffpClottingCheck.isSelected(),
                ffpOtherText.getText(),
                ffpInrText.getText(),
                ffpApttText.getText());
    }

    private Platelet loadPlatelet() {
        int count = 0;
        try {
            count = Integer.parseInt(plateletCountText.getText());
        } catch (NumberFormatException ex) {
        }
        return new Platelet(
                plateBmCheck.isSelected(),
                plateRiscCheck.isSelected(),
                plateBmBleedingCheck.isSelected(),
                plateAcuteCheck.isSelected(),
                plateMassiveCheck.isSelected(),
                plateSurgeryCheck.isSelected(),
                plateCnsCheck.isSelected(),
                plateFuncDefectCheck.isSelected(),
                plateOtherText.getText(), count);
    }

    private int getWardIndex() {
        return wardId;
    }

    void setNewStatus(int requestId, String status) {
        RequestDetails requestDummy = new RequestDetails(requestId + "", null, null, null, 0, null);
        RequestDetails requestInTable = tableList.get(tableList.indexOf(requestDummy));
        requestInTable.setStatus(status);
        tableList.remove(requestInTable);
       tableList.add(requestInTable);
    }
}
//int requestId = requestId_ObservLab.getRequest().getRequestId();
//
//            //Observer Usage
//            boolean b = labObserver.cancleRequest(requestId);
//            b = labObserver.changePriority(requestId, Priority.EMERGENCY);
//            b = labObserver.updateRequest(requestId, request);

