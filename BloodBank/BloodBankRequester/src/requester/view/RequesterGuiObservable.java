/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.view;

import bloodbankcommon.model.Request;
import bloodbankcommon.model.Request_ObservLab;
import bloodbankcommon.observer.LabObserver;
import bloodbankcommon.observer.RequestObserver;
import bloodbankcommon.observer.notifier.RequestNotifier;
import bloodbankcommon.request.RequestHandlerIntf;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Properties;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.control.Dialogs;
import javafx.stage.Stage;
import requester.connector.ServerConnector;
import requester.observerimpl.RequestObserverImpl;

/**
 *
 * @author Janith
 */
public class RequesterGuiObservable implements RequestNotifier {

    protected ObservableMap<Request, LabObserver> hMap_requestId_ObservLabs;
    private RequesterGuiController requesterGuiController;
    private RequestObserver requestObserver;
    private RequestHandlerIntf requestHandler;
    protected int wardId;
    private Stage stage;

    RequesterGuiObservable(RequesterGuiController guiController, Stage stage) throws RemoteException, NotBoundException, MalformedURLException, IOException {
        this.requestHandler = ServerConnector.getServerConnector().getRequestHandler();
        this.hMap_requestId_ObservLabs = FXCollections.observableHashMap();
        this.requesterGuiController = guiController;
        this.wardId = loadMyId();
        this.requestObserver = new RequestObserverImpl(this, wardId);
        this.stage = stage;
        requestHandler.addWard(requestObserver, wardId);
    }

    void sendRequest(final Request request) {
        try {
            Request_ObservLab requestId_ObservLab = requestHandler.sendRequest(request);
            hMap_requestId_ObservLabs.put(requestId_ObservLab.getRequest(), requestId_ObservLab.getLabObserver());
        } catch (RemoteException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error", "Error");
        }
    }

    void cancelRequest(final int requestId) throws RemoteException {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Request fakeRequest = new Request(requestId);
                    LabObserver observer = hMap_requestId_ObservLabs.get(fakeRequest);
                    observer.cancleRequest(requestId);
                    hMap_requestId_ObservLabs.remove(fakeRequest);
                } catch (RemoteException ex) {
                    Dialogs.showErrorDialog(stage, ex.getMessage(), "Server Error", "Error");
                }
            }
        });
    }

    private int loadMyId() throws IOException {
        Properties properties = new Properties();
        File file = new File("MyConfig.data");
        boolean fileCreated = file.createNewFile();
        int num = 0;
        if (fileCreated) {
            String response = Dialogs.showInputDialog(stage, "Please Enter the Ward Number", "Configuration file missing");
            try{
                num = Integer.parseInt(response);
                properties.setProperty("myId", num + "");
                properties.store(new FileOutputStream(file), "");
            }catch(NumberFormatException ex){                
            }
        }
        properties.load(new FileInputStream(file));
        String property = properties.getProperty("myId") == null ? num + "" : properties.getProperty("myId");
        return Integer.parseInt(property);
    }

    @Override
    public void setRequestStatus(int requestId, String status) {
        requesterGuiController.setNewStatus(requestId, status);
    }
}
