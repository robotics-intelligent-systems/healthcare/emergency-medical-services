/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.view.addpatientsview;

import bloodbankcommon.model.Receiver;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.TextField;
import requester.connector.ServerConnector;

/**
 * FXML Controller class
 *
 * @author Janith
 */
public class AddPatientViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField patientId;
    @FXML
    private TextField patientName;
    @FXML
    private TextField patientAddress;
    @FXML
    private TextField patientTele;
    @FXML
    private TextField patientAge;
    @FXML
    private ComboBox bloodGroup;
    @FXML
    private ComboBox bloodRH;
    @FXML
    private ComboBox patientSex;
    @FXML
    private Button saveButton;
    private BooleanProperty pId = new SimpleBooleanProperty(false);
    private BooleanProperty pName = new SimpleBooleanProperty(false);
    private BooleanProperty pAge = new SimpleBooleanProperty(false);
    private BooleanProperty pAddress = new SimpleBooleanProperty(false);
    private BooleanProperty pTele = new SimpleBooleanProperty(false);

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        patientId.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    pId.set(false);
                } else {
                    pId.set(true);
                    if (newValue.length() > 10) {
                        patientId.setText(oldValue);
                    }
                }
            }
        });
        patientName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    pName.set(false);
                } else {
                    pName.set(true);
                }
            }
        });
        patientTele.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    pTele.set(false);
                } else {
                    if (newValue.length() > 10) {
                        patientTele.setText(oldValue);
                        return;
                    }
                    if (!newValue.matches("^[0-9]+$")) {
                        patientTele.setText(oldValue);
                        return;
                    }
                    if (newValue.matches("^[0-9]{10}$")) {
                        pTele.set(true);
                    } else {
                        pTele.set(false);
                    }
                }

            }
        });
        patientAddress.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    pAddress.set(false);
                } else {
                    pAddress.set(true);
                }
            }
        });
        patientAge.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == null || newValue.isEmpty()) {
                    pAge.set(false);
                } else {
                    if (newValue.length() > 2) {
                        patientAge.setText(oldValue);
                    }
                    if (!newValue.matches("^[0-9]+$")) {
                        patientAge.setText(oldValue);
                        return;
                    }
                    if (newValue.matches("^[0-9]{2}$")) {
                        pAge.set(true);
                    } else {
                        pTele.set(false);
                    }
                }
            }
        });

        bloodGroup.getSelectionModel().select(0);
        bloodRH.getSelectionModel().select(0);
        patientSex.getSelectionModel().select(0);

        saveButton.disableProperty().bind((pId.and(pAge).and(pAddress).and(pName).and(pTele)).not());
    }

    @FXML
    public void addPatient() {
        String bloodType = bloodGroup.getSelectionModel().getSelectedItem().toString().concat(bloodRH.getSelectionModel().getSelectedItem().toString());
        Receiver receiver = new Receiver(patientId.getText(), patientName.getText(), bloodType,
                patientAddress.getText(), patientSex.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(patientTele.getText()));
        try {
            boolean status = ServerConnector.getServerConnector().getReceiverControllerIntf().addPatient(receiver);
            if (status) {
                Dialogs.showInformationDialog(null, "Saved");
            } else {
                Dialogs.showErrorDialog(null, "Faild to Save");
            }
        } catch (RemoteException | ClassNotFoundException | SQLException | NotBoundException | MalformedURLException | PropertyVetoException ex) {
            Dialogs.showErrorDialog(null, ex.getMessage());
        }

    }
}
