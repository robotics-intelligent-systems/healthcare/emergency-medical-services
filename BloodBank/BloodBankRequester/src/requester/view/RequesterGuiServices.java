/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.view;

import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.model.Receiver;
import java.beans.PropertyVetoException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import requester.connector.ServerConnector;

/**
 *
 * @author oshanz
 */
public class RequesterGuiServices {

    private DonorControllerIntf donorController = null;
    private DonationControllerIntf donationsController = null;
    private SeperationControllerIntf seperationController = null;
    private BloodTypeControllerIntf bloodController = null;
    private ReceiverControllerIntf receiverController = null;

    public RequesterGuiServices() throws NotBoundException, MalformedURLException, RemoteException {
        donorController = ServerConnector.getServerConnector().getDonorControllerIntf();
        donationsController = ServerConnector.getServerConnector().getDonationControllerIntf();
        seperationController = ServerConnector.getServerConnector().getSeperationControllerIntf();
        bloodController = ServerConnector.getServerConnector().getBloodTypeControllerIntf();
        receiverController = ServerConnector.getServerConnector().getReceiverControllerIntf();
    }

    Receiver getConfirmReceiver(String searchId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverController.getConfirmReceiver(searchId);
    }
}
