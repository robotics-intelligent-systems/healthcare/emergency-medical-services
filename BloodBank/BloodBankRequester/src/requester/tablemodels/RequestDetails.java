/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package requester.tablemodels;

import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Janith
 */
public class RequestDetails {

    private StringProperty requestId;
    private StringProperty patientId;
    private StringProperty accepterId;
    private StringProperty bloodType;
    private IntegerProperty numUnits;
    private StringProperty priority;
    private StringProperty status;

    public RequestDetails(String requestId, String patientId, String accepterId, String bloodType, int numUnits, String priority) {
        this.requestId = new SimpleStringProperty(requestId);
        this.patientId = new SimpleStringProperty(patientId);
        this.accepterId = new SimpleStringProperty(accepterId);
        this.bloodType = new SimpleStringProperty(bloodType);
        this.numUnits = new SimpleIntegerProperty(numUnits);
        this.priority = new SimpleStringProperty(priority);
        this.status = new SimpleStringProperty("");
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getRequestId() {
        return requestId.get();
    }

    public void setRequestId(String requestId) {
        this.requestId.set(requestId);
    }

    public String getPatientName() {
        return patientId.get();
    }

    public void setPatientName(String patientName) {
        this.patientId.set(patientName);
    }

    public String getAccepterId() {
        return accepterId.get();
    }

    public void setAccepterId(String accepterId) {
        this.accepterId.set(accepterId);
    }

    public String getBloodType() {
        return bloodType.get();
    }

    public void setBloodType(String bloodType) {
        this.bloodType.set(bloodType);
    }

    public int getNumUnits() {
        return numUnits.get();
    }

    public void setNumUnits(int numUnits) {
        this.numUnits.set(numUnits);
    }

    public String getPriority() {
        return priority.get();
    }

    public void setPriority(String priority) {
        this.priority.set(priority);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RequestDetails other = (RequestDetails) obj;
        if (!Objects.equals(this.requestId.get(), other.requestId.get())) {
            return false;
        }
        return true;
    }
}
