CREATE DATABASE  IF NOT EXISTS `bloodbank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bloodbank`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: bloodbank
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `platelet`
--

DROP TABLE IF EXISTS `platelet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platelet` (
  `plateletId` varchar(10) NOT NULL,
  `expiaryDate` date NOT NULL,
  `bloodGroup` varchar(3) NOT NULL,
  `sentDate` date DEFAULT NULL,
  `receiverId` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`plateletId`),
  KEY `receiverId_idx` (`receiverId`),
  CONSTRAINT `plateletId` FOREIGN KEY (`plateletId`) REFERENCES `seperations` (`plateletId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `plateletReceiverId` FOREIGN KEY (`receiverId`) REFERENCES `receiver` (`receiverId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platelet`
--

LOCK TABLES `platelet` WRITE;
/*!40000 ALTER TABLE `platelet` DISABLE KEYS */;
INSERT INTO `platelet` VALUES ('12KA36456','2012-03-01','A-',NULL,NULL),('12KA42356','2012-02-08','AB+',NULL,NULL),('12KA47665','2012-08-23','A-',NULL,NULL),('12KA53452','2012-03-26','A-',NULL,NULL),('12KA64565','2012-04-26','O+',NULL,NULL),('12KA66456','2012-09-30','O+',NULL,NULL),('12KA82345','2013-09-29','AB+',NULL,NULL),('13KA47567','2013-06-03','B+',NULL,NULL),('13KA56756','2013-05-17','B-',NULL,NULL),('13KA73033','2013-10-07','B+',NULL,NULL),('13KA93255','2013-01-09','B+',NULL,NULL);
/*!40000 ALTER TABLE `platelet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-19 12:55:54
