CREATE DATABASE  IF NOT EXISTS `bloodbank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bloodbank`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: bloodbank
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ffplasma`
--

DROP TABLE IF EXISTS `ffplasma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ffplasma` (
  `ffplasmaId` varchar(10) NOT NULL,
  `expiaryDate` date NOT NULL,
  `bloodGroup` varchar(3) NOT NULL,
  `sentDate` date DEFAULT NULL,
  `receiverId` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`ffplasmaId`),
  KEY `receiverId_idx` (`receiverId`),
  KEY `ffplasmaId` (`ffplasmaId`),
  KEY `receiverId` (`receiverId`),
  KEY `ffplasmId` (`ffplasmaId`),
  CONSTRAINT `ffplasmaReceiverId` FOREIGN KEY (`receiverId`) REFERENCES `receiver` (`receiverId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ffplasmId` FOREIGN KEY (`ffplasmaId`) REFERENCES `seperations` (`ffplasmaId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ffplasma`
--

LOCK TABLES `ffplasma` WRITE;
/*!40000 ALTER TABLE `ffplasma` DISABLE KEYS */;
INSERT INTO `ffplasma` VALUES ('12KA36456','2013-02-25','A-',NULL,NULL),('12KA47665','2013-08-18','A-',NULL,NULL),('12KA64565','2013-04-21','O+',NULL,NULL),('12KA66456','2013-09-25','O+',NULL,NULL),('12KA82345','2014-09-24','AB+',NULL,NULL),('13KA47567','2014-05-29','B+',NULL,NULL),('13KA73033','2014-10-02','B+',NULL,NULL);
/*!40000 ALTER TABLE `ffplasma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-19 12:55:51
