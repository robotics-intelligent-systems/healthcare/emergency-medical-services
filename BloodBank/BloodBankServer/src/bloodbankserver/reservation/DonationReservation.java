/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.reservation;

import bloodbankserver.controller.DonationControllerImpl;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author oshanz
 */
public class DonationReservation {
     private Map<String, DonationControllerImpl> customerMap;

    public DonationReservation() {
        customerMap = new HashMap<>();
    }

    public boolean reseveCustomer(String id, DonationControllerImpl donationControllerImpl) {
        if (customerMap.containsKey(id)) {
            if (customerMap.get(id) == donationControllerImpl) {
                return true;
            } else {
                return false;
            }
        } else {
            customerMap.put(id, donationControllerImpl);
            return true;
        }
    }

    public boolean releaseCustomer(String id, DonationControllerImpl donationControllerImpl) {
        if (customerMap.get(id) == donationControllerImpl) {
            customerMap.remove(id);
            return true;
        } else {
            return false;
        }
    }
}
