/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.reservation;

import bloodbankserver.controller.DonorControllerImpl;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author oshanz
 */
public class DonorReservation {
     private Map<String, DonorControllerImpl> customerMap;

    public DonorReservation() {
        customerMap = new HashMap<>();
    }

    public boolean reseveCustomer(String id, DonorControllerImpl donorControllerImpl) {
        if (customerMap.containsKey(id)) {
            if (customerMap.get(id) == donorControllerImpl) {
                return true;
            } else {
                return false;
            }
        } else {
            customerMap.put(id, donorControllerImpl);
            return true;
        }
    }

    public boolean releaseCustomer(String id, DonorControllerImpl donorControllerImpl) {
        if (customerMap.get(id) == donorControllerImpl) {
            customerMap.remove(id);
            return true;
        } else {
            return false;
        }
    }
}
