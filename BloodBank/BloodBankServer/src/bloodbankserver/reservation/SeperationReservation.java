/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.reservation;

import bloodbankserver.controller.SeperationControllerImpl;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author oshanz
 */
public class SeperationReservation {
     private Map<String, SeperationControllerImpl> customerMap;

    public SeperationReservation() {
        customerMap = new HashMap<>();
    }

    public boolean reseveCustomer(String id, SeperationControllerImpl seperationControllerImpl) {
        if (customerMap.containsKey(id)) {
            if (customerMap.get(id) == seperationControllerImpl) {
                return true;
            } else {
                return false;
            }
        } else {
            customerMap.put(id, seperationControllerImpl);
            return true;
        }
    }

    public boolean releaseCustomer(String id, SeperationControllerImpl seperationControllerImpl) {
        if (customerMap.get(id) == seperationControllerImpl) {
            customerMap.remove(id);
            return true;
        } else {
            return false;
        }
    }
}
