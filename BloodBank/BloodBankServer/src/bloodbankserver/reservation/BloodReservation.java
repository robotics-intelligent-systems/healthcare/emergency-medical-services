/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.reservation;

import bloodbankserver.controller.BloodTypeControllerImpl;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author oshanz
 */
public class BloodReservation {
    private Map<String, BloodTypeControllerImpl> customerMap;

    public BloodReservation() {
        customerMap = new HashMap<>();
    }

    public boolean reseveCustomer(String id, BloodTypeControllerImpl bloodTypeControllerImpl) {
        if (customerMap.containsKey(id)) {
            if (customerMap.get(id) == bloodTypeControllerImpl) {
                return true;
            } else {
                return false;
            }
        } else {
            customerMap.put(id, bloodTypeControllerImpl);
            return true;
        }
    }

    public boolean releaseCustomer(String id, BloodTypeControllerImpl bloodTypeControllerImpl) {
        if (customerMap.get(id) == bloodTypeControllerImpl) {
            customerMap.remove(id);
            return true;
        } else {
            return false;
        }
    }
}
