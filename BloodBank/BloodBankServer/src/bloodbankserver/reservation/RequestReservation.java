/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.reservation;

import bloodbankserver.controller.RequesterControllerImpl;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author oshanz
 */
public class RequestReservation {
     private Map<String, RequesterControllerImpl> customerMap;

    public RequestReservation() {
        customerMap = new HashMap<>();
    }

    public boolean reseveCustomer(String id, RequesterControllerImpl requesterControllerImpl) {
        if (customerMap.containsKey(id)) {
            if (customerMap.get(id) == requesterControllerImpl) {
                return true;
            } else {
                return false;
            }
        } else {
            customerMap.put(id, requesterControllerImpl);
            return true;
        }
    }

    public boolean releaseCustomer(String id, RequesterControllerImpl requesterControllerImpl) {
        if (customerMap.get(id) == requesterControllerImpl) {
            customerMap.remove(id);
            return true;
        } else {
            return false;
        }
    }
}
