/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankcommon.model.Donor;
import bloodbankserver.dbhandler.DBConnection;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author oshanz
 */
public class DonorDBAccess {

    private DBHandler dBHandler = new DBHandler();

    public Donor getDonorSimple(String nic) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select name,bloodGroup from donor where nic = ?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, nic);
            Donor donor = null;
            if (executeQuery.next()) {
                donor = new Donor(nic, executeQuery.getString("name"), executeQuery.getString("bloodGroup"));
            }
            return donor;
        }
    }

    public String getBloodGroup(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select bloodGroup from donor WHERE nic=(select nic from donation WHERE bloodId=?)";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            String bloodGroup = null;
            if (executeQuery.next()) {
                bloodGroup = executeQuery.getString("bloodGroup");
            }
            return bloodGroup;
        }
    }

    public Donor getDonor(String donorId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select `name`,address,tele,bloodGroup,rejected,`count`,sex from donor WHERE nic=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, donorId);
            Donor donor = null;
            if (executeQuery.next()) {
                donor = new Donor(donorId, executeQuery.getString("name"), executeQuery.getString("address"), executeQuery.getString("bloodGroup"),
                        executeQuery.getString("rejected"), executeQuery.getString("sex"), executeQuery.getInt("tele"), executeQuery.getInt("count"));
            }
            return donor;
        }
    }

    public boolean getDonorExists(String id) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "select nic from donor WHERE nic=? limit 1";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, id);
            if (executeQuery.next()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean addDonor(Donor donor) throws SQLException, ClassNotFoundException, PropertyVetoException {
        String query = "INSERT INTO donor VALUES(?,?,?,?,?,?,?,?)";
        return dBHandler.setData(query, donor.getNic(), donor.getName(), donor.getAddress(), donor.getTele(), donor.getBloodGroup(),
                donor.getRejected(), donor.getCount(), donor.getSex());
    }

    public Donor getDonorLess(String donorId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        //id, name, addr, tel, sex, bGroup
        //Others MUST BE NULL!
        String query = "select `name`,address,tele,sex,bloodGroup from donor WHERE nic=?";
        Donor donor = null;
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, donorId);
            if (executeQuery.next()) {
                donor = new Donor(donorId, executeQuery.getString("name"), executeQuery.getString("address"),
                        executeQuery.getString("bloodGroup"), null, executeQuery.getString("sex"), executeQuery.getInt("tele"), 0);
            }
            return donor;
        }
    }

    public boolean editDonor(Donor donor) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "UPDATE donor SET address=?,tele=?,bloodGroup=? WHERE nic=?";
        return dBHandler.setData(query, donor.getAddress(), donor.getTele(), donor.getBloodGroup());
    }
}
