/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankcommon.bloodtype.BloodTypes;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Receiver;
import bloodbankserver.dbhandler.DBConnection;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public class ReceiverDBAccess {

    private DBHandler dBHandler = new DBHandler();
    private String returnAmt;

    public Receiver getConfirmReceiver(String searchId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId,name,bloodGroup,returnedAmt from receiver WHERE receiverId= ?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, searchId);
            Receiver receiver = null;
            if (executeQuery.next()) {
                receiver = new Receiver(executeQuery.getString("receiverId"),
                        executeQuery.getString("name"), executeQuery.getInt("returnedAmt"));
                receiver.setBloodGroup(executeQuery.getString("bloodGroup"));
            }
            return receiver;
        }
    }

    public Receiver getReceiverSimple(String id) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select `name`,bloodGroup from receiver WHERE receiverId=?";
        Receiver receiver = null;
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, id);
            if (executeQuery.next()) {
                receiver = new Receiver(id, executeQuery.getString("name"), executeQuery.getString("bloodGroup"));
            }
            return receiver;
        }
    }

    public ObservableList<Donation> getTransfusionsOf(String patient) throws ClassNotFoundException, SQLException, PropertyVetoException {
        //need all blood given to thie patient in a DONATION MODEL!
        //Fields - trsn date, blood type(int), blood id;
        //AND DONOR NAME in Status;

        ObservableList<Donation> observableArrayList = FXCollections.observableArrayList();

        try (Connection connection = DBConnection.getDBConnection().getConnection();) {
            String query = "SELECT cryoId,nic,donationDate from cryo c, donation d WHERE c.cryoId=d.bloodId && c.cryoId=?";
            try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
                ResultSet executeQuery = dBHandler.getData(prepareStatement, patient);
                while (executeQuery.next()) {
                    String dDate = executeQuery.getDate("donationDate").toString();
                    Donation donation = new Donation(executeQuery.getString("cryoId"), dDate, null, executeQuery.getString("nic"),
                            BloodTypes.CRYO.getIndex());
                    observableArrayList.add(donation);
                }
            }

            query = "SELECT cspId,nic,donationDate from csp c, donation d WHERE c.cspId=d.bloodId && c.cspId=?";
            try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
                ResultSet executeQuery = dBHandler.getData(prepareStatement, patient);
                while (executeQuery.next()) {
                    String dDate = executeQuery.getDate("donationDate").toString();
                    Donation donation = new Donation(executeQuery.getString("cspId"), dDate, null, executeQuery.getString("nic"),
                            BloodTypes.CSP.getIndex());
                    observableArrayList.add(donation);
                }
            }

            query = "SELECT plateletId,nic,donationDate from platelet c, donation d WHERE c.plateletId=d.bloodId && c.plateletId=?";
            try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
                ResultSet executeQuery = dBHandler.getData(prepareStatement, patient);
                while (executeQuery.next()) {
                    String dDate = executeQuery.getDate("donationDate").toString();
                    Donation donation = new Donation(executeQuery.getString("plateletId"), dDate, null, executeQuery.getString("nic"),
                            BloodTypes.PLATELET.getIndex());
                    observableArrayList.add(donation);
                }
            }

            query = "SELECT ffplasmaId,nic,donationDate from ffplasma c, donation d WHERE c.ffplasmaId=d.bloodId && c.ffplasmaId=?";
            try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
                ResultSet executeQuery = dBHandler.getData(prepareStatement, patient);
                while (executeQuery.next()) {
                    String dDate = executeQuery.getDate("donationDate").toString();
                    Donation donation = new Donation(executeQuery.getString("ffplasmaId"), dDate, null, executeQuery.getString("nic"),
                            BloodTypes.PLASMA.getIndex());
                    observableArrayList.add(donation);
                }
            }

            query = "SELECT redCellId,nic,donationDate from redcell c, donation d WHERE c.redCellId=d.bloodId && c.redCellId=?";
            try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
                ResultSet executeQuery = dBHandler.getData(prepareStatement, patient);
                while (executeQuery.next()) {
                    String dDate = executeQuery.getDate("donationDate").toString();
                    Donation donation = new Donation(executeQuery.getString("redCellId"), dDate, null, executeQuery.getString("nic"),
                            BloodTypes.RED_CELL.getIndex());
                    observableArrayList.add(donation);
                }
            }
            return observableArrayList;
        }
    }

    public boolean addPatient(Receiver receiver) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "INSERT INTO receiver VALUES(?,?,?,?,0,?,?)";
        return dBHandler.setData(query, receiver.getReceiverId(), receiver.getName(), receiver.getAddress(), receiver.getTele(),
                receiver.getSex(), receiver.getBloodGroup());
    }

    public Receiver getHospital(String hospitalId) throws ClassNotFoundException, PropertyVetoException, SQLException {
        //Need id, name, addr, tel;
        //if not exists, id = null;
        String query = "select `name`,address,tele from bloodbank.receiver WHERE receiverId=?";

        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, hospitalId);
            Receiver receiver = null;
            while (executeQuery.next()) {
                receiver = new Receiver(hospitalId, executeQuery.getString("name"), executeQuery.getString("address"),
                        executeQuery.getInt("tele"));
            }
            return receiver;
        }
    }

    public boolean updateHospital(Receiver hospital) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "UPDATE bloodbank.receiver SET `name`=?,address=?,tele=? WHERE receiverId=?";
        return dBHandler.setData(query, hospital.getName(), hospital.getAddress(), hospital.getTele(), hospital.getReceiverId());
    }

    public boolean addHospital(Receiver hospital) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "INSERT INTO bloodbank.receiver(receiverId,`name`,address,tele) VALUES(?,?,?,?)";
        return dBHandler.setData(query, hospital.getReceiverId(), hospital.getName(), hospital.getAddress(), hospital.getTele());
    }

    public boolean submitOutsourcings(String hospId, List<Blood> bloodList) throws ClassNotFoundException, PropertyVetoException, SQLException {
        //not null - bId, bType
        //Use autocommits
        Connection connection = null;
        try {
            connection = DBConnection.getDBConnection().getConnection();
            connection.setAutoCommit(false);
            boolean completed = true;
            for (Blood blood : bloodList) {
                String query;
                boolean success = true;
                switch (blood.getBloodType()) {
                    case ("Red Cell"):
                        query = "UPDATE bloodbank.redcell SET receiverId=?,sentDate=CURDATE() WHERE redcellId=?";
                        success = dBHandler.setData(connection, query, blood.getReceiverId(), blood.getBloodId());
                        break;
                    case ("Platelet"):
                        query = "UPDATE bloodbank.platelet SET receiverId=?,sentDate=CURDATE() WHERE plateletId=?";
                        success = dBHandler.setData(connection, query, blood.getReceiverId(), blood.getBloodId());
                        break;
                    case ("Plasma"):
                        query = "UPDATE bloodbank.ffplasma SET receiverId=?,sentDate=CURDATE() WHERE ffplasmaId=?";
                        success = dBHandler.setData(connection, query, blood.getReceiverId(), blood.getBloodId());
                        break;
                    case ("CRYO"):
                        query = "UPDATE bloodbank.cryo SET receiverId=?,sentDate=CURDATE() WHERE cryoId=?";
                        success = dBHandler.setData(connection, query, blood.getReceiverId(), blood.getBloodId());
                        break;
                    case ("CSP"):
                        query = "UPDATE bloodbank.csp SET receiverId=?,sentDate=CURDATE() WHERE cspId=?";
                        success = dBHandler.setData(connection, query, blood.getReceiverId(), blood.getBloodId());
                        break;
                }
                if (!success) {
                    connection.rollback();
                    completed = false;
                    break;
                }
            }
            if (completed) {
                connection.commit();
                return true;
            } else {
                connection.rollback();
                return false;
            }
        } finally {
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close();
            }
        }
    }

    public String getNextHospitalId() throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "select receiverId from receiver WHERE sex is null || sex=\"\" ORDER BY receiverId desc limit 1;";
        Connection connection = DBConnection.getDBConnection().getConnection();
        PreparedStatement prepareStatement = connection.prepareStatement(query);
        ResultSet executeQuery = dBHandler.getData(prepareStatement);
        String id = null;
        if (executeQuery.next()) {
            id = executeQuery.getString("idstudent");
        }
        return id;
    }
}
