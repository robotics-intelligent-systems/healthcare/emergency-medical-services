/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankserver.dbhandler.DBConnection;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Seperation;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author oshanz
 */
public class SeperationDBAccess {

    private DBHandler dBHandler = new DBHandler();

    public Seperation getSeperationOf(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select cryoId,plateletId,ffplasmaId,redcellId,cspplasmaId from seperations WHERE bloodId=?";
        Seperation seperation = null;
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            if (executeQuery.next()) {
                seperation = new Seperation(bloodId, executeQuery.getString("cryoId"), executeQuery.getString("plateletId"),
                        executeQuery.getString("ffplasmaId"), executeQuery.getString("redcellId"), executeQuery.getString("cspplasmaId"));
            }
            return seperation;
        }
    }

    public boolean submitSeperation(Seperation seperation, List<Blood> bloodList) throws ClassNotFoundException, SQLException, PropertyVetoException {
        Connection connection = null;
        String query = "INSERT INTO seperations VALUES(?,?,?,?,?,?)";
        try {
            connection = DBConnection.getDBConnection().getConnection();
            connection.setAutoCommit(false);
            boolean executeUpdate = dBHandler.setData(connection, query, seperation.getBloodId(), seperation.getCryoId(), seperation.getPlateletId(),
                    seperation.getFfplasmaId(), seperation.getRedcellId(), seperation.getCspplamaId());
            if (executeUpdate) {
                int size = bloodList.size();
                int now = 0;
                for (Blood blood : bloodList) {
                    String queryBLOOD;
                    boolean executeUpdateBLOOD;
                    String bloodType = blood.getBloodType();
                    switch (bloodType) {
                        case ("Red Cell"):
                            queryBLOOD = "INSERT INTO redcell VALUES(?,?,?,null,null,null)";
                            executeUpdateBLOOD = dBHandler.setData(connection, queryBLOOD, blood.getBloodId(), new Date(blood.getExpiaryDate().getTime()), blood.getBloodGroup());
                            if (executeUpdateBLOOD) {
                                now++;
                            } else {
                                connection.rollback();
                            }
                            break;
                        case ("Platelet"):
                            queryBLOOD = "INSERT INTO platelet VALUES(?,?,?,null,null)";
                            executeUpdateBLOOD = dBHandler.setData(connection, queryBLOOD, blood.getBloodId(), new Date(blood.getExpiaryDate().getTime()), blood.getBloodGroup());
                            if (executeUpdateBLOOD) {
                                now++;
                            } else {
                                connection.rollback();
                            }
                            break;
                        case ("Plasma"):
                            queryBLOOD = "INSERT INTO ffplasma VALUES(?,?,?,null,null)";
                            executeUpdateBLOOD = dBHandler.setData(connection, queryBLOOD, blood.getBloodId(), new Date(blood.getExpiaryDate().getTime()), blood.getBloodGroup());
                            if (executeUpdateBLOOD) {
                                now++;
                            } else {
                                connection.rollback();
                            }
                            break;
                        case ("CRYO"):
                            queryBLOOD = "INSERT INTO cryo VALUES(?,?,?,null,null)";
                            executeUpdateBLOOD = dBHandler.setData(connection, queryBLOOD, blood.getBloodId(), new Date(blood.getExpiaryDate().getTime()), blood.getBloodGroup());
                            if (executeUpdateBLOOD) {
                                now++;
                            } else {
                                connection.rollback();
                            }
                            break;
                        case ("CSP"):
                            queryBLOOD = "INSERT INTO csp VALUES(?,?,?,null,null)";
                            executeUpdateBLOOD = dBHandler.setData(connection, queryBLOOD, blood.getBloodId(), new Date(blood.getExpiaryDate().getTime()), blood.getBloodGroup());
                            if (executeUpdateBLOOD) {
                                now++;
                            } else {
                                connection.rollback();
                            }
                            break;
                    }
                }
                if (size == now) {
                    connection.commit();
                    return true;
                } else {
                    connection.rollback();
                    return false;
                }
            } else {
                connection.rollback();
                return false;
            }
        } finally {
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close();
            }
        }
    }
}
