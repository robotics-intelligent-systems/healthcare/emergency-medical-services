/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankserver.dbhandler.DBConnection;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Janith
 */
public class LoginDBAccess {
    
    private DBHandler dBHandler = new DBHandler();
    
    public boolean check() throws ClassNotFoundException, SQLException, PropertyVetoException {        
        String query = "SELECT name from users WHERE idusers = ?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, 0);
            String string = "";                
            if (executeQuery.next()) {
                string = executeQuery.getString("name");
            }
            return string.equals("null");
        }
    }    

    public boolean login(String user, String hash, String type) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "SELECT password, userType from users WHERE name = ?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, user);
            String hashInDB = "";
            String typeInDB = "";
            if (executeQuery.next()) {
                hashInDB = executeQuery.getString("password").toLowerCase();
                typeInDB = executeQuery.getString("userType");                
            }else{
                return false;
            }
            if(hashInDB == null | type == null){
                return false;
            }
            return (hash.toLowerCase().equals(hashInDB) & typeInDB.equals(type));
        }
    }
}
