/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankcommon.model.Donation;
import bloodbankserver.dbhandler.DBConnection;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public class DonationDBAccess {

    private DBHandler dBHandler = new DBHandler();

    public String getBloodType(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select type from donation WHERE bloodId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            String bloodType = null;
            if (executeQuery.next()) {
                bloodType = executeQuery.getString("type");
            }
            return bloodType;
        }
    }

    public boolean addDonation(Donation donation) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "INSERT INTO donation(bloodId,`type`,nic,donationDate)  VALUES(?,?,?,CURDATE())";
        Connection connection = null;
        PreparedStatement prepareStatement = null;
        try {
            connection = DBConnection.getDBConnection().getConnection();
            connection.setAutoCommit(false);
            boolean addDon = dBHandler.setData(connection, query, donation.getBloodId(), donation.getType(), donation.getNic());
            if (addDon) {
                String getCountQuery = "select `count` FROM donor WHERE nic=?";
                prepareStatement = connection.prepareStatement(getCountQuery);
                ResultSet count = dBHandler.getData(prepareStatement, donation.getNic());
                if (count.next()) {
                    String incrsQuery = "UPDATE donor SET `count`=? WHERE nic=?";
                    boolean incresment = dBHandler.setData(connection, incrsQuery, (count.getInt("count") + 1), donation.getNic());
                    if (incresment) {
                        connection.commit();
                        return true;
                    } else {
                        connection.rollback();
                        return false;
                    }
                } else {
                    connection.rollback();
                    return false;
                }
            } else {
                connection.rollback();
                return false;
            }
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (prepareStatement != null) {
                prepareStatement.close();
            }
        }
    }

    //date,type,bid,status
    public ObservableList<Donation> getDonationListOfDonor(String donorId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select bloodId,`type`,donationDate,status from donation WHERE nic=? && YEAR(donationDate)+1>YEAR(CURDATE())";
        ObservableList<Donation> observableArrayList = FXCollections.observableArrayList();
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, donorId);
            while (executeQuery.next()) {
                Date date = executeQuery.getDate("donationDate");
                Donation donation = new Donation(executeQuery.getString("bloodId"), date.toString(), executeQuery.getString("nic"),
                        executeQuery.getString("status"),
                        executeQuery.getInt("type"));
                observableArrayList.add(donation);
            }
            return observableArrayList;
        }
    }

    public Donation getDonationFromBloodIdSimple(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        //Need bloodId & type Only;
        String query = "select `type` from donation WHERE bloodId=?";
        Donation donation = null;
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            if (executeQuery.next()) {
                int type = executeQuery.getInt("type");
                donation = new Donation(bloodId, "", "", "", type);
            }
            return donation;
        }
    }

    public Date getLastDonation(String nic, int typeIndex) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String query = "select donationDate from donation WHERE nic=? && type=? ORDER BY donationDate desc limit 1";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, nic, typeIndex);
            Date date = null;
            if (executeQuery.next()) {
                date = executeQuery.getDate("donationDate");
            }
            return date;
        }
    }
}
