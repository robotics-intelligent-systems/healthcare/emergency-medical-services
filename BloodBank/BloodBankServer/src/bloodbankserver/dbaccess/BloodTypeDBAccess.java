/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbaccess;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.bloodtype.BloodTypes;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donor;
import bloodbankserver.dbhandler.DBConnection;
import bloodbankserver.dbhandler.DBHandler;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public class BloodTypeDBAccess {

    private DBHandler dBHandler = new DBHandler();

    public ObservableList<Donor> getDonorList() throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select * from donor";
        ObservableList<Donor> observableArrayList = FXCollections.observableArrayList();
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement);
            while (executeQuery.next()) {
                Donor donor = new Donor(executeQuery.getString("nic"), executeQuery.getString("name"), executeQuery.getString("address"),
                        executeQuery.getString("bloodGroup"), executeQuery.getString("rejected"), executeQuery.getString("sex"),
                        executeQuery.getInt("tele"), executeQuery.getInt("count"));
                observableArrayList.add(donor);
            }
            return observableArrayList;
        }
    }

    public Blood getCryo(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId from cryo WHERE cryoId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            Blood blood = null;
            if (executeQuery.next()) {
                blood = new Blood(bloodId, executeQuery.getString("receiverId"), BloodTypes.CRYO.getName());
            }
            return blood;
        }
    }

    public Blood getPlatelet(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId from platelet WHERE plateletId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            Blood blood = null;
            if (executeQuery.next()) {
                blood = new Blood(bloodId, executeQuery.getString("receiverId"), BloodTypes.PLATELET.getName());
            }
            return blood;
        }
    }

    public Blood getRedCell(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId from redcell WHERE redCellId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            Blood blood = null;
            if (executeQuery.next()) {
                blood = new Blood(bloodId, executeQuery.getString("receiverId"), BloodTypes.RED_CELL.getName());
            }
            return blood;
        }
    }

    public Blood getFFp(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId from ffplasma WHERE ffplasmaId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            Blood blood = null;
            if (executeQuery.next()) {
                blood = new Blood(bloodId, executeQuery.getString("receiverId"), BloodTypes.PLASMA.getName());
            }
            return blood;
        }
    }

    public Blood getCSP(String bloodId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        String query = "select receiverId from csp WHERE cspId=?";
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            Blood blood = null;
            if (executeQuery.next()) {
                blood = new Blood(bloodId, executeQuery.getString("receiverId"), BloodTypes.CSP.getName());
            }
            return blood;
        }
    }

    public Blood getBlood(String bloodId, int type) throws ClassNotFoundException, SQLException, PropertyVetoException {
        Blood blood = null;
        String query = "";
        switch (type) {
            case (1):
                query = "select expiaryDate,bloodGroup,sentDate,receiverId from redcell WHERE datediff(curdate(),expiaryDate)<0 && (receiverId is null || receiverId='') && redcellId=?";
                break;
            case (2):
                query = "select expiaryDate,bloodGroup,sentDate,receiverId from platelet WHERE datediff(curdate(),expiaryDate)<0 && (receiverId is null || receiverId='') && plateletId=?";
                break;
            case (3):
                query = "select expiaryDate,bloodGroup,sentDate,receiverId from ffplasma WHERE datediff(curdate(),expiaryDate)<0 && (receiverId is null || receiverId='') && ffplasmaId=?";
                break;
            case (4):
                query = "select expiaryDate,bloodGroup,sentDate,receiverId from cryo WHERE datediff(curdate(),expiaryDate)<0 && (receiverId is null || receiverId='') && cryoId=?";
                break;
            case (5):
                query = "select expiaryDate,bloodGroup,sentDate,receiverId from csp WHERE datediff(curdate(),expiaryDate)<0 && (receiverId is null || receiverId='') && cspId=?";
                break;
        }
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement, bloodId);
            if (executeQuery != null && executeQuery.next()) {
                blood = new Blood(executeQuery.getDate("expiaryDate"), executeQuery.getDate("sentDate"), bloodId,
                        executeQuery.getString("bloodGroup"), null, null, BloodType.getStringOf(type));
                blood.setBloodId(bloodId);
            }
            return blood;
        }
    }

    public boolean removeBlood(Blood blood) throws SQLException, ClassNotFoundException, PropertyVetoException {
        String bloodType = blood.getBloodType();
        String subTypeQuery = "";
        switch (BloodType.getIndexOf(bloodType)) {
            case (1):
                subTypeQuery = "DELETE FROM redcell WHERE redCellId=?";
                break;
            case (2):
                subTypeQuery = "DELETE FROM platelet WHERE plateletId=?";
                break;
            case (3):
                subTypeQuery = "DELETE FROM ffplasma WHERE ffplasmaId=?";
                break;
            case (4):
                subTypeQuery = "DELETE FROM cryo WHERE cryoId=?";
                break;
            case (5):
                subTypeQuery = "DELETE FROM csp WHERE cspId=?";
                break;
        }
        return dBHandler.setData(subTypeQuery, blood.getBloodId());
    }

    public List<Blood> getExpiredBloodOf(int bloodType) throws ClassNotFoundException, PropertyVetoException, SQLException {
        ArrayList<Blood> arrayList = new ArrayList<>();
        String query = "";
        switch (bloodType) {
            case (1):
                query = "select redcellId from redcell WHERE datediff(curdate(),expiaryDate)>0";
                break;
            case (2):
                query = "select plateletId from platelet WHERE datediff(curdate(),expiaryDate)>0";
                break;
            case (3):
                query = "select ffplasmaId from ffplasma WHERE datediff(curdate(),expiaryDate)>0";
                break;
            case (4):
                query = "select cryoId from cryo WHERE datediff(curdate(),expiaryDate)>0";
                break;
            case (5):
                query = "select cspId from csp WHERE datediff(curdate(),expiaryDate)>0";
                break;
        }
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            ResultSet executeQuery = dBHandler.getData(prepareStatement);
            while (executeQuery.next()) {
                Blood blood = new Blood(executeQuery.getString(1));
                blood.setBloodType(BloodType.getStringOf(bloodType));
                arrayList.add(blood);
            }
        }
        return arrayList;
    }

    public boolean markReturned(Blood blood) throws ClassNotFoundException, PropertyVetoException, SQLException {
        String bloodType = blood.getBloodType();
        String query = "";
        switch (BloodType.getIndexOf(bloodType)) {
            case (1):
                query = "UPDATE redcell SET sentDate = NULL, receiverId = NULL WHERE plateletId = ?";
                break;
            case (2):
                query = "UPDATE platelet SET sentDate = NULL, receiverId = NULL WHERE plateletId = ?";
                break;
            case (3):
                query = "UPDATE ffplasma SET sentDate = NULL, receiverId = NULL WHERE plateletId = ?";
                break;
            case (4):
                query = "UPDATE cryo SET sentDate = NULL, receiverId = NULL WHERE plateletId = ?";
                break;
            case (5):
                query = "UPDATE csp SET sentDate = NULL, receiverId = NULL WHERE plateletId = ?";
                break;
        }
        return dBHandler.setData(query, blood.getBloodId());
    }
}
