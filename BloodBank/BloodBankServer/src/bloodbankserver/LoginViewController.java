/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver;

import bloodbankcommon.controller.LoginController;
import bloodbankserver.controller.LoginControllerImpl;
import bloodbankserver.dbaccess.LoginDBAccess;
import bloodbankserver.view.ServerViewController;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Janith
 */
public class LoginViewController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="Injections">
    @FXML
    private TextField userText;
    @FXML
    private PasswordField passField;
    @FXML
    private Rectangle loginRect;
    @FXML
    private AnchorPane loginPane;
    @FXML
    private Label progressLabel;
    @FXML
    private ProgressBar progressBar;
    //</editor-fold>
    private Stage primaryStage;
    private Stage loginStage;
    private boolean loadSuccess = false;
    private Timeline timer;
    private LoginController loginController;
    private LoginDBAccess loginDBAccess;

    public LoginViewController() {
        loginDBAccess = new LoginDBAccess();
    }

    public void setStages(Stage primaryStage, Stage loginStage) {
        this.primaryStage = primaryStage;
        this.loginStage = loginStage;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        disableLogin();
        hideLogin();

    }

    @FXML
    public void onCancelBtnClick(ActionEvent event) {
        progressLabel.setText("Unloading Resources...");
        timer = new Timeline(new KeyFrame(Duration.millis(30), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                progressBar.setProgress(progressBar.getProgress() - 0.01);
            }
        }));
        timer.setCycleCount((int) (progressBar.getProgress() * 100));
        timer.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loginStage.close();
                System.exit(0);
            }
        });
        timer.play();

    }

    @FXML
    public void onLoginBtnClick(ActionEvent event) {

        try {
            boolean loginStatus = login(userText.getText(), getHash(passField.getText()));
            if (loginStatus) {
                progressLabel.setText("Login Successful...");
                progressBar.setProgress(0.6);
                loadPhase4();
            } else {
                Dialogs.showErrorDialog(loginStage, "Check Username and Password", "Invalid Username or Password");
                userText.setText("");
                passField.setText("");
                userText.requestFocus();
            }
        } catch (ClassNotFoundException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "RMI Error! Check Server Status", "Error Loading Program");
            System.exit(0);
        } catch (PropertyVetoException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "Server Error! Check Server Status", "Error Loading Program");
            System.exit(0);
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "Database Error! Check Database Status", "Error Loading Program");
            System.exit(0);
        }

    }

    private String getHash(String password) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString().toLowerCase();
    }

    private void showLogin() {
        loginRect.setOpacity(1);
        loginPane.setOpacity(1);
    }

    private void hideLogin() {
        loginRect.setOpacity(0);
        loginPane.setOpacity(0);
    }

    private void disableLogin() {
        loginRect.setDisable(true);
        loginPane.setDisable(true);
    }

    private void enableLogin() {
        loginRect.setDisable(false);
        loginPane.setDisable(false);
    }

    protected void loadPhase1() {
        progressBar.setProgress(0.0);
        timer = new Timeline(new KeyFrame(Duration.millis(100), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                progressBar.setProgress(progressBar.getProgress() + 0.01);
            }
        }));
        timer.setCycleCount(10);
        timer.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loadPhase2();
            }
        });
        timer.play();
    }

    private void loadPhase2() {

        progressLabel.setText("Checking Connection...");
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (loadSuccess) {
                    progressLabel.setText("Connection Established...");
                    Timeline timerPhase1 = new Timeline(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            progressBar.setProgress(progressBar.getProgress() + 0.01);
                        }
                    }));
                    timerPhase1.setCycleCount(5);
                    timerPhase1.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            loadPhase3();
                        }
                    });
                    timerPhase1.play();
                } else {
                    Dialogs.showErrorDialog(loginStage, "Error Establishing Connection.\nCheck Server.",
                            "Click Ok to Exit", "Error Loading Program");
                    System.exit(0);
                }
            }
        };
        timer = new Timeline(new KeyFrame(Duration.millis(200), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                progressBar.setProgress(progressBar.getProgress() + 0.01);
            }
        }));
        timer.setCycleCount(5);
        timer.setOnFinished(eventHandler);
        timer.play();

        try {
            boolean checkConnection = loginDBAccess.check();
            loadSuccess = checkConnection;
        } catch (ClassNotFoundException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "RMI Error! Check Server Status", "Error Loading Program");
            System.exit(0);
        } catch (SQLException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "Database Error! Check Database Status", "Error Loading Program");
            System.exit(0);
        } catch (PropertyVetoException ex) {
            Dialogs.showErrorDialog(loginStage, ex.getMessage() + "\n\nProgram will exit now.",
                    "Server Error! Check Server Status", "Error Loading Program");
            System.exit(0);
        }

    }

    private void loadPhase3() {
        progressBar.setProgress(0.5);
        progressLabel.setText("Please Login Now...");
        showLogin();
        enableLogin();
    }

    private boolean login(String user, String hash) throws ClassNotFoundException, PropertyVetoException, SQLException {
        return loginDBAccess.login(user, hash, "server");
    }

    private void loadPhase4() {

        loadSuccess = false;
        progressLabel.setText("Loading UI...");
        timer = new Timeline(new KeyFrame(Duration.millis(50), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                progressBar.setProgress(progressBar.getProgress() + 0.01);
            }
        }));
        timer.setCycleCount(40);
        timer.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (loadSuccess) {
                    loginStage.close();
                    primaryStage.setOpacity(0.0);
                    primaryStage.show();
                    primaryStage.setOpacity(1.0);
                } else {
                    Dialogs.showErrorDialog(null, "Press Ok to exit",
                            "Error Loading UI");
                    System.exit(0);
                }
            }
        });
        timer.play();

        Parent root = null;
        ServerViewController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ServerView.fxml"));
            root = (Parent) loader.load();
            controller = (ServerViewController) loader.getController();
        } catch (IOException ex) {
            Dialogs.showErrorDialog(null, ex.getMessage() + "\n\nPress Ok to exit",
                    "Error Loading UI", "Error");
            System.exit(0);
        }
        Scene scene = new Scene(root);
        String image = LoginViewController.class.getResource("flow8.jpg").toExternalForm();
        root.setStyle("-fx-background-image: url('" + image + "');"
                + "-fx-background-position: center center;"
                + "-fx-background-repeat: stretch;");
        primaryStage.setTitle("Blood Bank Administration Software - Server");
        primaryStage.getIcons().add(new Image(LoginViewController.class.getResourceAsStream("BloodDrop.png")));
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        controller.setStage(primaryStage);
        primaryStage.setOnCloseRequest(controller.windowCloseHandler);
        loadSuccess = true;

    }
}
