/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.model.Donation;
import bloodbankserver.dbaccess.DonationDBAccess;
import bloodbankserver.observable.DonorObservable;
import bloodbankserver.reservation.DonationReservation;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Date;
import java.sql.SQLException;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public class DonationControllerImpl extends UnicastRemoteObject implements DonationControllerIntf {

    public DonationControllerImpl() throws RemoteException {
    }
    private DonationDBAccess donationDBAccess = new DonationDBAccess();
    private static DonorObservable donorObservable = new DonorObservable();
    private static DonationReservation donationReservation = new DonationReservation();

    @Override
    public String getBloodType(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationDBAccess.getBloodType(bloodId);
    }

    @Override
    public ObservableList<Donation> getDonationListOfDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationDBAccess.getDonationListOfDonor(donorId);
    }

    @Override
    public boolean addDonation(Donation donation) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationDBAccess.addDonation(donation);
    }

    @Override
    public Donation getDonnationFromBloodIdSimple(String bloodId) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationDBAccess.getDonationFromBloodIdSimple(bloodId);
    }

    @Override
    public Date getLastDonation(String nic, int typeIndex) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
        return donationDBAccess.getLastDonation(nic, typeIndex);
    }
}
