/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Donation;
import bloodbankcommon.model.Receiver;
import bloodbankserver.dbaccess.ReceiverDBAccess;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author oshanz
 */
public class ReceiverControllerImpl extends UnicastRemoteObject implements ReceiverControllerIntf {

    public ReceiverControllerImpl() throws RemoteException {
    }
    private ReceiverDBAccess receiverDBAccess = new ReceiverDBAccess();
    // Reciver patient or hospi Observ Onada?????

    @Override
    public Receiver getConfirmReceiver(String searchId) throws ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.getConfirmReceiver(searchId);
    }

    @Override
    public Receiver getReceiverSimple(String id) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.getReceiverSimple(id);
    }

    @Override
    public ObservableList<Donation> getTransfusionsOf(String patient) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.getTransfusionsOf(patient);
    }

    @Override
    public boolean addPatient(Receiver receiver) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.addPatient(receiver);
    }

    @Override
    public Receiver getHospital(String hospitalId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.getHospital(hospitalId);
    }

    @Override
    public boolean updateHospital(Receiver hospital) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.updateHospital(hospital);
    }

    @Override
    public boolean addHospital(Receiver hospital) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.addHospital(hospital);
    }

    @Override
    public boolean submitOutsourcings(String hospId, List<Blood> bloodList) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.submitOutsourcings(hospId, bloodList);
    }

    @Override
    public String getNextHospitalId() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return receiverDBAccess.getNextHospitalId();
    }
}
