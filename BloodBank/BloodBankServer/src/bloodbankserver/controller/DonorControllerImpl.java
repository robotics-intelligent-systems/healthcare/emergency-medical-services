/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.model.Donor;
import bloodbankserver.dbaccess.DonorDBAccess;
import bloodbankserver.observable.DonorObservable;
import bloodbankserver.reservation.DonorReservation;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;

/**
 *
 * @author oshanz
 */
public class DonorControllerImpl extends UnicastRemoteObject implements DonorControllerIntf {

    public DonorControllerImpl() throws RemoteException {
    }
    private DonorDBAccess donorDBAccess = new DonorDBAccess();
    private static DonorObservable donorObservable = new DonorObservable();
    private static DonorReservation donorReservation = new DonorReservation();

    @Override
    public Donor getDonor(String donorId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.getDonor(donorId);
    }

    @Override
    public Donor getDonorSimple(String donorId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.getDonorSimple(donorId);
    }

    @Override
    public String getBloodGroup(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.getBloodGroup(bloodId);
    }

    @Override
    public boolean getDonorExists(String id) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.getDonorExists(id);
    }

    @Override
    public boolean addDonor(Donor donor) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.addDonor(donor);
    }

    @Override
    public Donor getDonorLess(String donorId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.getDonorLess(donorId);
    }

    @Override
    public boolean editDonor(Donor donor) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return donorDBAccess.editDonor(donor);
    }
}
