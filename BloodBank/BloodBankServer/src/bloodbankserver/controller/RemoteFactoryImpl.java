/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.controller.CampaignControllerIntf;
import bloodbankcommon.controller.DonationControllerIntf;
import bloodbankcommon.controller.DonorControllerIntf;
import bloodbankcommon.controller.LoginController;
import bloodbankcommon.controller.ReceiverControllerIntf;
import bloodbankcommon.controller.RemoteFactory;
import bloodbankcommon.controller.RequesterControllerIntf;
import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.controller.UsersControllerIntf;
import bloodbankcommon.request.RequestHandlerIntf;
import bloodbankserver.request.RequestHandler;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author oshanz
 */
public class RemoteFactoryImpl extends UnicastRemoteObject implements RemoteFactory {

    public RemoteFactoryImpl() throws RemoteException {
    }

    @Override
    public BloodTypeControllerIntf getBloodTypeController() throws RemoteException {
        return new BloodTypeControllerImpl();
    }

    @Override
    public DonationControllerIntf getDonationController() throws RemoteException {
        return new DonationControllerImpl();
    }

    @Override
    public DonorControllerIntf getdDonorController() throws RemoteException {
        return new DonorControllerImpl();
    }

    @Override
    public RequesterControllerIntf getRequesterController() throws RemoteException {
        return new RequesterControllerImpl();
    }

    @Override
    public CampaignControllerIntf getCampaignControllerIntf() throws RemoteException {
        return new CampaignControllerImpl();
    }

    @Override
    public ReceiverControllerIntf getReceiverControllerIntf() throws RemoteException {
        return new ReceiverControllerImpl();
    }

    @Override
    public SeperationControllerIntf getSeperationControllerIntf() throws RemoteException {
        return new SeperationControllerImpl();
    }

    @Override
    public UsersControllerIntf getUsersControllerIntf() throws RemoteException {
        return new UserControllerImpl();
    }

    @Override
    public DonorControllerIntf getDonorController() throws RemoteException {
        return new DonorControllerImpl();
    }

    @Override
    public SeperationControllerIntf getSeperationController() throws RemoteException {
        return new SeperationControllerImpl();
    }

    @Override
    public ReceiverControllerIntf getReceiverController() throws RemoteException {
        return new ReceiverControllerImpl();
    }

    @Override
    public LoginController getLoginController() throws RemoteException {
        return new LoginControllerImpl();
    }
    
    @Override
    public synchronized RequestHandlerIntf getRequestHandler() throws RemoteException {
        return  new RequestHandler();
    }

}
