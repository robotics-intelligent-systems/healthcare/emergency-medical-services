/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.bloodtype.BloodType;
import bloodbankcommon.controller.BloodTypeControllerIntf;
import bloodbankcommon.model.Blood;
import bloodbankserver.dbaccess.BloodTypeDBAccess;
import bloodbankserver.observable.BloodObservable;
import bloodbankserver.reservation.BloodReservation;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class BloodTypeControllerImpl extends UnicastRemoteObject implements BloodTypeControllerIntf {

    public BloodTypeControllerImpl() throws RemoteException {
    }
    private BloodTypeDBAccess bloodTypeDBAccess = new BloodTypeDBAccess();
    private static BloodObservable bloodObservable = new BloodObservable();
    private static BloodReservation bloodReservation = new BloodReservation();

    //For All Below - bloodId, ReceiverId, t;
    @Override
    public Blood getCryo(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getCryo(bloodId);
    }

    @Override
    public Blood getCsp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getCSP(bloodId);
    }

    @Override
    public Blood getFfp(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getFFp(bloodId);
    }

    @Override
    public Blood getRedCell(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getRedCell(bloodId);
    }

    @Override
    public Blood getPlatelet(String bloodId) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getPlatelet(bloodId);
    }
////For Expired : Only id and type needed; (type could be added here)   ////////////////////////////

    @Override
    public List<Blood> getExpiredRedCells() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getExpiredBloodOf(BloodType.RED_CELL);
    }

    @Override
    public List<Blood> getExpiredPlatelets() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getExpiredBloodOf(BloodType.PLATELET);
    }

    @Override
    public List<Blood> getExpiredPlasma() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getExpiredBloodOf(BloodType.PLASMA);
    }

    @Override
    public List<Blood> getExpiredCryo() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getExpiredBloodOf(BloodType.CRYO);
    }

    @Override
    public List<Blood> getExpiredCsp() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.getExpiredBloodOf(BloodType.CSP);
    }

    @Override
    public boolean removeBlood(Blood blood) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        //get blood type from blood.getType();
        //remove from Seperations & bType Tables
        return bloodTypeDBAccess.removeBlood(blood);
    }

    @Override
    public Blood getBlood(String bloodId, int type) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        //if not exists id = null;
        //check for issued and expired;
        return bloodTypeDBAccess.getBlood(bloodId, type);
    }

    @Override
    public boolean isBloodIssued(String id, int type) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        Blood blood = bloodTypeDBAccess.getBlood(id, type);
        if(blood == null){
            return true;
        }
        if(blood.getReceiverId() == null || blood.getReceiverId().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public boolean markReturned(Blood blood) throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException {
        return bloodTypeDBAccess.markReturned(blood);
    }
}
