/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.SeperationControllerIntf;
import bloodbankcommon.model.Blood;
import bloodbankcommon.model.Seperation;
import bloodbankserver.dbaccess.SeperationDBAccess;
import bloodbankserver.observable.SeperationObservable;
import bloodbankserver.reservation.SeperationReservation;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oshanz
 */
public class SeperationControllerImpl extends UnicastRemoteObject implements SeperationControllerIntf {

    public SeperationControllerImpl() throws RemoteException {
    }
    private SeperationDBAccess seperationDBAccess = new SeperationDBAccess();
    private static SeperationObservable seperationObservable = new SeperationObservable();
    private static SeperationReservation seperationReservation = new SeperationReservation();

    @Override
    public Seperation getSeperationOf(String bloodId) throws RemoteException, ClassNotFoundException, SQLException ,PropertyVetoException{
        return seperationDBAccess.getSeperationOf(bloodId);
    }

    @Override
    public boolean submitSeperation(Seperation seperation, List<Blood> bloodList) throws RemoteException, ClassNotFoundException, SQLException,PropertyVetoException {
            //Didn't passed the bloodList to bloodTypeController because Connection Autocommit issue;
            //So add everything from seperationController;
            return seperationDBAccess.submitSeperation(seperation, bloodList);
    }
}
