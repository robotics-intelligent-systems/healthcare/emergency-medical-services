/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.RequesterControllerIntf;
import bloodbankserver.dbaccess.RequesterDBAccess;
import bloodbankserver.observable.RequestObservable;
import bloodbankserver.reservation.RequestReservation;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author oshanz
 */
public class RequesterControllerImpl extends UnicastRemoteObject implements RequesterControllerIntf {

    public RequesterControllerImpl() throws RemoteException {
    }
    
    private RequesterDBAccess dBAccess = new RequesterDBAccess();
    private static RequestObservable requestObservable = new RequestObservable();
    private static RequestReservation requestReservation = new RequestReservation();

}
