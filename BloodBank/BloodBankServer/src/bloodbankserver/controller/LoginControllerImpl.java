/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.controller;

import bloodbankcommon.controller.LoginController;
import bloodbankserver.dbaccess.LoginDBAccess;
import java.beans.PropertyVetoException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;

/**
 *
 * @author Janith
 */
public class LoginControllerImpl extends UnicastRemoteObject implements LoginController{
    
    private LoginDBAccess loginDBAccess = new LoginDBAccess();

    public LoginControllerImpl() throws RemoteException {
    }
    
    @Override
    public boolean checkConnection() throws RemoteException, ClassNotFoundException, SQLException, PropertyVetoException{
        return loginDBAccess.check();
    }

    @Override
    public boolean login(String user, String hash, String type) throws RemoteException, ClassNotFoundException, PropertyVetoException, SQLException {
        return loginDBAccess.login(user, hash, type);
    }
}
