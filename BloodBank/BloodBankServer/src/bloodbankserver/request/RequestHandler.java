/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.request;

import bloodbankcommon.model.Request;
import bloodbankcommon.request.RequestHandlerIntf;
import bloodbankcommon.model.Lab;
import bloodbankcommon.model.Request_ObservLab;
import bloodbankcommon.model.Request_ObservRequest;
import bloodbankcommon.observer.LabObserver;
import bloodbankcommon.observer.RequestObserver;
import com.thoughtworks.xstream.persistence.FilePersistenceStrategy;
import com.thoughtworks.xstream.persistence.XmlArrayList;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author oshanz
 */
public class RequestHandler extends UnicastRemoteObject implements RequestHandlerIntf {

    private static ArrayList<Lab> labData = new ArrayList<>();
    private static HashMap<Integer, RequestObserver> wardData = new HashMap<>();
    private static int requestNumber = 0;
    private static XmlArrayList xmlArrayList;

    public RequestHandler() throws RemoteException {
    }

    private Lab getBestLab() {
        Lab bestLab = null;
        for (Lab lab : labData) {
            if (bestLab == null || lab.getRequests() <= bestLab.getRequests()) {
                bestLab = lab;
            }
        }
        return bestLab;
    }

    @Override
    public boolean addLab(LabObserver labObserver, int LabId) throws RemoteException {
        return labData.add(new Lab(labObserver, 0, LabId));
    }

    @Override
    public boolean addWard(RequestObserver requestObserver, int wardId) throws RemoteException {
        return wardData.put(wardId, requestObserver) == null ? true : false;
    }

    @Override
    public boolean removeLab(int labId) throws RemoteException {
        for (Lab lab : labData) {
            if (lab.getLabId() == labId) {
                return labData.remove(lab);
            }
        }
        return false;
    }

    @Override
    public boolean removeWard(int wardId) throws RemoteException {
        return wardData.remove(wardId) != null ? true : false;
    }

    @Override
    public Request_ObservLab sendRequest(Request request) throws RemoteException {
        try {
            backUpRequest(request);
        } catch (IOException ex) {
            
        }
        request.setRequestId(requestNumber);
        requestNumber++;
        Lab bestLab = getBestLab();
        LabObserver btLabObserver = bestLab.getLabObserver();
        btLabObserver.addRequest(wardData.get(request.getWardId()), request);
        bestLab.setRequests(bestLab.getRequests() + 1);
        return new Request_ObservLab(request, btLabObserver);
    }

    /**
     * *
     *
     *
     *
     * Below are extra methods
     *
     *
     *
     */
    /**
     *
     *
     * @param request_ObservRequest
     * @throws RemoteException
     */
    @Override
    public void labLogout(Request_ObservRequest request_ObservRequest) throws RemoteException {
        Lab bestLab = getBestLab();
        LabObserver lab = bestLab.getLabObserver();
        lab.addRequest(request_ObservRequest.getRequestObserver(), request_ObservRequest.getRequest());
    }

    private void backUpRequest(Request request) throws IOException {
        if (xmlArrayList == null) {
            File file = new File("/tmp");
            file.mkdir();
            xmlArrayList = new XmlArrayList(new FilePersistenceStrategy(file));
        }
        xmlArrayList.add(requestNumber, request);
    }

    private void restoreRequests() {
        if (xmlArrayList == null) {
            File file = new File("/tmp");
            file.mkdir();
            xmlArrayList = new XmlArrayList(new FilePersistenceStrategy(file));
        }
        for (Iterator iterator = xmlArrayList.iterator(); iterator.hasNext();) {
            Request request = (Request) iterator.next();
            /**
             * *
             *
             * Cant retore to lab because no observers;
             *
             */
        }
    }
}
