/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.observable;

import bloodbankcommon.model.Donor;
import bloodbankcommon.observer.DonorObserver;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class DonorObservable {
    private List<DonorObserver> donorObservers;

    public DonorObservable() {
        this.donorObservers = new ArrayList<>();
    }

    public void addBloodObserver(DonorObserver donorObserver) {
        donorObservers.add(donorObserver);
    }

    public void removeBloodObserver(DonorObserver donorObserver) {
        donorObservers.remove(donorObserver);
    }

    public void notifyBloodObserver(Donor donor) throws RemoteException{
        for (DonorObserver donorObserver : donorObservers) {
            donorObserver.update(donor);
        }
    }
}
