/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.observable;

import bloodbankcommon.model.Blood;
import bloodbankcommon.observer.BloodObserver;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class BloodObservable {

    private List<BloodObserver> bloodObservers;

    public BloodObservable() {
        this.bloodObservers = new ArrayList<>();
    }

    public void addBloodObserver(BloodObserver bloodObserver) {
        bloodObservers.add(bloodObserver);
    }

    public void removeBloodObserver(BloodObserver bloodObserver) {
        bloodObservers.remove(bloodObserver);
    }

    public void notifyBloodObserver(Blood blood) throws RemoteException{
        for (BloodObserver bloodObserver : bloodObservers) {
            bloodObserver.update(blood);
        }
    }
}
