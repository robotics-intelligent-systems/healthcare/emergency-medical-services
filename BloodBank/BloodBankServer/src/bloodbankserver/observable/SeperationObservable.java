/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.observable;

import bloodbankcommon.model.Seperation;
import bloodbankcommon.observer.SeperationObserver;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class SeperationObservable {
    private List<SeperationObserver> seperationObservers;

    public SeperationObservable() {
        this.seperationObservers = new ArrayList<>();
    }

    public void addBloodObserver(SeperationObserver seperationObserver) {
        seperationObservers.add(seperationObserver);
    }

    public void removeBloodObserver(SeperationObserver seperationObserver) {
        seperationObservers.remove(seperationObserver);
    }

    public void notifyBloodObserver(Seperation seperation) throws RemoteException{
        for (SeperationObserver seperationObserver : seperationObservers) {
            seperationObserver.update(seperation);
        }
    }
}
