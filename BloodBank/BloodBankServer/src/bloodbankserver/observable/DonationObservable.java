/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.observable;

import bloodbankcommon.model.Donation;
import bloodbankcommon.observer.DonationObserver;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class DonationObservable {
    private List<DonationObserver> donationObservers;

    public DonationObservable() {
        this.donationObservers = new ArrayList<>();
    }

    public void addBloodObserver(DonationObserver donationObserver) {
        donationObservers.add(donationObserver);
    }

    public void removeBloodObserver(DonationObserver donationObserver) {
        donationObservers.remove(donationObserver);
    }

    public void notifyBloodObserver(Donation donation) throws RemoteException{
        for (DonationObserver donationObserver : donationObservers) {
            donationObserver.update(donation);
        }
    }
}
