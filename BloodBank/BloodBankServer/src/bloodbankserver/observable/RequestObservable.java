/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.observable;

import bloodbankcommon.model.Request;
import bloodbankcommon.observer.RequestObserver;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author oshanz
 */
public class RequestObservable {

    private List<RequestObserver> requestObservers;

    public RequestObservable() {
        this.requestObservers = new ArrayList<>();
    }

    public void addBloodObserver(RequestObserver requestObserver) {
        requestObservers.add(requestObserver);
    }

    public void removeBloodObserver(RequestObserver requestObserver) {
        requestObservers.remove(requestObserver);
    }

    public void notifyBloodObserver(Request request) throws RemoteException {
//        for (RequestObserver requestObserver : requestObservers) {
//            requestObserver.update(request);
//        }
    }
}
