/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.view;

import bloodbankserver.controller.RemoteFactoryImpl;
import bloodbankserver.dbhandler.BackUp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Janith
 */
public class ServerViewController implements Initializable {

    //<editor-fold defaultstate="collapsed" desc="FXML Injections">
    @FXML
    private ImageView aboutImage;
    @FXML
    private TextField portTextField;
    @FXML
    private Button serverControlButton;
    @FXML
    private Button setPortButton;
    @FXML
    private TitledPane addUserTitledPane;
    @FXML
    private TextField addUserNameTextField;
    @FXML
    private Button addUserButton;
    @FXML
    private PasswordField addPasswordField;
    @FXML
    private PasswordField addPasswordRetypeField;
    @FXML
    private TitledPane editUserTitledPane;
    @FXML
    private Button submitUpdateButton;
    @FXML
    private ComboBox editUserNameComboBox;
    @FXML
    private PasswordField editPasswordField;
    @FXML
    private PasswordField editPasswordRetypeField;
    @FXML
    private Button backupButton;
    @FXML
    private Button restoreButton;
    @FXML
    private Accordion accordian;
    //</editor-fold>
    private ArrayList<String> userList;
    private Paint redColor = Color.web("#ff3333");
    private Paint greenColor = Color.web("#0c9900");
    private Properties properties;
    private Stage stage;
    private Registry bloodBankReg;
    private RemoteFactoryImpl remoteFactoryImpl;
    private int port = 5050;
    private String binding = "BloodBankServer";
    private boolean serverRunning = false;
    public EventHandler<WindowEvent> windowCloseHandler;

    public ServerViewController() {
        properties = new Properties();
        try {
            File file = new File("Server.config");
            boolean mkdir = file.createNewFile();
            if (mkdir) {
                Properties defaultValues = new Properties();
                defaultValues.setProperty("port", "5050");
                defaultValues.store(new FileWriter(file), "");
            }
            properties.load(new FileInputStream(file));
        } catch (FileNotFoundException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Server configuration file missing", "Error");
        } catch (IOException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Cannot read server configuration file", "Error");
        }
        System.setProperty("java.rmi.server.hostname", "127.0.0.1");
        //userList = getUserList();

        windowCloseHandler = new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (serverRunning) {
                    controlServer(null);
                }
                stage.close();
                System.exit(0);
            }
        };
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //fillComboBoxes();            
        displayServerPort();
        addUserTitledPane.setExpanded(true);
    }

    private void displayServerPort() {
        portTextField.setText(properties.getProperty("port"));
    }

    private ArrayList<String> getUserList() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void fillComboBoxes() {
        for (String string : userList) {
            editUserNameComboBox.getItems().add(string);
        }
    }

    @FXML
    private void controlServer(ActionEvent event) {
        if (!serverRunning) {
            try {
                remoteFactoryImpl = new RemoteFactoryImpl();
                if (bloodBankReg == null) {
                    bloodBankReg = LocateRegistry.createRegistry(port);
                }
                bloodBankReg.rebind(binding, remoteFactoryImpl);
                serverControlButton.setTextFill(greenColor);
                serverControlButton.setText("Server is Running");
                serverRunning = true;
            } catch (RemoteException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Error starting server ", "Error");
            }
        } else {
            try {
                bloodBankReg.unbind(binding);
                UnicastRemoteObject.unexportObject(remoteFactoryImpl, true);
                serverControlButton.setTextFill(redColor);
                serverControlButton.setText("Start Server");
                serverRunning = false;
            } catch (RemoteException | NotBoundException ex) {
                Dialogs.showErrorDialog(stage, ex.getMessage(), "Error stopping server ", "Error");
            }
        }

    }

    @FXML
    private void onAboutClick(MouseEvent event) {
        Stage aboutStage = new Stage();
        aboutStage.initModality(Modality.APPLICATION_MODAL);
        aboutStage.getIcons().add(new Image(ServerViewController.class.getResourceAsStream("BloodDrop.png")));
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("aboutview/AboutView.fxml"));
        } catch (IOException ex) {
        }
        Scene scene = new Scene(root);
        aboutStage.setScene(scene);
        aboutStage.show();
    }

    @FXML
    private void onSetPortAction() {
        String newPort = portTextField.getText();
        properties.setProperty("port", newPort);
        try {
            properties.store(new FileWriter(new File("Server.config")), newPort);
        } catch (IOException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Cannot read server configuration file", "Error");
        } finally {
            displayServerPort();
        }
    }

    @FXML
    private void onBackUpAction() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Folder to Export");
        File showDialog = directoryChooser.showDialog(stage);
        String path = showDialog.getPath();
        try {
            int response = new BackUp().createBackup(path);
            if (response == 0) {
                Dialogs.showInformationDialog(stage, "Backup Successfully Created", "Success");
            } else {
                Dialogs.showErrorDialog(stage, "Error ID : " + response, "Error Occured Backing Up", "Error");
            }
        } catch (IOException | InterruptedException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Error Occured Backing Up", "Error");
        }
    }
    
    @FXML
    private void onRestoreAction() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Backup File");
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MySQL Backup Files", "*.sql"));
        File backupFile = fileChooser.showOpenDialog(stage);
        try {
            int response = new BackUp().restoreBackup(backupFile);
            if (response == 0) {
                Dialogs.showInformationDialog(stage, "Database Successfully Restored", "Success");
            } else {
                Dialogs.showErrorDialog(stage, "Error ID : " + response, "Error Occured Restoring", "Error");
            }
        } catch (IOException | InterruptedException ex) {
            Dialogs.showErrorDialog(stage, ex.getMessage(), "Error Occured Restoring", "Error");
        }
    }
}
