
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbhandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author oshanz
 */
public class BackUp {

    private String date;

    public BackUp() {
        Calendar instance = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-a-KK-mm");
        date = simpleDateFormat.format(instance.getTime());
    }

    /**
     * Create Backup with date and a given note.
     *
     * @param path
     * @param note
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public int createBackup(String path) throws IOException, InterruptedException {
        File file = new File(path + "/" + date + ".sql");
        boolean createNewFile = file.createNewFile();
        if (createNewFile) {
            Process start = new ProcessBuilder("mysqldump", "-hlocalhost", "-uroot", "-pgnu", "bloodbank").redirectOutput(file).start();
            return start.waitFor();
        } else {
            return -1;
        }
    }

    /**
     * Only Store data in compilt time given database
     *
     * @param path
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public int restoreBackup(File path) throws IOException, InterruptedException {
        Process start = new ProcessBuilder("mysql", "-hlocalhost", "-uroot", "-pgnu", "bloodbank").redirectInput(path).start();
        return start.waitFor();
    }
}
