/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbhandler;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author oshanz
 */
public class DBConnection {

    private static ComboPooledDataSource comboPooledDataSource;
    private static DataSource pooledDataSource;
    private static DBConnection dBConnection;
    private Connection connection;

    private DBConnection() throws ClassNotFoundException, SQLException, PropertyVetoException {
            comboPooledDataSource = new ComboPooledDataSource();
            comboPooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
            comboPooledDataSource.setJdbcUrl("jdbc:mysql://localhost/bloodbank");
            comboPooledDataSource.setUser("root");
            comboPooledDataSource.setPassword("gnu");
            // comboPooledDataSource.setMaxStatements(180);
            pooledDataSource = DataSources.pooledDataSource(comboPooledDataSource);
    }

    public static synchronized DBConnection getDBConnection() throws ClassNotFoundException, SQLException, PropertyVetoException {
        if (dBConnection == null) {
            dBConnection = new DBConnection();
        }
        return new DBConnection();
    }

    public Connection getConnection() throws SQLException {
        return pooledDataSource.getConnection();
    }
}
