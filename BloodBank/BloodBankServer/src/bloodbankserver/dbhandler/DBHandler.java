/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bloodbankserver.dbhandler;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author oshanz
 */
public class DBHandler {

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public boolean setData(String query, Object... values) throws ClassNotFoundException, PropertyVetoException, SQLException {
        readWriteLock.readLock().lock();
        try (Connection connection = DBConnection.getDBConnection().getConnection();
                PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            int index = 1;
            for (Object object : values) {
                prepareStatement.setObject(index, object);
                index++;
            }
            int executeUpdate = prepareStatement.executeUpdate();
            if (executeUpdate >= 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public boolean setData(Connection connection, String query, Object... values) throws ClassNotFoundException, PropertyVetoException, SQLException {
        readWriteLock.readLock().lock();
        try (PreparedStatement prepareStatement = connection.prepareStatement(query);) {
            int index = 1;
            for (Object object : values) {
                prepareStatement.setObject(index, object);
                index++;
            }
            int executeUpdate = prepareStatement.executeUpdate();
            if (executeUpdate >= 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public ResultSet getData(PreparedStatement ps, Object... values) throws ClassNotFoundException, PropertyVetoException, SQLException {
        readWriteLock.writeLock().lock();
        try {
            int index = 1;
            for (Object object : values) {
                ps.setObject(index, object);
                index++;
            }
            return ps.executeQuery();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }
}
